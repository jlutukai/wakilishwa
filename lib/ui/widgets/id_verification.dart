import 'package:flutter/material.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/local/local_models.dart';
import 'package:wakilishwa/core/viewmodels/id_verify_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';

import '../../utils/constants.dart';

class IdVerification extends StatefulWidget {
  final void Function(int index) onDataChange;

  IdVerification(this.onDataChange);

  @override
  _IdVerificationState createState() => _IdVerificationState();
}

class _IdVerificationState extends State<IdVerification> {
  bool hasConnection;
  TextEditingController _idNumber = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -    ${getAddUserData().toJson()}");
    _idNumber.text = getAddUserData()?.idNumber ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        widget.onDataChange(0);
        return false;
      },
      child: BaseView<IDVerificationModel>(
        builder: (context, model, child) => Scaffold(
          backgroundColor: Colors.white,
          resizeToAvoidBottomInset: true,
          body: Column(
            children: [
              Expanded(
                child: model.state == ViewState.Idle
                    ? SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "ID Verification",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        color: fromHex(blue),
                                        fontSize: 20),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 25, vertical: 5),
                                    child: TextFormField(
                                      controller: _idNumber,
                                      keyboardType: TextInputType.text,
                                      textCapitalization:
                                          TextCapitalization.characters,
                                      style: TextStyle(color: fromHex(blue)),
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return "Please Enter your ID number";
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
                                        isDense: true,
                                        hintText: "Enter your ID number",
                                        labelText: "ID Number",
                                        labelStyle:
                                            TextStyle(color: fromHex(blue)),
                                        hintStyle: TextStyle(
                                          color: Colors.blueGrey[400],
                                        ),
                                        border: getBorder(),
                                        focusedBorder: getBorder(),
                                        enabledBorder: getBorder(),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  model.isCorrect != null
                                      ? model.isCorrect
                                          ? Container()
                                          : InkWell(
                                              onTap: () async {
                                                if (_formKey.currentState
                                                    .validate()) {
                                                  hasConnection =
                                                      await checkConnection();
                                                  if (!hasConnection) {
                                                    showToast(
                                                        "Check Internet Connection");
                                                    return;
                                                  }
                                                  Map<String, dynamic> data = {
                                                    "first_name":
                                                        "${getAddUserData()?.name?.first ?? ''}",
                                                    "other_names":
                                                        "${getAddUserData()?.name?.others ?? ''}",
                                                    "national_id":
                                                        "${_idNumber.text}"
                                                  };
                                                  print(data);
                                                  try {
                                                    var r = await model
                                                        .verifyId(data);
                                                    if (r.isCorrect) {
                                                      showToast(r.status);
                                                    } else {
                                                      showToast(
                                                          "ID Details Do Not Match!");
                                                    }
                                                  } catch (e) {}
                                                }
                                              },
                                              child: Container(
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                padding: EdgeInsets.symmetric(
                                                    vertical: 15,
                                                    horizontal: 34),
                                                margin: EdgeInsets.symmetric(
                                                    horizontal: 25),
                                                decoration: BoxDecoration(
                                                  color: fromHex(blue),
                                                  borderRadius:
                                                      BorderRadius.only(
                                                    topLeft:
                                                        Radius.circular(20),
                                                    bottomLeft:
                                                        Radius.circular(20),
                                                    bottomRight:
                                                        Radius.circular(20),
                                                  ),
                                                ),
                                                child: Center(
                                                  child: Text(
                                                    'SEARCH',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 15,
                                                        fontWeight:
                                                            FontWeight.w900),
                                                  ),
                                                ),
                                              ),
                                            )
                                      : InkWell(
                                          onTap: () async {
                                            if (_formKey.currentState
                                                .validate()) {
                                              hasConnection =
                                                  await checkConnection();
                                              if (!hasConnection) {
                                                showToast(
                                                    "Check Internet Connection");
                                                return;
                                              }
                                              Map<String, dynamic> data = {
                                                "first_name":
                                                    "${getAddUserData()?.name?.first ?? ''}",
                                                "other_names":
                                                    "${getAddUserData()?.name?.others ?? ''}",
                                                "national_id":
                                                    "${_idNumber.text}"
                                              };
                                              print(data);
                                              try {
                                                var r =
                                                    await model.verifyId(data);
                                                if (r.isCorrect) {
                                                  showToast(r.status);
                                                } else {
                                                  showToast(
                                                      "ID Details Do Not Match!");
                                                }
                                              } catch (e) {}
                                            }
                                          },
                                          child: Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            padding: EdgeInsets.symmetric(
                                                vertical: 15, horizontal: 34),
                                            margin: EdgeInsets.symmetric(
                                                horizontal: 25),
                                            decoration: BoxDecoration(
                                              color: fromHex(blue),
                                              borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(20),
                                                bottomLeft: Radius.circular(20),
                                                bottomRight:
                                                    Radius.circular(20),
                                              ),
                                            ),
                                            child: Center(
                                              child: Text(
                                                'SEARCH',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 15,
                                                    fontWeight:
                                                        FontWeight.w900),
                                              ),
                                            ),
                                          ),
                                        ),
                                ],
                              ),
                              SizedBox(
                                height: 30,
                              ),
                            ],
                          ),
                        ))
                    : Center(
                        child: CircularProgressIndicator(
                          backgroundColor: fromHex(gold_dark),
                          valueColor: new AlwaysStoppedAnimation<Color>(
                            fromHex(grey),
                          ),
                        ),
                      ),
              ),
              Row(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        widget.onDataChange(0);
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 34),
                        margin: EdgeInsets.symmetric(horizontal: 25),
                        decoration: BoxDecoration(
                          color: fromHex(blue),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'BACK',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: model.isCorrect != null
                        ? model.isCorrect
                            ? InkWell(
                                onTap: () {
                                  CreateAccountBody c = getAddUserData();
                                  c.idNumber = _idNumber.text;
                                  setAddUserData(c);
                                  widget.onDataChange(2);
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 15, horizontal: 34),
                                  margin: EdgeInsets.symmetric(horizontal: 25),
                                  decoration: BoxDecoration(
                                    color: fromHex(blue),
                                    borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(20),
                                      bottomLeft: Radius.circular(20),
                                      bottomRight: Radius.circular(20),
                                    ),
                                  ),
                                  child: Center(
                                    child: Text(
                                      'NEXT',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15,
                                          fontWeight: FontWeight.w900),
                                    ),
                                  ),
                                ),
                              )
                            : Container()
                        : Container(),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
