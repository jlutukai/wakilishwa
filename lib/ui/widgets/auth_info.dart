import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/local/local_models.dart';
import 'package:wakilishwa/core/viewmodels/auth_info_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';
import 'package:wakilishwa/ui/pages/otp_page.dart';
import 'package:wakilishwa/ui/pages/web_view_page.dart';

import '../../utils/constants.dart';

class AuthInfo extends StatefulWidget {
  final void Function(int index) onDataChange;
  AuthInfo(this.onDataChange);

  @override
  _AuthInfoState createState() => _AuthInfoState();
}

class _AuthInfoState extends State<AuthInfo> {
  bool hasConnection;
  bool hasReadTC = false;
  bool hasReadR = false;
  TextEditingController _pass = TextEditingController();
  TextEditingController _confirmPass = TextEditingController();
  String pass = "";
  String confirmPass = "";
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -    ${getAddUserData().toJson()}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        widget.onDataChange(2);
        return false;
      },
      child: BaseView<AuthInfoModel>(
        builder: (context, model, child) => Scaffold(
          backgroundColor: Colors.white,
          resizeToAvoidBottomInset: true,
          body: Column(
            children: [
              Expanded(
                child: model.state == ViewState.Idle
                    ? SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Set Password",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        color: fromHex(blue),
                                        fontSize: 20),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 25, vertical: 5),
                                    child: TextFormField(
                                      controller: _pass,
                                      keyboardType:
                                          TextInputType.visiblePassword,
                                      obscureText: true,
                                      style: TextStyle(color: fromHex(blue)),
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return "Please Enter your pass word";
                                        }
                                        if (confirmPass != value) {
                                          return "Passwords do not match";
                                        }
                                        return null;
                                      },
                                      onChanged: (value) {
                                        setState(() {
                                          pass = value;
                                        });
                                      },
                                      decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
                                        isDense: true,
                                        hintText: "************",
                                        labelText: "Password",
                                        labelStyle:
                                            TextStyle(color: fromHex(blue)),
                                        hintStyle: TextStyle(
                                          color: Colors.blueGrey[400],
                                        ),
                                        border: getBorder(),
                                        focusedBorder: getBorder(),
                                        enabledBorder: getBorder(),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 25, vertical: 5),
                                    child: TextFormField(
                                      controller: _confirmPass,
                                      keyboardType:
                                          TextInputType.visiblePassword,
                                      obscureText: true,
                                      style: TextStyle(color: fromHex(blue)),
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return "Please Re-Enter your password";
                                        }
                                        if (pass != value) {
                                          return "Passwords do not match";
                                        }
                                        return null;
                                      },
                                      onChanged: (value) {
                                        setState(() {
                                          confirmPass = value;
                                        });
                                      },
                                      decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
                                        isDense: true,
                                        hintText: "*********",
                                        labelText: "Confirm Password",
                                        labelStyle:
                                            TextStyle(color: fromHex(blue)),
                                        hintStyle: TextStyle(
                                          color: Colors.blueGrey[400],
                                        ),
                                        border: getBorder(),
                                        focusedBorder: getBorder(),
                                        enabledBorder: getBorder(),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 25),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Theme(
                                          data: ThemeData(
                                              accentColor: fromHex(grey)),
                                          child: Checkbox(
                                              checkColor: fromHex(gold_dark),
                                              activeColor: fromHex(blue),
                                              value: hasReadTC,
                                              onChanged: (value) {
                                                setState(() {
                                                  hasReadTC = value;
                                                });
                                              }),
                                        ),
                                        Expanded(
                                          child: InkWell(
                                              onTap: () {
                                                Navigator.of(context).push(
                                                  MaterialPageRoute(
                                                    builder: (BuildContext
                                                            context) =>
                                                        WebViewPage(
                                                      title:
                                                          "TERMS AND CONDITIONS",
                                                      url:
                                                          "https://wakilishwa.com/terms-and-conditions",
                                                    ),
                                                  ),
                                                );
                                              },
                                              child: RichText(
                                                maxLines: 3,
                                                text: TextSpan(
                                                  text:
                                                      "I have read and agreed with the ",
                                                  style: TextStyle(
                                                      color: fromHex(grey)),
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text:
                                                            "Terms And Conditions",
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w800,
                                                            color:
                                                                fromHex(blue))),
                                                  ],
                                                ),
                                              )),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 30,
                              ),
                            ],
                          ),
                        ),
                      )
                    : Center(
                        child: CircularProgressIndicator(
                          backgroundColor: fromHex(gold_dark),
                          valueColor: new AlwaysStoppedAnimation<Color>(
                            fromHex(grey),
                          ),
                        ),
                      ),
              ),
              Row(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        widget.onDataChange(2);
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 24),
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          color: fromHex(blue),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'BACK',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () async {
                        if (!hasReadTC) {
                          showToast(
                              "Read Terms and Conditions first to proceed");
                          return;
                        }
                        hasConnection = await checkConnection();
                        if (!hasConnection) {
                          showToast("Check Internet Connection");
                          return;
                        }
                        if (_formKey.currentState.validate()) {
                          CreateAccountBody c = getAddUserData();
                          c.password = "${_pass.text}";
                          c.passwordConfirm = "${_confirmPass.text}";
                          setAddUserData(c);

                          print(
                              "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -    ${getAddUserData().toJson()}");

                          try {
                            var r = await model
                                .createUser(getAddUserData().toJson());
                            showToast(r.message);
                            CreateAccountBody c1 = getAddUserData();
                            c1.hasFinishedRegistration = true;
                            setAddUserData(c1);
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      OTPPage("login", getAddUserData().email),
                                ),
                                (Route<dynamic> route) => false);
                          } catch (e) {
                            try {
                              Map<String, dynamic> error =
                                  jsonDecode(e.toString());
                              showToast(error['errors'][0]['message']);
                            } catch (a) {}
                          }

                          // Navigator.pushNamedAndRemoveUntil(context,
                          //     HomePage.tag, (Route<dynamic> route) => false);
                        }
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 24),
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          color: fromHex(blue),
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'FINISH',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
