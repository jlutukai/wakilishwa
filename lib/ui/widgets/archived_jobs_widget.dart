import 'dart:convert';

import 'package:cupertino_tabbar/cupertino_tabbar.dart' as CupertinoTabBar;
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:jiffy/jiffy.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_my_contracts_response.dart';
import 'package:wakilishwa/core/viewmodels/jobs_i_performed_model.dart';
import 'package:wakilishwa/core/viewmodels/my_jobs_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';
import 'package:wakilishwa/ui/pages/contract_details.dart';
import 'package:wakilishwa/ui/pages/job_details.dart';

import '../../utils/constants.dart';

class ArchivedJobsWidget extends StatefulWidget {
  static const tag = 'archived_jobs';
  @override
  _ArchivedJobsWidgetState createState() => _ArchivedJobsWidgetState();
}

class _ArchivedJobsWidgetState extends State<ArchivedJobsWidget> {
  int cupertinoTabBarIValue = 0;
  int cupertinoTabBarIValueGetter() => cupertinoTabBarIValue;
  PageController _pageControlle;
  bool hasConnection;
  JobsIPerformedModel _model;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _pageControlle = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    _pageControlle.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Scaffold(
            key: _scaffoldKey,
            backgroundColor: fromHex(blue).withOpacity(0.7),
            body: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  children: [
                    Expanded(
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Column(
                              children: [
                                Text(
                                  "Adv. ${getCurrentUser()?.name?.first ?? ''} ${getCurrentUser()?.name?.others ?? ''}",
                                  style: TextStyle(
                                    color: fromHex(gold_dark),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  'Advocate',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 45, right: 20, left: 20, bottom: 25),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(40),
                            topRight: Radius.circular(40)),
                        color: Colors.white,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'ARCHIVED JOBS',
                                style: TextStyle(
                                    color: fromHex(gold_dark),
                                    fontSize: 20,
                                    fontWeight: FontWeight.w900),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          _model != null
                              ? _model.state == ViewState.Idle
                                  ? CupertinoTabBar.CupertinoTabBar(
                                      fromHex(gold_dark),
                                      fromHex(blue),
                                      [
                                        Text(
                                          "JOBS I POSTED",
                                          style: TextStyle(
                                            color: cupertinoTabBarIValue == 0
                                                ? fromHex(gold_dark)
                                                : fromHex(blue),
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                        Text(
                                          "JOBS I PERFORMED",
                                          style: TextStyle(
                                            color: cupertinoTabBarIValue == 1
                                                ? fromHex(gold_dark)
                                                : fromHex(blue),
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ],
                                      cupertinoTabBarIValueGetter,
                                      (int index) {
                                        setState(() {
                                          cupertinoTabBarIValue = index;
                                          _pageControlle.animateToPage(index,
                                              duration:
                                                  Duration(milliseconds: 1),
                                              curve: Curves.ease);
                                        });
                                      },
                                      useSeparators: false,
                                      allowScrollable: true,
                                    )
                                  : Container()
                              : CupertinoTabBar.CupertinoTabBar(
                                  fromHex(gold_dark),
                                  fromHex(blue),
                                  [
                                    Text(
                                      "JOBS I POSTED",
                                      style: TextStyle(
                                        color: cupertinoTabBarIValue == 0
                                            ? fromHex(gold_dark)
                                            : fromHex(blue),
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    Text(
                                      "JOBS I PERFORMED",
                                      style: TextStyle(
                                        color: cupertinoTabBarIValue == 1
                                            ? fromHex(gold_dark)
                                            : fromHex(blue),
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                  cupertinoTabBarIValueGetter,
                                  (int index) {
                                    setState(() {
                                      cupertinoTabBarIValue = index;
                                      _pageControlle.animateToPage(index,
                                          duration: Duration(milliseconds: 1),
                                          curve: Curves.ease);
                                    });
                                  },
                                  useSeparators: false,
                                  allowScrollable: true,
                                ),
                          Container(
                            constraints: BoxConstraints(
                                maxHeight:
                                    MediaQuery.of(context).size.height * 0.50),
                            child: PageView(
                              controller: _pageControlle,
                              onPageChanged: (index) {
                                setState(() {
                                  cupertinoTabBarIValue = index;
                                });
                              },
                              children: <Widget>[
                                _jobsFeeds(context),
                                _myJobsFeeds(context)
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        _model != null
            ? _model.state == ViewState.Idle
                ? Positioned(
                    left: 20,
                    top: 35,
                    child: FloatingActionButton(
                      foregroundColor: Colors.grey,
                      backgroundColor: fromHex(blue),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      mini: true,
                      tooltip: "go back",
                      child: Icon(
                        Icons.keyboard_backspace_sharp,
                        color: fromHex(gold_dark),
                      ),
                    ),
                  )
                : Container()
            : Positioned(
                left: 20,
                top: 35,
                child: FloatingActionButton(
                  foregroundColor: Colors.grey,
                  backgroundColor: fromHex(blue),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  mini: true,
                  tooltip: "go back",
                  child: Icon(
                    Icons.keyboard_backspace_sharp,
                    color: fromHex(gold_dark),
                  ),
                ),
              ),
      ],
    );
  }

  _jobsFeeds(BuildContext context) {
    return BaseView<MyJobsModel>(
      onModelReady: (model) async {
        hasConnection = await checkConnection();
        if (!hasConnection) {
          showToast("Check Internet Connection");
          return;
        }
        Map<String, dynamic> data = {
          "page": "all",
          "is_active": true,
          "status": 'archived'
        };
        await model.getArchivedJobs(data);
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? model.myJobs != null
              ? model.myJobs.isNotEmpty
                  ? ListView.separated(
                      itemCount: model.myJobs.length,
                      physics: BouncingScrollPhysics(),
                      separatorBuilder: (context, index) => Padding(
                            padding: EdgeInsets.symmetric(vertical: 5),
                            child: Divider(
                              indent: 15,
                              endIndent: 15,
                              color: fromHex(gold_lite),
                            ),
                          ),
                      itemBuilder: (context, index) => Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 5),
                            child: InkWell(
                              onTap: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        ViewJobDetails(
                                            s: "my_jobs",
                                            jobsData: model.myJobs[index]),
                                  ),
                                );
                              },
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${model.myJobs[index].title}",
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                            color: fromHex(gold_dark),
                                          ),
                                        ),
                                      ),
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.location_city,
                                            color: fromHex(gold_dark),
                                            size: 16,
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text(
                                            "${model.myJobs[index]?.town?.name ?? ''}",
                                            style: TextStyle(
                                              color: fromHex(blue),
                                              fontStyle: FontStyle.italic,
                                              fontSize: 12,
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "${model.myJobs[index].description}",
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.black),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.calendar_today,
                                              color: fromHex(gold_dark),
                                              size: 16,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              "${model.myJobs[index]?.jobDate ?? ''}",
                                              style: TextStyle(
                                                color: fromHex(blue),
                                                fontStyle: FontStyle.italic,
                                                fontSize: 12,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.access_time,
                                              color: fromHex(gold_dark),
                                              size: 16,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              "${model.myJobs[index]?.jobTime ?? ''}",
                                              style: TextStyle(
                                                color: fromHex(blue),
                                                fontStyle: FontStyle.italic,
                                                fontSize: 12,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Visibility(
                                              visible:
                                                  "${model.myJobs[index].status}" ==
                                                      "published",
                                              child: Icon(
                                                Icons.verified_rounded,
                                                color: Colors.green,
                                                size: 16,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Text(
                                              "${model.myJobs[index].status}",
                                              style: TextStyle(
                                                  color: fromHex(blue),
                                                  fontSize: 11,
                                                  fontStyle: FontStyle.italic),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ))
                  : Container(
                      child: Center(
                        child: Text(
                          'No jobs posted yet',
                          style: TextStyle(color: fromHex(blue)),
                        ),
                      ),
                    )
              : Container(
                  child: Center(
                    child: Text(
                      'An Error Occurred while fetching data, \n Check Internet Connection',
                      style: TextStyle(
                        color: fromHex(blue),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
          : Center(
              child: CircularProgressIndicator(
                backgroundColor: fromHex(gold_dark),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  fromHex(grey),
                ),
              ),
            ),
    );
  }

  _myJobsFeeds(BuildContext context) {
    return BaseView<JobsIPerformedModel>(
      onModelReady: (model) async {
        hasConnection = await checkConnection();
        if (!hasConnection) {
          showToast("Check Internet Connection");
          return;
        }
        Map<String, dynamic> data = {
          "page": "all",
          "is_active": true,
          "status": "completed"
        };
        await model.getMyContracts(data);
        setState(() {
          _model = model;
        });
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? model.contracts != null
              ? model.contracts.isNotEmpty
                  ? ListView.separated(
                      itemCount: model.contracts.length,
                      physics: BouncingScrollPhysics(),
                      separatorBuilder: (context, index) => Padding(
                            padding: EdgeInsets.symmetric(vertical: 5),
                            child: Divider(
                              indent: 15,
                              endIndent: 15,
                              color: fromHex(gold_lite),
                            ),
                          ),
                      itemBuilder: (context, index) => Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 5),
                            child: InkWell(
                              onTap: () {
                                _myContractsOptions(
                                    context, model.contracts[index], model);
                              },
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${model.contracts[index].job.title}",
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                            color: fromHex(gold_dark),
                                          ),
                                        ),
                                      ),
                                      // Row(
                                      //   children: [
                                      //     Icon(
                                      //       Icons.location_city,
                                      //       color: fromHex(gold_dark),
                                      //       size: 16,
                                      //     ),
                                      //     SizedBox(
                                      //       width: 5,
                                      //     ),
                                      Text(
                                        "${model.contracts[index]?.status ?? ''}",
                                        style: TextStyle(
                                          color: fromHex(blue),
                                          fontStyle: FontStyle.italic,
                                          fontSize: 12,
                                        ),
                                      )
                                      //   ],
                                      // )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "${model.contracts[index]?.job?.description ?? ''}",
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.black),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.calendar_today,
                                              color: fromHex(gold_dark),
                                              size: 16,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              "${model.contracts[index]?.job?.jobDate ?? ''}",
                                              style: TextStyle(
                                                color: fromHex(blue),
                                                fontStyle: FontStyle.italic,
                                                fontSize: 12,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.access_time,
                                              color: fromHex(gold_dark),
                                              size: 16,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              "${model.contracts[index]?.job?.jobTime ?? ''}",
                                              style: TextStyle(
                                                color: fromHex(blue),
                                                fontStyle: FontStyle.italic,
                                                fontSize: 12,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          "${Jiffy("${model.contracts[index]?.createdAt ?? ''}", dateFormat).fromNow()}",
                                          style: TextStyle(
                                            color: fromHex(blue),
                                            fontStyle: FontStyle.italic,
                                            fontSize: 12,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ))
                  : Container(
                      child: Center(
                        child: Text(
                          'No Contracts Awarded Yet',
                          style: TextStyle(color: fromHex(blue)),
                        ),
                      ),
                    )
              : Container(
                  child: Center(
                    child: Text(
                      'An Error Occurred while fetching data, \n Check Internet Connection',
                      style: TextStyle(
                        color: fromHex(blue),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
          : Center(
              child: CircularProgressIndicator(
                backgroundColor: fromHex(gold_dark),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  fromHex(grey),
                ),
              ),
            ),
    );
  }

  void _myContractsOptions(
      BuildContext context, Contracts contract, JobsIPerformedModel model) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        clipBehavior: Clip.antiAlias,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Container(
              color: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                  color: fromHex(blue),
                  // color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(40),
                      topRight: const Radius.circular(40)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      ListTile(
                        onTap: () async {
                          Navigator.pop(context);
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  ContractDetails(contract, "CONTRACT DETAILS"),
                            ),
                          );
                        },
                        title: Center(
                          child: Text(
                            'View Contract Details',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      ListTile(
                        onTap: () async {
                          hasConnection = await checkConnection();
                          if (!hasConnection) {
                            showToast("Check Internet Connection");
                            return;
                          }
                          _getBiddersRatings(
                              context, contract, "Rate Job Owner", model);
                          // Navigator.pop(context);
                        },
                        title: Center(
                          child: Text(
                            'Rate Job Owner',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  void _getBiddersRatings(
    BuildContext context,
    Contracts contract,
    String s,
    JobsIPerformedModel model,
  ) async {
    TextEditingController _description = TextEditingController();
    double rate = 0.0;
    await showDialog(
      context: _scaffoldKey.currentContext,
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.white,
        child: StatefulBuilder(
          builder: (context, StateSetter setState) => Container(
            color: Colors.transparent,
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Container(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "$s",
                      style: TextStyle(
                          color: fromHex(gold_lite),
                          fontWeight: FontWeight.w800,
                          fontSize: 18),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: RatingBar.builder(
                            initialRating: 0,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemSize: 30.7,
                            unratedColor: fromHex(grey),
                            itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: fromHex(gold_lite),
                            ),
                            onRatingUpdate: (rating) {
                              setState(() {
                                rate = rating;
                              });
                            },
                          ),
                        ),
                        Text("$rate")
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      child: TextFormField(
                        controller: _description,
                        keyboardType: TextInputType.text,
                        textAlignVertical: TextAlignVertical.top,
                        textCapitalization: TextCapitalization.sentences,
                        maxLines: 4,
                        style: TextStyle(color: fromHex(blue)),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Please Enter Review";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          alignLabelWithHint: true,
                          filled: true,
                          fillColor: Colors.white,
                          isDense: true,
                          hintText: "Enter Review here...",
                          labelText: "Review Comment",
                          labelStyle: TextStyle(color: fromHex(blue)),
                          hintStyle: TextStyle(
                            color: Colors.blueGrey[400],
                          ),
                          border: getBorder(),
                          focusedBorder: getBorder(),
                          enabledBorder: getBorder(),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        Map<String, dynamic> data = {
                          "rating": rate,
                          "comment": "${_description.text}"
                        };
                        _markAsFinished(context, contract, model, data);
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 34),
                        margin: EdgeInsets.symmetric(horizontal: 25),
                        decoration: BoxDecoration(
                          color: fromHex(blue),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'Submit',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _markAsFinished(BuildContext context, Contracts contract,
      JobsIPerformedModel model, Map<String, dynamic> data) async {
    try {
      var r = await model.markAsFinished(data, contract.id);
      showToast(r.status);
      Map<String, dynamic> data1 = {
        "page": "all",
        "is_active": true,
        "status": "completed"
      };
      await model.getMyContracts(data1);
      // Navigator.of(context).pop();
    } catch (e) {
      try {
        Map<String, dynamic> error = jsonDecode(e.toString());
        showToast(error['errors'][0]['message']);
      } catch (a) {}
    }
  }
}
