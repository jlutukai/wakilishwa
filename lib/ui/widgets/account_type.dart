import 'dart:convert';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/local/local_models.dart';
import 'package:wakilishwa/core/models/verify_advocate_response.dart';
import 'package:wakilishwa/core/viewmodels/account_type_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';
import 'package:wakilishwa/utils/constants.dart';

class AccountType extends StatefulWidget {
  final void Function(int index) onDataChange;
  AccountType(this.onDataChange);

  @override
  _AccountTypeState createState() => _AccountTypeState();
}

class _AccountTypeState extends State<AccountType> {
  bool isAdvocate = false;
  bool hasConnection;
  TextEditingController _admission = TextEditingController();
  // TextEditingController _accountType = TextEditingController();
  String accountTypeId = "";
  String accountTypeName = "";
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    _admission.text = getAddUserData()?.admissionNumber ?? '';
    accountTypeId = getAddUserData()?.accountType ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<AccountTypeModel>(
      onModelReady: (model) async {
        print('hre');
        Map<String, dynamic> data = {"page": "all", "is_active": true};
        hasConnection = await checkConnection();
        if (!hasConnection) {
          showToast("Check Internet Connection");
          return;
        }

        await model.getAccountTypes(data);
      },
      builder: (context, model, child) => Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: Column(
          children: [
            Expanded(
              child: model.state == ViewState.Idle
                  ? SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Select Account Type",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      color: fromHex(blue),
                                      fontSize: 20),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 25, vertical: 5),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: DropdownSearch<AccountTypeData>(
                                      mode: Mode.BOTTOM_SHEET,
                                      showClearButton: false,
                                      showSearchBox: true,
                                      searchBoxDecoration: InputDecoration(
                                          hintText: "Search Account Types",
                                          hintStyle: TextStyle(
                                              color: Colors.grey[100]),
                                          prefixIcon: Icon(Icons.search,
                                              color: Colors.white),
                                          filled: true,
                                          fillColor: Colors.white10,
                                          border: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          focusedBorder: InputBorder.none),
                                      dropdownSearchDecoration: InputDecoration(
                                          filled: true,
                                          isDense: true,
                                          contentPadding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 0),
                                          fillColor: Colors.transparent,
                                          border: getBorder(),
                                          enabledBorder: getBorder(),
                                          focusedBorder: getBorder()),
                                      items: model.accountTypes,
                                      itemAsString: (AccountTypeData p) =>
                                          "${p.name ?? "Undefined"}",
                                      autoValidateMode:
                                          AutovalidateMode.onUserInteraction,
                                      validator: (AccountTypeData u) {
                                        if (accountTypeName.isEmpty) {
                                          return "Please choose Account Type";
                                        }
                                        return null;
                                      },
                                      onChanged: (AccountTypeData data) {
                                        accountTypeId = data.id;
                                        accountTypeName = data.name;
                                        if (accountTypeName == "Advocate") {
                                          setState(() {
                                            isAdvocate = true;
                                          });
                                        }
                                      },
                                      dropdownBuilder: _accountTypeDrop,
                                      popupBackgroundColor: fromHex(blue),
                                      popupItemBuilder: _accountTypePopUp,
                                      dropDownButton: Icon(
                                        Icons.arrow_drop_down_circle,
                                        color: fromHex(gold_lite),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Visibility(
                              visible: isAdvocate,
                              child: Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 25, vertical: 5),
                                    child: TextFormField(
                                      controller: _admission,
                                      keyboardType: TextInputType.text,
                                      textCapitalization:
                                          TextCapitalization.characters,
                                      style: TextStyle(color: fromHex(blue)),
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return "Please Enter your admission number";
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Colors.white,
                                          isDense: true,
                                          hintText:
                                              "Enter your admission number",
                                          labelText: "Admission Number",
                                          labelStyle:
                                              TextStyle(color: fromHex(blue)),
                                          hintStyle: TextStyle(
                                            color: Colors.blueGrey[400],
                                          ),
                                          border: getBorder(),
                                          focusedBorder: getBorder(),
                                          enabledBorder: getBorder(),
                                          prefixText: "P.105/"),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  InkWell(
                                    onTap: () async {
                                      if (_formKey.currentState.validate()) {
                                        hasConnection = await checkConnection();
                                        if (!hasConnection) {
                                          showToast(
                                              "Check Internet Connection");
                                          return;
                                        }
                                        Map<String, dynamic> data = {
                                          "advocate_number":
                                              "P.105/${_admission.text}"
                                        };
                                        try {
                                          var r =
                                              await model.verifyAdvocate(data);
                                          if (model.advocate != null) {
                                            if(model.advocate.isActive) {
                                              _showDetails(
                                                  context, model.advocate);
                                            }else{
                                              showToast("Advocate Status Not Active");
                                            }
                                          }else{
                                            showToast("Advocate not found");
                                          }
                                        } catch (e) {
                                          try {
                                            Map<String, dynamic> error =
                                                jsonDecode(e.toString());
                                            showToast(
                                                error['errors'][0]['message']);
                                          } catch (a) {}
                                        }
                                      }
                                    },
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      padding: EdgeInsets.symmetric(
                                          vertical: 15, horizontal: 34),
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 25),
                                      decoration: BoxDecoration(
                                        color: fromHex(blue),
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(20),
                                          bottomLeft: Radius.circular(20),
                                          bottomRight: Radius.circular(20),
                                        ),
                                      ),
                                      child: Center(
                                        child: Text(
                                          'SEARCH',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 15,
                                              fontWeight: FontWeight.w900),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                          ],
                        ),
                      ),
                    )
                  : Center(
                      child: CircularProgressIndicator(
                        backgroundColor: fromHex(gold_dark),
                        valueColor: new AlwaysStoppedAnimation<Color>(
                          fromHex(grey),
                        ),
                      ),
                    ),
            ),
            Row(
              children: [
                Expanded(child: Container()),
                Expanded(child: Container())
              ],
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _showDetails(BuildContext context, Advocate advocate) async {
    await showDialog(
        context: context,
        child: Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          elevation: 0.0,
          backgroundColor: Colors.white,
          child: Container(
            color: Colors.transparent,
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Container(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 25,
                    ),
                    Text(
                      "FIRST NAME",
                      style: TextStyle(
                        color: fromHex(blue),
                        fontSize: 18,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "${advocate?.firstName ?? ''}",
                      style: TextStyle(
                        color: fromHex(grey),
                        fontSize: 18,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "OTHER NAMES",
                      style: TextStyle(
                        color: fromHex(blue),
                        fontSize: 18,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "${advocate?.otherNames ?? ''}",
                      style: TextStyle(
                        color: fromHex(grey),
                        fontSize: 18,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "ADVOCATE NUMBER",
                      style: TextStyle(
                        color: fromHex(blue),
                        fontSize: 18,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "${advocate?.adovateNumber ?? ''}",
                      style: TextStyle(
                        color: fromHex(grey),
                        fontSize: 18,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "PRACTISING YEAR",
                      style: TextStyle(
                        color: fromHex(blue),
                        fontSize: 18,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "${advocate?.practisingYear ?? ''}",
                      style: TextStyle(
                        color: fromHex(grey),
                        fontSize: 18,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 34),
                        margin: EdgeInsets.symmetric(horizontal: 25),
                        decoration: BoxDecoration(
                          color: fromHex(gold_dark),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'Cancel',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                        if (getAddUserData() == null) {
                          CreateAccountBody c = CreateAccountBody();
                          Name n = Name();
                          n.first = advocate.firstName;
                          n.others = advocate.otherNames;
                          c.admissionNumber = advocate.adovateNumber;
                          c.name = n;
                          c.accountType = accountTypeId;
                          setAddUserData(c);
                        } else {
                          CreateAccountBody c = getAddUserData();
                          Name n = Name();
                          n.first = advocate.firstName;
                          n.others = advocate.otherNames;
                          c.admissionNumber = advocate.adovateNumber;
                          c.name = n;
                          c.accountType = accountTypeId;
                          setAddUserData(c);
                        }
                        widget.onDataChange(1);
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 34),
                        margin: EdgeInsets.symmetric(horizontal: 25),
                        decoration: BoxDecoration(
                          color: fromHex(blue),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'Next',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  Widget _accountTypeDrop(
      BuildContext context, AccountTypeData item, String itemDesignation) {
    return Container(
      child: Text(
        accountTypeName.isNotEmpty
            ? "$accountTypeName"
            : "${item?.name ?? 'Select Account Type'}",
        style: TextStyle(
          color: fromHex(blue),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _accountTypePopUp(
      BuildContext context, AccountTypeData item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item.name ?? 'undefined',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }
}
