import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_alerts_response.dart';
import 'package:wakilishwa/core/viewmodels/alerts_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';
import 'package:wakilishwa/ui/pages/bid_details.dart';
import 'package:wakilishwa/ui/pages/bidder_profile.dart';
import 'package:wakilishwa/ui/pages/job_details.dart';

import '../../utils/constants.dart';

class Alerts extends StatefulWidget {
  @override
  _AlertsState createState() => _AlertsState();
}

class _AlertsState extends State<Alerts> {
  bool hasConnection;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/bg.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Scaffold(
        backgroundColor: fromHex(blue).withOpacity(0.7),
        body: Column(
          children: [
            Expanded(
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Column(
                      children: [
                        Text(
                          "Adv. ${getCurrentUser()?.name?.first ?? ''} ${getCurrentUser()?.name?.others ?? ''}",
                          style: TextStyle(
                            color: fromHex(gold_dark),
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          'Advocate',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding:
                  EdgeInsets.only(top: 45, right: 20, left: 20, bottom: 25),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40)),
                color: Colors.white,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Alerts',
                        style: TextStyle(
                            color: fromHex(gold_dark),
                            fontSize: 20,
                            fontWeight: FontWeight.w900),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                      constraints: BoxConstraints(
                          maxHeight: MediaQuery.of(context).size.height * 0.50),
                      child: _alerts(context))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _alerts(BuildContext context) {
    return BaseView<AlertsModel>(
      onModelReady: (model) async {
        hasConnection = await checkConnection();
        if (!hasConnection) {
          showToast("Check Internet Connection");
          return;
        }
        Map<String, dynamic> data = {"page": "all"};
        await model.getAlerts(data);
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? model.notifications != null
              ? model.notifications.isNotEmpty
                  ? ListView.separated(
                      itemCount: model.notifications.length,
                      physics: BouncingScrollPhysics(),
                      separatorBuilder: (context, index) => Divider(
                        indent: 15,
                        endIndent: 15,
                        color: fromHex(gold_lite),
                      ),
                      itemBuilder: (context, index) => Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                        child: InkWell(
                          onTap: () async {
                            hasConnection = await checkConnection();
                            if (!hasConnection) {
                              showToast("Check Internet Connection");
                              return;
                            }
                            Map<String, dynamic> data = {"page": "all"};
                            try {
                              var r = await model
                                  .readAlerts(model.notifications[index].id);
                              await model.getAlerts(data);
                              _goToNext(model.notifications[index], model);
                            } catch (e) {
                              try {
                                Map<String, dynamic> error =
                                    jsonDecode(e.toString());
                                showToast(error['errors'][0]['message']);
                              } catch (a) {}
                            }
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${model.notifications[index]?.notificationType ?? ''}"
                                    .capitalize(),
                                style: TextStyle(
                                  fontSize: 12,
                                  fontWeight:
                                      model.notifications[index].isActive
                                          ? FontWeight.w800
                                          : FontWeight.bold,
                                  color: fromHex(gold_dark),
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "${model.notifications[index]?.message ?? ''}",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                    fontWeight:
                                        model.notifications[index].isActive
                                            ? FontWeight.w800
                                            : FontWeight.normal),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    "${Jiffy("${model.notifications[index]?.createdAt ?? ''}", dateFormat).fromNow()}",
                                    style: TextStyle(
                                        color: fromHex(blue),
                                        fontStyle: FontStyle.italic,
                                        fontSize: 10,
                                        fontWeight: FontWeight.w100),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  : Center(
                      child: Text('No Notification yet'),
                    )
              : Center(
                  child: Text("An Error occurred while loading data"),
                )
          : Center(
              child: CircularProgressIndicator(
                backgroundColor: fromHex(gold_dark),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  fromHex(grey),
                ),
              ),
            ),
    );
  }

  void _goToNext(Notifications notification, AlertsModel model) {
    if (notification.notificationType == "job") {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) =>
              ViewJobDetails(s: "my_jobs", jobsData: notification.job),
        ),
      );
    }
    if (notification.notificationType == "bid") {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) =>
              JobBids(notification.bid, "JOB OWNER'S PROFILE"),
        ),
      );
    }
    if (notification.notificationType == "user") {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (BuildContext context) =>
              UserProfileDetails(notification.user, null),
        ),
      );
    }
  }
}
