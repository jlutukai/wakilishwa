import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/viewmodels/user_profile_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';
import 'package:wakilishwa/utils/constants.dart';

class UserProfile extends StatefulWidget {
  final void Function(int index, [int jobsIndex]) onDataChange;
  UserProfile(this.onDataChange);

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  bool hasConnection;
  @override
  Widget build(BuildContext context) {
    return BaseView<UserProfileModel>(
      onModelReady: (model) async {
        hasConnection = await checkConnection();
        await model.getCurrentUser();
        setActiveContracts(model.reports?.jobs?.inprogress?.toString() ?? '0');
        setArchivedJobs(model.reports?.jobs?.archived?.toString() ?? '0');
        setCurrentUser(model.user);
      },
      builder: (context, model, child) => Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Column(
            children: [
              Expanded(
                child: Container(),
              ),
              Container(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height * .65,
                  minHeight: MediaQuery.of(context).size.height * .35,
                ),
                padding:
                    EdgeInsets.only(top: 45, right: 20, left: 20, bottom: 25),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40)),
                  color: fromHex(blue),
                ),
                child: model.state == ViewState.Idle
                    ? SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Adv. ${model.user?.name?.first ?? ''} ${model.user?.name?.others ?? ''}",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                          color: fromHex(gold_dark),
                                        ),
                                      ),
                                      Text(
                                        'Advocate',
                                        style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.white,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                model.reports != null
                                    ? Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          Text(
                                            'Ratings  ',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 11,
                                            ),
                                          ),
                                          RatingBar.builder(
                                            initialRating: model.ratings ?? 0,
                                            minRating: 1,
                                            direction: Axis.horizontal,
                                            allowHalfRating: true,
                                            itemCount: 5,
                                            itemSize: 12.7,
                                            unratedColor: fromHex(grey),
                                            ignoreGestures: true,
                                            itemPadding: EdgeInsets.symmetric(
                                                horizontal: 2.0),
                                            itemBuilder: (context, _) => Icon(
                                              Icons.star,
                                              color: fromHex(gold_lite),
                                            ),
                                            onRatingUpdate: (rating) {
                                              print(rating);
                                            },
                                          ),
                                        ],
                                      )
                                    : Container()
                              ],
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 10.0),
                              child: Divider(
                                thickness: 1.5,
                                color: Colors.grey.shade800,
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      'About',
                                      style: TextStyle(
                                        color: fromHex(gold_dark),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 13,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  "${model.user?.description ?? ''}",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12),
                                )
                              ],
                            ),
                            model.reports != null
                                ? Column(
                                    children: [
                                      // Padding(
                                      //   padding: const EdgeInsets.symmetric(
                                      //       vertical: 10.0),
                                      //   child: Divider(
                                      //     thickness: 1.5,
                                      //     color: Colors.grey.shade800,
                                      //   ),
                                      // ),
                                      // InkWell(
                                      //   onTap: () {
                                      //     Navigator.of(context).pushNamed(
                                      //         ActiveContractsWidget.tag);
                                      //   },
                                      //   child: Row(
                                      //     mainAxisAlignment:
                                      //         MainAxisAlignment.spaceBetween,
                                      //     children: [
                                      //       Column(
                                      //         crossAxisAlignment:
                                      //             CrossAxisAlignment.start,
                                      //         children: [
                                      //           Text(
                                      //             'MY Contracts',
                                      //             style: TextStyle(
                                      //               color: Colors.white,
                                      //             ),
                                      //           ),
                                      //           SizedBox(
                                      //             height: 5,
                                      //           ),
                                      //           Row(
                                      //             children: [
                                      //               Icon(
                                      //                 Icons
                                      //                     .arrow_drop_down_circle,
                                      //                 color: Colors.white,
                                      //               ),
                                      //               SizedBox(
                                      //                 width: 5,
                                      //               ),
                                      //               Text(
                                      //                 'VIEW MY Contracts',
                                      //                 style: TextStyle(
                                      //                     color:
                                      //                         fromHex(gold_dark),
                                      //                     fontSize: 10),
                                      //               )
                                      //             ],
                                      //           )
                                      //         ],
                                      //       ),
                                      //       Text(
                                      //         "${model.reports?.jobs?.inprogress ?? ''}",
                                      //         style: TextStyle(
                                      //             fontSize: 30,
                                      //             color: Colors.white,
                                      //             fontWeight: FontWeight.w900),
                                      //       )
                                      //     ],
                                      //   ),
                                      // ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10.0),
                                        child: Divider(
                                          thickness: 1.5,
                                          color: Colors.grey.shade800,
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          widget.onDataChange(1, 2);
                                        },
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  'MY POSTED JOBS',
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Row(
                                                  children: [
                                                    Icon(
                                                      Icons
                                                          .arrow_drop_down_circle,
                                                      color: Colors.white,
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      'VIEW JOBS I POSTED',
                                                      style: TextStyle(
                                                          color: fromHex(
                                                              gold_dark),
                                                          fontSize: 10),
                                                    )
                                                  ],
                                                )
                                              ],
                                            ),
                                            Text(
                                              "${model.reports?.jobs?.posted ?? ''}",
                                              style: TextStyle(
                                                  fontSize: 30,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w900),
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10.0),
                                        child: Divider(
                                          thickness: 1.5,
                                          color: Colors.grey.shade800,
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          widget.onDataChange(3);
                                        },
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  'CONTRACTS',
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Row(
                                                  children: [
                                                    Icon(
                                                      Icons
                                                          .arrow_drop_down_circle,
                                                      color: Colors.white,
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Text(
                                                      'VIEW MY CONTRACTS',
                                                      style: TextStyle(
                                                          color: fromHex(
                                                              gold_dark),
                                                          fontSize: 10),
                                                    )
                                                  ],
                                                )
                                              ],
                                            ),
                                            Text(
                                              "${model.reports?.jobs?.completed ?? ''}",
                                              style: TextStyle(
                                                  fontSize: 30,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w900),
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10.0),
                                        child: Divider(
                                          thickness: 1.5,
                                          color: Colors.grey.shade800,
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                'FREE CREDITS',
                                                style: TextStyle(
                                                  color: Colors.white,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Row(
                                                children: [
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(
                                                    '',
                                                    style: TextStyle(
                                                        color:
                                                            fromHex(gold_dark),
                                                        fontSize: 10),
                                                  )
                                                ],
                                              )
                                            ],
                                          ),
                                          Text(
                                            "${model.user?.freeCredits?.amount?.truncate() ?? ''}",
                                            style: TextStyle(
                                                fontSize: 30,
                                                color: Colors.white,
                                                fontWeight: FontWeight.w900),
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: 23,
                                      ),
                                    ],
                                  )
                                : Container()
                          ],
                        ),
                      )
                    : Center(
                        child: CircularProgressIndicator(
                          backgroundColor: fromHex(gold_dark),
                          valueColor: new AlwaysStoppedAnimation<Color>(
                            fromHex(grey),
                          ),
                        ),
                      ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
