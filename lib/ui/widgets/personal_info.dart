import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:string_validator/string_validator.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/local/local_models.dart';
import 'package:wakilishwa/core/models/regions_response.dart';
import 'package:wakilishwa/core/models/towns_response.dart';
import 'package:wakilishwa/core/viewmodels/personal_info_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';

import '../../utils/constants.dart';

class PersonalInfo extends StatefulWidget {
  final void Function(int index) onDataChange;
  PersonalInfo(this.onDataChange);

  @override
  _PersonalInfoState createState() => _PersonalInfoState();
}

class _PersonalInfoState extends State<PersonalInfo> {
  bool hasConnection;
  TextEditingController _email = TextEditingController();
  TextEditingController _phone = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  String accountTypeId = "";
  String countryId = "";
  String regionId = "";
  String townId = "";

  String accountTypeName = "";
  String countryName = "";
  String countryCode = "+254";
  String regionName = "";
  String townName = "";

  @override
  void initState() {
    print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -    ${getAddUserData().toJson()}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        widget.onDataChange(1);
        return false;
      },
      child: BaseView<PersonalInfoModel>(
        onModelReady: (model) async {
          Map<String, dynamic> data = {"page": "all", "is_active": true};
          hasConnection = await checkConnection();
          if (!hasConnection) {
            showToast("Check Internet Connection");
            return;
          }

          await model.getRegions(data);
          await model.getCountries(data);
        },
        builder: (context, model, child) => Scaffold(
          backgroundColor: Colors.white,
          resizeToAvoidBottomInset: true,
          body: Column(
            children: [
              Expanded(
                  child: model.state == ViewState.Idle
                      ? SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "Personal Info",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w800,
                                          color: fromHex(blue),
                                          fontSize: 20),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                Column(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 25, vertical: 5),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: DropdownSearch<CountryData>(
                                              mode: Mode.BOTTOM_SHEET,
                                              showClearButton: false,
                                              showSearchBox: true,
                                              searchBoxDecoration:
                                                  InputDecoration(
                                                      hintText:
                                                          "Search Countries",
                                                      hintStyle: TextStyle(
                                                          color:
                                                              Colors.grey[100]),
                                                      prefixIcon: Icon(
                                                          Icons.search,
                                                          color: Colors.white),
                                                      filled: true,
                                                      fillColor: Colors.white10,
                                                      border: InputBorder.none,
                                                      enabledBorder:
                                                          InputBorder.none,
                                                      focusedBorder:
                                                          InputBorder.none),
                                              dropdownSearchDecoration:
                                                  InputDecoration(
                                                      filled: true,
                                                      isDense: true,
                                                      contentPadding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 10,
                                                              vertical: 0),
                                                      fillColor:
                                                          Colors.transparent,
                                                      border: getBorder(),
                                                      enabledBorder:
                                                          getBorder(),
                                                      focusedBorder:
                                                          getBorder()),
                                              items: model.countries,
                                              itemAsString: (CountryData p) =>
                                                  "${p.name ?? "Undefined"}",
                                              autoValidateMode: AutovalidateMode
                                                  .onUserInteraction,
                                              validator: (CountryData u) {
                                                if (countryName.isEmpty) {
                                                  return "Please choose country";
                                                }
                                                return null;
                                              },
                                              onChanged: (CountryData
                                                  countryData) async {
                                                countryId = countryData.id;
                                                countryCode =
                                                    countryData.dialCode;
                                                countryName = countryData.name;
                                              },
                                              dropdownBuilder: _countryDropDown,
                                              popupBackgroundColor:
                                                  fromHex(blue),
                                              popupItemBuilder: _countryPopUp,
                                              dropDownButton: Icon(
                                                Icons.arrow_drop_down_circle,
                                                color: fromHex(gold_lite),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 25, vertical: 5),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: DropdownSearch<RegionsData>(
                                              mode: Mode.BOTTOM_SHEET,
                                              showClearButton: false,
                                              showSearchBox: true,
                                              searchBoxDecoration:
                                                  InputDecoration(
                                                      hintText:
                                                          "Search Regions",
                                                      hintStyle: TextStyle(
                                                          color:
                                                              Colors.grey[100]),
                                                      prefixIcon: Icon(
                                                          Icons.search,
                                                          color: Colors.white),
                                                      filled: true,
                                                      fillColor: Colors.white10,
                                                      border: InputBorder.none,
                                                      enabledBorder:
                                                          InputBorder.none,
                                                      focusedBorder:
                                                          InputBorder.none),
                                              dropdownSearchDecoration:
                                                  InputDecoration(
                                                      filled: true,
                                                      isDense: true,
                                                      contentPadding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 10,
                                                              vertical: 0),
                                                      fillColor:
                                                          Colors.transparent,
                                                      border: getBorder(),
                                                      enabledBorder:
                                                          getBorder(),
                                                      focusedBorder:
                                                          getBorder()),
                                              items: model.regions,
                                              itemAsString: (RegionsData p) =>
                                                  "${p.name ?? "Undefined"}",
                                              autoValidateMode: AutovalidateMode
                                                  .onUserInteraction,
                                              validator: (RegionsData u) {
                                                if (regionName.isEmpty) {
                                                  return "Please choose region";
                                                }
                                                return null;
                                              },
                                              onChanged: (RegionsData
                                                  regionData) async {
                                                regionId = regionData.id;
                                                regionName = regionData.name;
                                                Map<String, dynamic> data = {
                                                  "page": "all",
                                                  "is_active": true,
                                                  "region": "${regionData.id}"
                                                };
                                                await model.getTowns(data);
                                                setState(() {
                                                  townId = "";
                                                  townName = "";
                                                });
                                              },
                                              dropdownBuilder: _regionDrop,
                                              popupBackgroundColor:
                                                  fromHex(blue),
                                              popupItemBuilder: _regionPopUp,
                                              dropDownButton: Icon(
                                                Icons.arrow_drop_down_circle,
                                                color: fromHex(gold_lite),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 25, vertical: 5),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: DropdownSearch<TownsData>(
                                              mode: Mode.BOTTOM_SHEET,
                                              showClearButton: false,
                                              showSearchBox: true,
                                              searchBoxDecoration:
                                                  InputDecoration(
                                                      hintText: "Search Town",
                                                      hintStyle: TextStyle(
                                                          color:
                                                              Colors.grey[100]),
                                                      prefixIcon: Icon(
                                                          Icons.search,
                                                          color: Colors.white),
                                                      filled: true,
                                                      fillColor: Colors.white10,
                                                      border: InputBorder.none,
                                                      enabledBorder:
                                                          InputBorder.none,
                                                      focusedBorder:
                                                          InputBorder.none),
                                              dropdownSearchDecoration:
                                                  InputDecoration(
                                                      filled: true,
                                                      isDense: true,
                                                      contentPadding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 10,
                                                              vertical: 0),
                                                      fillColor:
                                                          Colors.transparent,
                                                      border: getBorder(),
                                                      enabledBorder:
                                                          getBorder(),
                                                      focusedBorder:
                                                          getBorder()),
                                              items: model.towns,
                                              itemAsString: (TownsData p) =>
                                                  "${p.name ?? "Undefined"}",
                                              autoValidateMode: AutovalidateMode
                                                  .onUserInteraction,
                                              validator: (TownsData u) {
                                                if (townName.isEmpty) {
                                                  return "Please choose town";
                                                }
                                                return null;
                                              },
                                              onChanged: (TownsData townData) {
                                                townId = townData.id;
                                                townName = townData.name;
                                              },
                                              dropdownBuilder: _townDropDown,
                                              popupBackgroundColor:
                                                  fromHex(blue),
                                              popupItemBuilder: _townPopUp,
                                              dropDownButton: Icon(
                                                Icons.arrow_drop_down_circle,
                                                color: fromHex(gold_lite),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 25, vertical: 5),
                                      child: TextFormField(
                                        controller: _email,
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        style: TextStyle(color: fromHex(blue)),
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return "Please Enter your email address";
                                          }
                                          if (!isEmail(value)) {
                                            return "Please enter valid Email Address";
                                          }
                                          return null;
                                        },
                                        decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Colors.white,
                                          isDense: true,
                                          hintText: "Enter your email address",
                                          labelText: "Email",
                                          labelStyle:
                                              TextStyle(color: fromHex(blue)),
                                          hintStyle: TextStyle(
                                            color: Colors.blueGrey[400],
                                          ),
                                          border: getBorder(),
                                          focusedBorder: getBorder(),
                                          enabledBorder: getBorder(),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 25, vertical: 10),
                                      child: TextFormField(
                                        controller: _phone,
                                        keyboardType: TextInputType.phone,
                                        style: TextStyle(color: fromHex(blue)),
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return "Please Enter phone number";
                                          }
                                          if (value.length != 9) {
                                            return "Please Enter A Valid Phone Number";
                                          }
                                          return null;
                                        },
                                        maxLength: 9,
                                        decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Colors.white,
                                          isDense: true,
                                          hintText: "7** *** ***",
                                          labelText: "M-pesa Number",
                                          labelStyle:
                                              TextStyle(color: fromHex(blue)),
                                          hintStyle: TextStyle(
                                            color: fromHex(grey),
                                          ),
                                          border: getBorder(),
                                          focusedBorder: getBorder(),
                                          enabledBorder: getBorder(),
                                          prefix: Text(
                                            "${countryCode ?? ''}  ",
                                            style: TextStyle(
                                              fontSize: 16,
                                              color: fromHex(blue),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                              ],
                            ),
                          ),
                        )
                      : Center(
                          child: CircularProgressIndicator(
                            backgroundColor: fromHex(gold_dark),
                            valueColor: new AlwaysStoppedAnimation<Color>(
                              fromHex(grey),
                            ),
                          ),
                        )),
              Row(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        widget.onDataChange(1);
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 34),
                        margin: EdgeInsets.symmetric(horizontal: 25),
                        decoration: BoxDecoration(
                          color: fromHex(blue),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'BACK',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        if (_formKey.currentState.validate()) {
                          CreateAccountBody c = getAddUserData();
                          c.country = countryId;
                          c.phone = "$countryCode${_phone.text}";
                          c.town = "$townId";
                          c.region = "$regionId";
                          c.email = "${_email.text}";
                          setAddUserData(c);
                          widget.onDataChange(3);
                        }
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 34),
                        margin: EdgeInsets.symmetric(horizontal: 25),
                        decoration: BoxDecoration(
                          color: fromHex(blue),
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'NEXT',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _townDropDown(
      BuildContext context, TownsData item, String itemDesignation) {
    return Container(
      child: Text(
        townName.isNotEmpty ? "$townName" : "${item?.name ?? 'Select Town'}",
        style: TextStyle(
          color: fromHex(blue),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _townPopUp(BuildContext context, TownsData item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item.name ?? 'undefined',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _countryDropDown(
      BuildContext context, CountryData item, String itemDesignation) {
    return Container(
      child: Text(
        countryName.isNotEmpty
            ? "$countryName"
            : "${item?.name ?? 'Select Country'}",
        style: TextStyle(
          color: fromHex(blue),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _countryPopUp(
      BuildContext context, CountryData item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item.name ?? 'undefined',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _regionDrop(
      BuildContext context, RegionsData item, String itemDesignation) {
    return Container(
      child: Text(
        regionName.isNotEmpty
            ? "$regionName"
            : "${item?.name ?? 'Select Region'}",
        style: TextStyle(
          color: fromHex(blue),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _regionPopUp(BuildContext context, RegionsData item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item.name ?? 'undefined',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }
}
