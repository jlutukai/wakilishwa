import 'dart:convert';

import 'package:cupertino_tabbar/cupertino_tabbar.dart' as CupertinoTabBar;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jiffy/jiffy.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_jobs_response.dart';
import 'package:wakilishwa/core/viewmodels/jobs_feed_model.dart';
import 'package:wakilishwa/core/viewmodels/my_jobs_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';
import 'package:wakilishwa/ui/pages/job_details.dart';
import 'package:wakilishwa/ui/pages/post_new_job.dart';

import '../../locator.dart';
import '../../utils/constants.dart';

class JobsWidget extends StatefulWidget {
  final int jobsIndex;
  JobsWidget(this.jobsIndex);

  @override
  _JobsWidgetState createState() => _JobsWidgetState();
}

class _JobsWidgetState extends State<JobsWidget> {
  int cupertinoTabBarIValue = 0;
  int cupertinoTabBarIValueGetter() => cupertinoTabBarIValue;
  PageController _pageControlle;
  bool hasConnection;
  bool isSearching = false;
  bool enableSearch = false;
  JobsFeedModel _jobsFeedModel = locator<JobsFeedModel>();
  MyJobsModel _myJobsModel = locator<MyJobsModel>();
  TextEditingController _search = TextEditingController();
  List<JobsData> _jobs;

  @override
  void initState() {
    cupertinoTabBarIValue = widget.jobsIndex;
    _pageControlle = PageController(initialPage: widget.jobsIndex);
    super.initState();
  }

  @override
  void dispose() {
    _pageControlle.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Scaffold(
            backgroundColor: fromHex(blue).withOpacity(0.7),
            body: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: Container(
                height: MediaQuery.of(context).size.height - 60,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Column(
                            children: [
                              Text(
                                "Adv. ${getCurrentUser()?.name?.first ?? ''} ${getCurrentUser()?.name?.others ?? ''}",
                                style: TextStyle(
                                  color: fromHex(gold_dark),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Advocate',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 45, right: 20, left: 20, bottom: 25),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(40),
                            topRight: Radius.circular(40)),
                        color: Colors.white,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'JOBS',
                                style: TextStyle(
                                    color: fromHex(gold_dark),
                                    fontSize: 20,
                                    fontWeight: FontWeight.w900),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          CupertinoTabBar.CupertinoTabBar(
                            fromHex(gold_dark),
                            fromHex(blue),
                            [
                              Text(
                                "JOBS FEED",
                                style: GoogleFonts.montserrat(
                                  textStyle: TextStyle(
                                    color: cupertinoTabBarIValue == 0
                                        ? fromHex(gold_dark)
                                        : fromHex(blue),
                                    fontSize: 13.75,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                textAlign: TextAlign.center,
                              ),
                              Text(
                                "MY JOBS",
                                style: GoogleFonts.montserrat(
                                  textStyle: TextStyle(
                                    color: cupertinoTabBarIValue == 1
                                        ? fromHex(gold_dark)
                                        : fromHex(blue),
                                    fontSize: 13.75,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ],
                            cupertinoTabBarIValueGetter,
                            (int index) {
                              setState(() {
                                cupertinoTabBarIValue = index;
                                _pageControlle.animateToPage(index,
                                    duration: Duration(milliseconds: 1),
                                    curve: Curves.ease);
                              });
                            },
                            useSeparators: false,
                            allowScrollable: true,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 10),
                            constraints: BoxConstraints(
                                maxHeight:
                                    MediaQuery.of(context).size.height * 0.50 -
                                        40),
                            child: PageView(
                              controller: _pageControlle,
                              onPageChanged: (index) {
                                setState(() {
                                  cupertinoTabBarIValue = index;
                                });
                              },
                              children: <Widget>[
                                _jobsFeeds(context),
                                _myJobsFeeds(context)
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            floatingActionButton: Container(
              height: 40,
              width: 150,
              child: RaisedButton(
                elevation: 5.0,
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => PostNewJobPage(
                        isEditing: false,
                        refreshData: refreshData,
                      ),
                    ),
                  );
                },
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                ),
                color: fromHex(blue),
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Center(
                    child: Text(
                      'Add A Job',
                      style: TextStyle(color: fromHex(gold_dark), fontSize: 14),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        enableSearch
            ? Positioned(
                top: 20,
                left: 80,
                right: 10,
                child: Container(
                  // margin: EdgeInsets.only(right: 15),
                  child: TextFormField(
                    controller: _search,
                    keyboardType: TextInputType.text,
                    style: TextStyle(color: Colors.white),
                    onChanged: (value) async {
                      if (value.length > 3) {
                        hasConnection = await checkConnection();
                        if (!hasConnection) {
                          showToast("Check Internet Connection");
                          return;
                        }
                        Map<String, dynamic> data = {
                          "page": "all",
                          "is_active": true,
                          "status": "published",
                          "search": "$value"
                        };
                        print(data);
                        await _jobsFeedModel.getJobsFeeds(data);
                        setState(() {
                          if (_jobsFeedModel.jobsFeed != null) {
                            _jobs = List();
                            _jobs.addAll(_jobsFeedModel.jobsFeed);
                          }
                        });
                      }
                    },
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white24,
                        isDense: true,
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        hintText: "Search town, case, court...",
                        labelText: "Search Job",
                        labelStyle: TextStyle(color: fromHex(gold_dark)),
                        hintStyle: TextStyle(
                          color: Colors.blueGrey[400],
                        ),
                        border: getSearchBorder(),
                        focusedBorder: getSearchBorder(),
                        enabledBorder: getSearchBorder(),
                        prefixIcon: Icon(
                          Icons.search,
                          color: fromHex(gold_dark),
                        ),
                        suffixIcon: IconButton(
                          onPressed: () {
                            refreshData();
                            setState(() {
                              _search.text = "";
                              enableSearch = false;
                            });
                          },
                          icon: Icon(Icons.clear, color: fromHex(blue)),
                        )),
                  ),
                ),
              )
            : Positioned(
                right: 10,
                top: 20,
                child: FloatingActionButton(
                  foregroundColor: Colors.grey,
                  backgroundColor: Colors.white24,
                  heroTag: "Search",
                  onPressed: () {
                    setState(() {
                      enableSearch = true;
                    });
                  },
                  mini: true,
                  tooltip: "search",
                  child: Icon(
                    Icons.search,
                    color: fromHex(gold_dark),
                  ),
                ),
              )
      ],
    );
  }

  refreshData() async {
    hasConnection = await checkConnection();
    if (!hasConnection) {
      showToast("Check Internet Connection");
      return;
    }
    Map<String, dynamic> data = {
      "page": "all",
      "is_active": true,
      "status": "published"
    };
    await _jobsFeedModel.refreshData(data);

    setState(() {
      if (_jobsFeedModel.jobsFeed != null) {
        _jobs = List();
        _jobs.addAll(_jobsFeedModel.jobsFeed);
      }
    });

    Map<String, dynamic> data1 = {
      "page": "all",
      "is_active": true,
    };
    await _myJobsModel.refreshData(data1);
  }

  _jobsFeeds(BuildContext context) {
    return BaseView<JobsFeedModel>(
      onModelReady: (model) async {
        hasConnection = await checkConnection();
        if (!hasConnection) {
          showToast("Check Internet Connection");
          return;
        }
        Map<String, dynamic> data = {
          "page": "all",
          "is_active": true,
          "status": "published"
        };
        await model.getJobsFeeds(data);
        if (model.jobsFeed != null) {
          _jobs = List();
          _jobs.addAll(model.jobsFeed);
        }
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? _jobs != null
              ? _jobs.isNotEmpty
                  ? ListView.separated(
                      itemCount: _jobs.length,
                      physics: BouncingScrollPhysics(),
                      separatorBuilder: (context, index) => Padding(
                        padding: EdgeInsets.symmetric(vertical: 5),
                        child: Divider(
                          indent: 15,
                          endIndent: 15,
                          color: fromHex(gold_lite),
                        ),
                      ),
                      itemBuilder: (context, index) => Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    ViewJobDetails(
                                        s: "jobs_feed",
                                        jobsData: _jobs[index],
                                        refreshData: refreshData,
                                        bidOnAJob: bidOnAJob),
                              ),
                            );
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      "${_jobs[index].title}",
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                        color: fromHex(gold_dark),
                                      ),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.location_city,
                                        color: fromHex(gold_dark),
                                        size: 16,
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        "${_jobs[index]?.town?.name ?? ''}",
                                        style: TextStyle(
                                          color: fromHex(blue),
                                          fontStyle: FontStyle.italic,
                                          fontSize: 12,
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "${_jobs[index].description}",
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: 12, color: Colors.black),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: Row(
                                      children: [
                                        Icon(
                                          Icons.calendar_today,
                                          color: fromHex(gold_dark),
                                          size: 16,
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          "${_jobs[index]?.jobDate ?? ''}",
                                          style: TextStyle(
                                            color: fromHex(blue),
                                            fontStyle: FontStyle.italic,
                                            fontSize: 12,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Row(
                                      children: [
                                        Icon(
                                          Icons.access_time,
                                          color: fromHex(gold_dark),
                                          size: 16,
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          "${_jobs[index]?.jobTime ?? ''}",
                                          style: TextStyle(
                                            color: fromHex(blue),
                                            fontStyle: FontStyle.italic,
                                            fontSize: 12,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Text(
                                      "${Jiffy("${_jobs[index]?.createdAt ?? ''}", dateFormat).fromNow()}",
                                      style: TextStyle(
                                        color: fromHex(blue),
                                        fontStyle: FontStyle.italic,
                                        fontSize: 12,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  : Container(
                      child: Center(
                        child: Text(
                          'No Jobs found',
                          style: TextStyle(color: fromHex(blue)),
                        ),
                      ),
                    )
              : Container(
                  child: Center(
                    child: Text(
                      'An Error Occurred while fetching data, \n Check Internet Connection',
                      style: TextStyle(
                        color: fromHex(blue),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
          : Center(
              child: CircularProgressIndicator(
                backgroundColor: fromHex(gold_dark),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  fromHex(grey),
                ),
              ),
            ),
    );
  }

  _myJobsFeeds(BuildContext context) {
    return BaseView<MyJobsModel>(
      onModelReady: (model) async {
        hasConnection = await checkConnection();
        if (!hasConnection) {
          showToast("Check Internet Connection");
          return;
        }
        Map<String, dynamic> data = {"page": "all", "is_active": true};
        await model.getDraftAndPublishedJobs(data);
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? model.myJobs != null
              ? model.myJobs.isNotEmpty
                  ? ListView.separated(
                      itemCount: model.myJobs.length,
                      physics: BouncingScrollPhysics(),
                      separatorBuilder: (context, index) => Padding(
                            padding: EdgeInsets.symmetric(vertical: 5),
                            child: Divider(
                              indent: 15,
                              endIndent: 15,
                              color: fromHex(gold_lite),
                            ),
                          ),
                      itemBuilder: (context, index) => Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 5),
                            child: InkWell(
                              onTap: () {
                                if (model.myJobs[index].status != "archived") {
                                  _myJobOptions(
                                      context, model.myJobs[index], model);
                                }
                              },
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${model.myJobs[index].title}",
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                            color: fromHex(gold_dark),
                                          ),
                                        ),
                                      ),
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.location_city,
                                            color: fromHex(gold_dark),
                                            size: 16,
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text(
                                            "${model.myJobs[index]?.town?.name ?? ''}",
                                            style: TextStyle(
                                              color: fromHex(blue),
                                              fontStyle: FontStyle.italic,
                                              fontSize: 12,
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "${model.myJobs[index].description}",
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.black),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.calendar_today,
                                              color: fromHex(gold_dark),
                                              size: 16,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              "${model.myJobs[index]?.jobDate ?? ''}",
                                              style: TextStyle(
                                                color: fromHex(blue),
                                                fontStyle: FontStyle.italic,
                                                fontSize: 12,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.access_time,
                                              color: fromHex(gold_dark),
                                              size: 16,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              "${model.myJobs[index]?.jobTime ?? ''}",
                                              style: TextStyle(
                                                color: fromHex(blue),
                                                fontStyle: FontStyle.italic,
                                                fontSize: 12,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Visibility(
                                              visible:
                                                  "${model.myJobs[index].status}" ==
                                                      "published",
                                              child: Icon(
                                                Icons.verified_rounded,
                                                color: Colors.green,
                                                size: 16,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Text(
                                              "${model.myJobs[index].status}",
                                              style: TextStyle(
                                                  color: fromHex(blue),
                                                  fontSize: 11,
                                                  fontStyle: FontStyle.italic),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ))
                  : Container(
                      child: Center(
                        child: Text(
                          'No jobs posted yet',
                          style: TextStyle(color: fromHex(blue)),
                        ),
                      ),
                    )
              : Container(
                  child: Center(
                    child: Text(
                      'An Error Occurred while fetching data, \n Check Internet Connection',
                      style: TextStyle(
                        color: fromHex(blue),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
          : Center(
              child: CircularProgressIndicator(
                backgroundColor: fromHex(gold_dark),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  fromHex(grey),
                ),
              ),
            ),
    );
  }

  _myJobOptions(BuildContext context, JobsData myJob, MyJobsModel model) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        clipBehavior: Clip.antiAlias,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Container(
              color: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                  color: fromHex(blue),
                  // color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(40),
                      topRight: const Radius.circular(40)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      ListTile(
                        onTap: () async {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  ViewJobDetails(s: "my_jobs", jobsData: myJob),
                            ),
                          );
                        },
                        title: Center(
                          child: Text(
                            'View Job Details',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      myJob.status == "published"
                          ? Container()
                          : ListTile(
                              onTap: () async {
                                hasConnection = await checkConnection();
                                if (!hasConnection) {
                                  showToast("Check Internet Connection");
                                  return;
                                }
                                _publishJob(context, myJob, model);
                                Navigator.pop(context);
                              },
                              title: Center(
                                child: Text(
                                  'Publish Job',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                      myJob.status == "published"
                          ? Container()
                          : ListTile(
                              onTap: () async {
                                Navigator.pop(context);
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        PostNewJobPage(
                                            isEditing: true,
                                            myJob: myJob,
                                            refreshData: refreshData),
                                  ),
                                );
                              },
                              title: Center(
                                child: Text(
                                  'Edit Job Details',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                      ListTile(
                        onTap: () async {
                          hasConnection = await checkConnection();
                          if (!hasConnection) {
                            showToast("Check Internet Connection");
                            return;
                          }
                          Navigator.pop(context);
                          _deactivateJob(context, myJob, model);
                        },
                        title: Center(
                          child: Text(
                            myJob.status == "published"
                                ? 'UnPublish Job'
                                : 'Delete Job',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  void _jobsFeedsOptions(
      BuildContext context, JobsData jobsFeed, JobsFeedModel model) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        clipBehavior: Clip.antiAlias,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Container(
              color: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                  color: fromHex(blue),
                  // color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(40),
                      topRight: const Radius.circular(40)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      ListTile(
                        onTap: () async {
                          Navigator.pop(context);
                          // Navigator.of(context).push(
                          //   MaterialPageRoute(
                          //     builder: (BuildContext context) => ViewJobDetails(
                          //         "jobs_feed", jobsFeed, refreshData),
                          //   ),
                          // );
                        },
                        title: Center(
                          child: Text(
                            'View Job Details',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      getCurrentUser().id != jobsFeed.user.id
                          ? ListTile(
                              onTap: () async {
                                hasConnection = await checkConnection();
                                if (!hasConnection) {
                                  showToast("Check Internet Connection");
                                  return;
                                }
                                // _bidOnAJob(context, jobsFeed, model);
                                Navigator.pop(context);
                              },
                              title: Center(
                                child: Text(
                                  'Bid On Job',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            )
                          : Container(),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  Future<void> _publishJob(
      BuildContext context, JobsData myJob, MyJobsModel model) async {
    Map<String, dynamic> data = {
      "number_of_bids": myJob.numberOfBids,
      "status": "published",
      "title": "${myJob.title}",
      "court": "${myJob.court.id}",
      "town": "${myJob.town.id}",
      "magistrate_or_judge": "${myJob.magistrateOrJudge}",
      "court_room": "${myJob.courtRoom}",
      "job_date": "${myJob.jobDate}",
      "additional_fees": myJob.additionalFees,
      "job_time": "${myJob.jobTime}",
      "brief_type": "${myJob.briefType.id}",
      "description": "${myJob.description}",
      "created_at": "${myJob.createdAt}",
      "is_active": true,
      "user": "${getCurrentUser().id}"
    };
    print(data);
    try {
      var r = await model.editJob(data, myJob.id);
      showToast(r.status);
      Map<String, dynamic> data1 = {"page": "all", "is_active": true};
      await model.getDraftAndPublishedJobs(data1);

      refreshData();
      // Navigator.of(context).pop();
    } catch (e) {
      try {
        Map<String, dynamic> error = jsonDecode(e.toString());
        showToast(error['errors'][0]['message']);
      } catch (a) {}
    }
  }

  Future<void> _deactivateJob(
      BuildContext context, JobsData myJob, MyJobsModel model) async {
    Map<String, dynamic> data = {
      "number_of_bids": myJob.numberOfBids,
      "status": "unpublished",
      "title": "${myJob.title}",
      "court": "${myJob.court.id}",
      "town": "${myJob.town.id}",
      "magistrate_or_judge": "${myJob.magistrateOrJudge}",
      "court_room": "${myJob.courtRoom}",
      "job_date": "${myJob.jobDate}",
      "additional_fees": myJob.additionalFees,
      "job_time": "${myJob.jobTime}",
      "brief_type": "${myJob.briefType.id}",
      "description": "${myJob.description}",
      "created_at": "${myJob.createdAt}",
      "is_active": false,
      "user": "${getCurrentUser().id}"
    };
    print(data);
    try {
      var r = await model.editJob(data, myJob.id);
      Map<String, dynamic> data1 = {"page": "all", "is_active": true};
      await model.getDraftAndPublishedJobs(data1);
      showToast(r.status);
      refreshData();
      // Navigator.of(context).pop();
    } catch (e) {
      try {
        Map<String, dynamic> error = jsonDecode(e.toString());
        showToast(error['errors'][0]['message']);
      } catch (a) {}
    }
  }

  Future<void> bidOnAJob(BuildContext context, JobsData jobsFeed) async {
    try {
      var r = await _jobsFeedModel.bidOnJob(jobsFeed.id);
      showToast(r.message);
      refreshData();
    } catch (e) {
      try {
        Map<String, dynamic> error = jsonDecode(e.toString());
        showToast(error['errors'][0]['message']);
      } catch (a) {}
    }
  }
}
