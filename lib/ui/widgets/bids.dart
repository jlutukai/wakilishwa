import 'package:cupertino_tabbar/cupertino_tabbar.dart' as CupertinoTabBar;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_jobs_response.dart';
import 'package:wakilishwa/core/models/get_my_bids_response.dart';
import 'package:wakilishwa/core/viewmodels/my_bids_model.dart';
import 'package:wakilishwa/core/viewmodels/my_jobs_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';
import 'package:wakilishwa/ui/pages/bid_details.dart';
import 'package:wakilishwa/ui/pages/job_details.dart';

import '../../utils/constants.dart';

class Bids extends StatefulWidget {
  @override
  _BidsState createState() => _BidsState();
}

class _BidsState extends State<Bids> {
  int cupertinoTabBarIValue = 0;
  int cupertinoTabBarIValueGetter() => cupertinoTabBarIValue;
  PageController _pageControlle;
  bool hasConnection;

  @override
  void initState() {
    _pageControlle = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    _pageControlle.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/bg.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Scaffold(
        backgroundColor: fromHex(blue).withOpacity(0.7),
        body: Column(
          children: [
            Expanded(
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Column(
                      children: [
                        Text(
                          "Adv. ${getCurrentUser()?.name?.first ?? ''} ${getCurrentUser()?.name?.others ?? ''}",
                          style: TextStyle(
                            color: fromHex(gold_dark),
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          'Advocate',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding:
                  EdgeInsets.only(top: 45, right: 20, left: 20, bottom: 25),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40)),
                color: Colors.white,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'BIDS',
                        style: TextStyle(
                            color: fromHex(gold_dark),
                            fontSize: 20,
                            fontWeight: FontWeight.w900),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  CupertinoTabBar.CupertinoTabBar(
                    fromHex(gold_dark),
                    fromHex(blue),
                    [
                      Text(
                        "JOBS I POSTED",
                        style: TextStyle(
                          color: cupertinoTabBarIValue == 0
                              ? fromHex(gold_dark)
                              : fromHex(blue),
                          fontSize: 13.75,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        "JOBS I BID FOR",
                        style: TextStyle(
                          color: cupertinoTabBarIValue == 1
                              ? fromHex(gold_dark)
                              : fromHex(blue),
                          fontSize: 13.75,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                    cupertinoTabBarIValueGetter,
                    (int index) {
                      setState(() {
                        cupertinoTabBarIValue = index;
                        _pageControlle.animateToPage(index,
                            duration: Duration(milliseconds: 1),
                            curve: Curves.ease);
                      });
                    },
                    useSeparators: false,
                    allowScrollable: true,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10),
                    constraints: BoxConstraints(
                        maxHeight:
                            MediaQuery.of(context).size.height * 0.50 - 40),
                    child: PageView(
                      controller: _pageControlle,
                      onPageChanged: (index) {
                        setState(() {
                          cupertinoTabBarIValue = index;
                        });
                      },
                      children: <Widget>[
                        _jobsIPosted(context),
                        _jobsIBidFor(context)
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _jobsIPosted(BuildContext context) {
    return BaseView<MyJobsModel>(
      onModelReady: (model) async {
        hasConnection = await checkConnection();
        Map<String, dynamic> data = {
          "page": "all",
          "is_active": true,
          "status": "published"
        };
        await model.getMyBidJobs(data);
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? model.myJobs != null
              ? model.myJobs.isNotEmpty
                  ? ListView.separated(
                      itemCount: model.myJobs.length,
                      physics: BouncingScrollPhysics(),
                      separatorBuilder: (context, index) => Padding(
                            padding: EdgeInsets.symmetric(vertical: 5),
                            child: Divider(
                              indent: 15,
                              endIndent: 15,
                              color: fromHex(gold_lite),
                            ),
                          ),
                      itemBuilder: (context, index) => Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 5),
                            child: InkWell(
                              onTap: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        ViewJobDetails(
                                            s: "my_jobs",
                                            jobsData: model.myJobs[index]),
                                  ),
                                );
                              },
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${model.myJobs[index].title}",
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                            color: fromHex(gold_dark),
                                          ),
                                        ),
                                      ),
                                      Row(
                                        children: [
                                          Icon(
                                            Icons.location_city,
                                            color: fromHex(gold_dark),
                                            size: 16,
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text(
                                            "${model.myJobs[index]?.town?.name ?? ''}",
                                            style: TextStyle(
                                              color: fromHex(blue),
                                              fontStyle: FontStyle.italic,
                                              fontSize: 12,
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "${model.myJobs[index].description}",
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.black),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.calendar_today,
                                              color: fromHex(gold_dark),
                                              size: 16,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              "${model.myJobs[index]?.jobDate ?? ''}",
                                              style: TextStyle(
                                                color: fromHex(blue),
                                                fontStyle: FontStyle.italic,
                                                fontSize: 12,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.access_time,
                                              color: fromHex(gold_dark),
                                              size: 16,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              "${model.myJobs[index]?.jobTime ?? ''}",
                                              style: TextStyle(
                                                color: fromHex(blue),
                                                fontStyle: FontStyle.italic,
                                                fontSize: 12,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ))
                  : Container(
                      child: Center(
                        child: Text(
                          'No jobs posted yet',
                          style: TextStyle(color: fromHex(blue)),
                        ),
                      ),
                    )
              : Container(
                  child: Center(
                    child: Text(
                      "An Error Occurred while fetching data, \n Check Internet Connection ",
                      style: TextStyle(
                        color: fromHex(blue),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
          : Center(
              child: CircularProgressIndicator(
                backgroundColor: fromHex(gold_dark),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  fromHex(grey),
                ),
              ),
            ),
    );
  }

  _jobsIBidFor(BuildContext context) {
    return BaseView<MyJobBidsModel>(
      onModelReady: (model) async {
        hasConnection = await checkConnection();
        Map<String, dynamic> data = {
          "page": "all",
          "is_active": true,
          "status": "active"
        };
        await model.getMyBids(data);
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? model.jobBids != null
              ? model.jobBids.isNotEmpty
                  ? ListView.separated(
                      itemCount: model.jobBids.length,
                      physics: BouncingScrollPhysics(),
                      separatorBuilder: (context, index) => Padding(
                            padding: EdgeInsets.symmetric(vertical: 5),
                            child: Divider(
                              indent: 15,
                              endIndent: 15,
                              color: fromHex(gold_lite),
                            ),
                          ),
                      itemBuilder: (context, index) => Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 5),
                            child: InkWell(
                              onTap: () {
                                _myJobBidsOptions(
                                    context, model.jobBids[index], model);
                              },
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${model.jobBids[index]?.job?.title ?? ''}",
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                            color: fromHex(gold_dark),
                                          ),
                                        ),
                                      ),
                                      // Row(
                                      //   children: [
                                      //     Icon(
                                      //       Icons.location_city,
                                      //       color: fromHex(gold_dark),
                                      //       size: 16,
                                      //     ),
                                      //     SizedBox(
                                      //       width: 5,
                                      //     ),
                                      //     Text(
                                      //       "${model.jobBids[index]?.job?.town?.name ?? ''}",
                                      //       style: TextStyle(
                                      //         color: fromHex(blue),
                                      //         fontStyle: FontStyle.italic,
                                      //         fontSize: 12,
                                      //       ),
                                      //     )
                                      //   ],
                                      // )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "${model.jobBids[index]?.job?.description}",
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.black),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.calendar_today,
                                              color: fromHex(gold_dark),
                                              size: 16,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              "${model.jobBids[index]?.job?.jobDate ?? ''}",
                                              style: TextStyle(
                                                color: fromHex(blue),
                                                fontStyle: FontStyle.italic,
                                                fontSize: 12,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.access_time,
                                              color: fromHex(gold_dark),
                                              size: 16,
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Text(
                                              "${model.jobBids[index]?.job?.jobTime ?? ''}",
                                              style: TextStyle(
                                                color: fromHex(blue),
                                                fontStyle: FontStyle.italic,
                                                fontSize: 12,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ))
                  : Container(
                      child: Center(
                        child: Text(
                          'No bids done yet',
                          style: TextStyle(color: fromHex(blue)),
                        ),
                      ),
                    )
              : Container(
                  child: Center(
                    child: Text(
                      'An Error Occurred while fetching data, \n Check Internet Connection',
                      style: TextStyle(
                        color: fromHex(blue),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
          : Center(
              child: CircularProgressIndicator(
                backgroundColor: fromHex(gold_dark),
                valueColor: new AlwaysStoppedAnimation<Color>(
                  fromHex(grey),
                ),
              ),
            ),
    );
  }

  void _myJobOptions(BuildContext context, JobsData myJob, MyJobsModel model) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        clipBehavior: Clip.antiAlias,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Container(
              color: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                  color: fromHex(blue),
                  // color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(40),
                      topRight: const Radius.circular(40)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      ListTile(
                        onTap: () async {
                          Navigator.pop(context);
                        },
                        title: Center(
                          child: Text(
                            'View Bid Details',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  void _myJobBidsOptions(
      BuildContext context, BidsData jobBid, MyJobBidsModel model) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        clipBehavior: Clip.antiAlias,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Container(
              color: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                  color: fromHex(blue),
                  // color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(40),
                      topRight: const Radius.circular(40)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      ListTile(
                        onTap: () async {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  JobBids(jobBid, "JOB OWNER'S PROFILE"),
                            ),
                          );
                        },
                        title: Center(
                          child: Text(
                            'View Bid Details',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      ListTile(
                        onTap: () async {
                          hasConnection = await checkConnection();
                          if (!hasConnection) {
                            showToast("Check Internet Connection");
                            return;
                          }
                          Navigator.pop(context);
                          _cancelBid(context, jobBid, model);
                        },
                        title: Center(
                          child: Text(
                            'Cancel Bid',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  Future<void> _cancelBid(
      BuildContext context, BidsData jobBid, MyJobBidsModel model) async {
    Map<String, dynamic> data = {
      "status": "cancelled",
    };
    print(data);
    try {
      var r = await model.cancelBid(data, jobBid.id);
      Map<String, dynamic> data1 = {
        "page": "all",
        "is_active": true,
        "status": "active"
      };
      await model.getMyBids(data1);
      showToast(r.status);
    } catch (e) {
      showToast(e.toString());
    }
  }
}
