import 'package:flutter/material.dart';
import 'package:wakilishwa/core/models/get_transactions_response.dart';
import 'package:wakilishwa/utils/constants.dart';

class TransactionsReport extends StatefulWidget {
  final List<Transactions> transactions;
  TransactionsReport(this.transactions);

  @override
  _TransactionsReportState createState() => _TransactionsReportState();
}

class _TransactionsReportState extends State<TransactionsReport> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.white,
          resizeToAvoidBottomInset: true,
          body: Column(
            children: [
              Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height * 0.25,
                    margin: EdgeInsets.only(bottom: 60),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: fromHex(blue),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(40),
                        bottomRight: Radius.circular(40),
                      ),
                    ),
                  ),
                  Positioned(
                      bottom: 1,
                      right: 0,
                      left: 0,
                      child: CircleAvatar(
                        radius: 60,
                        backgroundColor: Colors.white,
                        child: CircleAvatar(
                          radius: 55,
                          backgroundColor: fromHex(gold_lite),
                          child: Icon(
                            Icons.receipt,
                            color: Colors.white,
                            size: 66,
                          ),
                        ),
                      ))
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'TRANSACTIONS REPORT',
                style: TextStyle(
                    color: fromHex(blue),
                    fontSize: 20,
                    fontWeight: FontWeight.w900),
              ),
              SizedBox(
                height: 20,
              ),
              Expanded(child: _transactions(context)),
            ],
          ),
        ),
        Positioned(
          left: 20,
          top: 35,
          child: FloatingActionButton(
            foregroundColor: Colors.grey,
            backgroundColor: fromHex(blue),
            onPressed: () {
              Navigator.of(context).pop();
            },
            mini: true,
            tooltip: "go back",
            child: Icon(
              Icons.keyboard_backspace_sharp,
              color: fromHex(gold_dark),
            ),
          ),
        ),
      ],
    );
  }

  _transactions(BuildContext context) {
    return widget.transactions != null
        ? widget.transactions.isNotEmpty
            ? ListView.separated(
                itemCount: widget.transactions.length,
                physics: BouncingScrollPhysics(),
                separatorBuilder: (context, index) => Divider(
                      indent: 15,
                      endIndent: 15,
                      color: fromHex(gold_lite),
                    ),
                itemBuilder: (context, index) => Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${widget.transactions[index]?.createdAt?.substring(0, 10) ?? ''}",
                            style: TextStyle(
                                color: fromHex(gold_lite),
                                fontSize: 12,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.arrow_drop_down_circle_rounded,
                                color: fromHex(gold_lite),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: Text(
                                  "${widget.transactions[index]?.transactionType ?? ''}"
                                      .capitalize(),
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: fromHex(blue),
                                  ),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                              Text(
                                "${cf.format(widget.transactions[index]?.amount ?? 0)}",
                                style: TextStyle(
                                    color: fromHex(blue),
                                    fontSize: 15,
                                    fontWeight: FontWeight.w900),
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                "${widget.transactions[index].status}",
                                style: TextStyle(
                                    color: fromHex(blue),
                                    fontWeight: FontWeight.w100,
                                    fontSize: 10,
                                    fontStyle: FontStyle.italic),
                              )
                            ],
                          )
                        ],
                      ),
                    ))
            : Container(
                height: 150,
                child: Center(
                  child: Text("No Transactions Found"),
                ),
              )
        : Container(
            height: 150,
            child: Center(
              child: Text("An error occurred while loading the data"),
            ),
          );
  }
}
