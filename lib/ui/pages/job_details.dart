import 'package:flutter/material.dart';
import 'package:wakilishwa/core/models/get_jobs_response.dart';

import '../../utils/constants.dart';
import 'bidder_profile.dart';
import 'job_bids_page.dart';

class ViewJobDetails extends StatefulWidget {
  final String s;
  final JobsData jobsData;
  final Function() refreshData;
  final Future<void> Function(BuildContext context, JobsData jobsFeed)
      bidOnAJob;
  ViewJobDetails({this.s, this.jobsData, this.refreshData, this.bidOnAJob});

  // static const tag = 'bid_view_page';
  @override
  _ViewJobDetailsState createState() => _ViewJobDetailsState();
}

class _ViewJobDetailsState extends State<ViewJobDetails> {
  //jobs_feed
  //my_jobs

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Scaffold(
            backgroundColor: fromHex(blue).withOpacity(0.7),
            body: Column(
              children: [
                Expanded(
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Column(
                          children: [
                            Text(
                              'JOB DETAILS',
                              style: TextStyle(
                                color: fromHex(gold_dark),
                                fontWeight: FontWeight.w800,
                                fontSize: 28,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding:
                      EdgeInsets.only(top: 45, right: 20, left: 20, bottom: 25),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40)),
                    color: Colors.white,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      // SizedBox(
                      //   height: 15,
                      // ),
                      Container(
                        constraints: BoxConstraints(
                            maxHeight:
                                MediaQuery.of(context).size.height * 0.55),
                        child: widget.jobsData != null
                            ? SingleChildScrollView(
                                physics: BouncingScrollPhysics(),
                                child: Column(
                                  children: [
                                    widget.s == "jobs_feed" &&
                                            getCurrentUser().id !=
                                                "${widget.jobsData.user.id}"
                                        ? InkWell(
                                            onTap: () {
                                              Navigator.of(context).push(
                                                MaterialPageRoute(
                                                  builder: (BuildContext
                                                          context) =>
                                                      UserProfileDetails(
                                                          widget.jobsData.user,
                                                          "JOB OWNER'S PROFILE"),
                                                ),
                                              );
                                            },
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Job Owner's Profile",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    color: fromHex(gold_dark),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                        "Check out Job Owner's Profile",
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),
                                                    Icon(
                                                      Icons
                                                          .arrow_drop_down_circle,
                                                      color: fromHex(gold_dark),
                                                    )
                                                  ],
                                                )
                                              ],
                                            ),
                                          )
                                        : Container(),
                                    widget.s == "jobs_feed" &&
                                            getCurrentUser().id !=
                                                "${widget.jobsData.user.id}"
                                        ? Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 5.0),
                                            child: Divider(
                                              thickness: .5,
                                              color: Colors.grey.shade800,
                                            ),
                                          )
                                        : Container(),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${widget.jobsData?.title ?? ''}",
                                          style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            color: fromHex(gold_dark),
                                          ),
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Text(
                                                "${widget.jobsData?.description ?? ''}",
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    widget.jobsData.status == "published" &&
                                            widget.s == "my_jobs"
                                        ? Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 5.0),
                                            child: Divider(
                                              thickness: .5,
                                              color: Colors.grey.shade800,
                                            ),
                                          )
                                        : Container(),
                                    widget.jobsData.status == "published" &&
                                            widget.s == "my_jobs"
                                        ? InkWell(
                                            onTap: () {
                                              Navigator.of(context).push(
                                                MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          JobBidsPage(widget
                                                              .jobsData.id),
                                                ),
                                              );
                                            },
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Bids on the Job",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    color: fromHex(gold_dark),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Icon(
                                                      Icons
                                                          .arrow_drop_down_circle,
                                                      color: fromHex(gold_dark),
                                                      // size: 15,
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Expanded(
                                                      child: Text(
                                                        "View bids on the Job",
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),
                                                    Text(
                                                        "${widget.jobsData?.numberOfBids ?? ''}")
                                                  ],
                                                )
                                              ],
                                            ),
                                          )
                                        : Container(),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 5.0),
                                      child: Divider(
                                        thickness: .5,
                                        color: Colors.grey.shade800,
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Town",
                                          style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            color: fromHex(gold_dark),
                                          ),
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Text(
                                                "${widget.jobsData?.town?.name ?? ''}",
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 5.0),
                                      child: Divider(
                                        thickness: .5,
                                        color: Colors.grey.shade800,
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Date",
                                          style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            color: fromHex(gold_dark),
                                          ),
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Text(
                                                "${widget.jobsData?.jobDate ?? ''}",
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 5.0),
                                      child: Divider(
                                        thickness: .5,
                                        color: Colors.grey.shade800,
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Time",
                                          style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            color: fromHex(gold_dark),
                                          ),
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Text(
                                                "${widget.jobsData?.jobTime ?? ''}",
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 5.0),
                                      child: Divider(
                                        thickness: .5,
                                        color: Colors.grey.shade800,
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Magistrate",
                                          style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            color: fromHex(gold_dark),
                                          ),
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Text(
                                                "${widget.jobsData?.magistrateOrJudge ?? ''}",
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 5.0),
                                      child: Divider(
                                        thickness: .5,
                                        color: Colors.grey.shade800,
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Brief Type",
                                          style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            color: fromHex(gold_dark),
                                          ),
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Text(
                                                "${widget.jobsData?.briefType?.caseType ?? ''}"
                                                    .capitalize(),
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ),
                                            Icon(
                                              Icons.arrow_drop_down_circle,
                                              color: fromHex(gold_dark),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 5.0),
                                      child: Divider(
                                        thickness: .5,
                                        color: Colors.grey.shade800,
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Court Room",
                                          style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            color: fromHex(gold_dark),
                                          ),
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Text(
                                                "${widget.jobsData?.court?.name ?? ''}",
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ),
                                            Text(
                                              "${widget.jobsData?.courtRoom ?? ''}",
                                              style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 12,
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 5.0),
                                      child: Divider(
                                        thickness: .5,
                                        color: Colors.grey.shade800,
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Amount",
                                          style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            color: fromHex(gold_dark),
                                          ),
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Text(
                                                "${cf.format((widget.jobsData?.additionalFees ?? 0) + (widget.jobsData?.briefType?.amount ?? 0))}",
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 30,
                                    ),
                                  ],
                                ),
                              )
                            : Container(
                                child: Center(
                                  child: Text(
                                    "An Error occurred while loading data",
                                  ),
                                ),
                              ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      widget.jobsData != null
                          ? widget.s == "jobs_feed" &&
                                  getCurrentUser().id !=
                                      "${widget.jobsData.user.id}"
                              ? Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 25,
                                  ),
                                  child: RaisedButton(
                                    elevation: 5.0,
                                    onPressed: () async {
                                      widget.bidOnAJob(
                                          context, widget.jobsData);
                                      Navigator.of(context).pop();
                                    },
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(20),
                                        bottomLeft: Radius.circular(20),
                                        bottomRight: Radius.circular(20),
                                      ),
                                    ),
                                    color: fromHex(blue),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 12.0),
                                      child: Center(
                                        child: Text(
                                          'BID ON THIS JOB',
                                          style: TextStyle(
                                              color: fromHex(gold_dark),
                                              fontSize: 16),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              : Container()
                          : Container(),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Positioned(
          left: 20,
          top: 35,
          child: FloatingActionButton(
            foregroundColor: Colors.grey,
            backgroundColor: fromHex(blue),
            onPressed: () {
              Navigator.of(context).pop();
            },
            mini: true,
            tooltip: "go back",
            child: Icon(
              Icons.keyboard_backspace_sharp,
              color: fromHex(gold_dark),
            ),
          ),
        ),
      ],
    );
  }
}
