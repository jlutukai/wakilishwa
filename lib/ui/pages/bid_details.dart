import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:wakilishwa/core/models/get_my_bids_response.dart';

import '../../utils/constants.dart';
import 'bidder_profile.dart';
import 'chat_page.dart';

class JobBids extends StatefulWidget {
  final BidsData jobBid;
  final String s;
  JobBids(this.jobBid, this.s);

  @override
  _JobBidsState createState() => _JobBidsState();
}

class _JobBidsState extends State<JobBids> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Scaffold(
            backgroundColor: fromHex(blue).withOpacity(0.7),
            body: Column(
              children: [
                Expanded(
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Column(
                          children: [
                            Text(
                              "Adv. ${getCurrentUser()?.name?.first ?? ''} ${getCurrentUser()?.name?.others ?? ''}",
                              style: TextStyle(
                                color: fromHex(gold_dark),
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "${Jiffy("${widget.jobBid?.createdAt ?? ''}", dateFormat).fromNow()}",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w100),
                            ),
                            SizedBox(
                              height: 20,
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding:
                      EdgeInsets.only(top: 45, right: 20, left: 20, bottom: 25),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40)),
                    color: Colors.white,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'My Jobs Bids',
                            style: TextStyle(
                                color: fromHex(blue),
                                fontSize: 20,
                                fontWeight: FontWeight.w900),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        constraints: BoxConstraints(
                            maxHeight:
                                MediaQuery.of(context).size.height * 0.55),
                        child: SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            children: [
                              InkWell(
                                onTap: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          UserProfileDetails(
                                              widget.jobBid.job.user,
                                              "JOB OWNER'S PROFILE"),
                                    ),
                                  );
                                },
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Job Owner's Profile",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        color: fromHex(gold_dark),
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            "Check Out Job Owner's Profile",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 12,
                                            ),
                                          ),
                                        ),
                                        Icon(
                                          Icons.arrow_drop_down_circle,
                                          color: fromHex(gold_dark),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: Divider(
                                  thickness: .5,
                                  color: Colors.grey.shade800,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Job Title",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      color: fromHex(gold_dark),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${widget.jobBid?.job?.title ?? ''}",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: Divider(
                                  thickness: .5,
                                  color: Colors.grey.shade800,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Job Description",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      color: fromHex(gold_dark),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${widget.jobBid?.job?.description ?? ''}",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: Divider(
                                  thickness: .5,
                                  color: Colors.grey.shade800,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Town",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      color: fromHex(gold_dark),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${widget.jobBid?.job?.town?.name ?? ''}",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: Divider(
                                  thickness: .5,
                                  color: Colors.grey.shade800,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Date",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      color: fromHex(gold_dark),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${widget.jobBid?.job?.jobDate ?? ''}",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: Divider(
                                  thickness: .5,
                                  color: Colors.grey.shade800,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Time",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      color: fromHex(gold_dark),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${widget.jobBid?.job?.jobTime ?? ''}",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: Divider(
                                  thickness: .5,
                                  color: Colors.grey.shade800,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Court Room",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      color: fromHex(gold_dark),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${widget.jobBid?.job?.court?.name ?? ''}",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                      Text(
                                        "${widget.jobBid?.job?.courtRoom ?? ''}",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: Divider(
                                  thickness: .5,
                                  color: Colors.grey.shade800,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Magistrate",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      color: fromHex(gold_dark),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${widget.jobBid?.job?.magistrateOrJudge ?? ''}",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: Divider(
                                  thickness: .5,
                                  color: Colors.grey.shade800,
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          ChatPage(
                                        id: "${widget.jobBid.id}",
                                        type: "bids",
                                        senderToken:
                                            "${getCurrentUser().pushToken ?? ''}",
                                        otherToken:
                                            "${widget.jobBid?.job?.user?.pushToken ?? ''}",
                                      ),
                                    ),
                                  );
                                },
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Chat",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        color: fromHex(gold_dark),
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            "Get to communicate with the Job Poster",
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 12,
                                            ),
                                          ),
                                        ),
                                        Icon(
                                          Icons.arrow_drop_down_circle,
                                          color: fromHex(gold_dark),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: Divider(
                                  thickness: .5,
                                  color: Colors.grey.shade800,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Breif Type",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      color: fromHex(gold_dark),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${widget.jobBid?.job?.briefType?.caseType ?? ''}",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: Divider(
                                  thickness: .5,
                                  color: Colors.grey.shade800,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Status",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      color: fromHex(gold_dark),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${widget.jobBid?.status ?? ''}"
                                              .capitalize(),
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: Divider(
                                  thickness: .5,
                                  color: Colors.grey.shade800,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Amount",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      color: fromHex(gold_dark),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "${cf.format((widget.jobBid?.job?.additionalFees ?? 0) + (widget.jobBid?.job?.briefType?.amount ?? 0))}",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 30,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Positioned(
          left: 20,
          top: 35,
          child: FloatingActionButton(
            foregroundColor: Colors.grey,
            backgroundColor: fromHex(blue),
            onPressed: () {
              Navigator.of(context).pop();
            },
            mini: true,
            tooltip: "go back",
            child: Icon(
              Icons.keyboard_backspace_sharp,
              color: fromHex(gold_dark),
            ),
          ),
        ),
      ],
    );
  }
}
