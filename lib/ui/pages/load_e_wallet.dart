import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/viewmodels/deposit_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';

import '../../utils/constants.dart';

class LoadEWallet extends StatefulWidget {
  static const tag = 'load_e_wallet';
  @override
  _LoadEWalletState createState() => _LoadEWalletState();
}

class _LoadEWalletState extends State<LoadEWallet> {
  bool hasConnection;
  TextEditingController _amount = TextEditingController();
  TextEditingController _phoneNumber = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    _phoneNumber.text = getCurrentUser()?.phone ?? "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<DepositModel>(
      onModelReady: (model) async {
        hasConnection = await checkConnection();
        if (!hasConnection) {
          showToast("Check Internet Connection");
          return;
        }
        Map<String, dynamic> data = {
          "transaction_type": "deposit",
          "page": "all"
        };
        await model.getTransactions(data);
      },
      builder: (context, model, child) => Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.white,
            resizeToAvoidBottomInset: true,
            body: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.25,
                      margin: EdgeInsets.only(bottom: 60),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: fromHex(blue),
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(40),
                          bottomRight: Radius.circular(40),
                        ),
                      ),
                    ),
                    Positioned(
                        bottom: 1,
                        right: 0,
                        left: 0,
                        child: CircleAvatar(
                          radius: 60,
                          backgroundColor: Colors.white,
                          child: CircleAvatar(
                            radius: 55,
                            backgroundColor: fromHex(gold_lite),
                            child: Icon(
                              Icons.account_balance_wallet,
                              color: Colors.white,
                              size: 66,
                            ),
                          ),
                        ))
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: model.state == ViewState.Idle
                      ? SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                Text(
                                  'DEPOSIT',
                                  style: TextStyle(
                                      color: fromHex(blue),
                                      fontSize: 20,
                                      fontWeight: FontWeight.w900),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _amount,
                                    keyboardType: TextInputType.number,
                                    style: TextStyle(color: fromHex(blue)),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Please Enter amount";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      isDense: true,
                                      hintText: "500",
                                      labelText: "AMOUNT",
                                      labelStyle:
                                          TextStyle(color: fromHex(blue)),
                                      hintStyle: TextStyle(
                                        color: Colors.blueGrey[400],
                                      ),
                                      border: getBorder(),
                                      focusedBorder: getBorder(),
                                      enabledBorder: getBorder(),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _phoneNumber,
                                    keyboardType: TextInputType.phone,
                                    style: TextStyle(color: fromHex(blue)),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Please Enter phone number";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      isDense: true,
                                      hintText: "+2547 00 000 000",
                                      labelText: "Phone Number",
                                      labelStyle:
                                          TextStyle(color: fromHex(blue)),
                                      hintStyle: TextStyle(
                                        color: Colors.blueGrey[400],
                                      ),
                                      border: getBorder(),
                                      focusedBorder: getBorder(),
                                      enabledBorder: getBorder(),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                InkWell(
                                  onTap: () async {
                                    hasConnection = await checkConnection();
                                    if (!hasConnection) {
                                      showToast("Check Internet Connection");
                                      return;
                                    }
                                    if (_formKey.currentState.validate()) {
                                      Map<String, dynamic> data = {
                                        "phone": "${_phoneNumber.text}",
                                        "amount": "${_amount.text}"
                                      };
                                      Map<String, dynamic> data1 = {
                                        "transaction_type": "deposit",
                                        "page": "all"
                                      };

                                      try {
                                        print(data);
                                        var r = await model.depositCash(data);
                                        await model.getTransactions(data1);
                                        showToast(r.message);
                                      } catch (e) {
                                        Map<String, dynamic> error =
                                            jsonDecode(e.toString());
                                        showToast(
                                            error['errors'][0]['message']);
                                      }
                                    }
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.symmetric(
                                        vertical: 15, horizontal: 34),
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 25),
                                    decoration: BoxDecoration(
                                      color: fromHex(blue),
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(20),
                                        bottomLeft: Radius.circular(20),
                                        bottomRight: Radius.circular(20),
                                      ),
                                    ),
                                    child: Center(
                                      child: Text(
                                        'DEPOSIT',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 15,
                                            fontWeight: FontWeight.w900),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 25),
                                      child: Text(
                                        'DEPOSIT HISTORY',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w900,
                                            color: fromHex(blue),
                                            fontSize: 10),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                _transactions(context, model),
                              ],
                            ),
                          ),
                        )
                      : Center(
                          child: CircularProgressIndicator(
                            backgroundColor: fromHex(gold_dark),
                            valueColor: new AlwaysStoppedAnimation<Color>(
                              fromHex(grey),
                            ),
                          ),
                        ),
                )
              ],
            ),
          ),
          Positioned(
            left: 20,
            top: 35,
            child: FloatingActionButton(
              foregroundColor: Colors.grey,
              backgroundColor: fromHex(blue),
              onPressed: () {
                Navigator.of(context).pop();
              },
              mini: true,
              tooltip: "go back",
              child: Icon(
                Icons.keyboard_backspace_sharp,
                color: fromHex(gold_dark),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _transactions(BuildContext context, DepositModel model) {
    return model.transactions != null
        ? model.transactions.isNotEmpty
            ? ListView.separated(
                itemCount: model.transactions?.length ?? 0,
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                separatorBuilder: (context, index) => Divider(
                  indent: 15,
                  endIndent: 15,
                  color: fromHex(gold_lite),
                ),
                itemBuilder: (context, index) => Container(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.arrow_drop_down_circle_rounded,
                            color: fromHex(gold_lite),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              "${model.transactions[index]?.transactionType ?? ''}"
                                  .capitalize(),
                              style: TextStyle(
                                fontSize: 12,
                                color: fromHex(blue),
                              ),
                              textAlign: TextAlign.start,
                            ),
                          ),
                          Text(
                            "${cf.format(model.transactions[index]?.amount ?? 0)}",
                            style: TextStyle(
                                color: fromHex(blue),
                                fontSize: 15,
                                fontWeight: FontWeight.w900),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            "${model.transactions[index].status}",
                            style: TextStyle(
                                color: fromHex(blue),
                                fontWeight: FontWeight.w100,
                                fontSize: 10,
                                fontStyle: FontStyle.italic),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              )
            : Container(
                height: 150,
                child: Center(
                  child: Text("No Deposits Done Yet"),
                ),
              )
        : Container(
            height: 150,
            child: Center(
              child: Text("An error occurred while loading the data"),
            ),
          );
  }
}
