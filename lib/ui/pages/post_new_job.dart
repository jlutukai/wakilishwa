import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/brief_types_response.dart';
import 'package:wakilishwa/core/models/courts_response.dart';
import 'package:wakilishwa/core/models/get_jobs_response.dart';
import 'package:wakilishwa/core/models/regions_response.dart';
import 'package:wakilishwa/core/models/towns_response.dart';
import 'package:wakilishwa/core/viewmodels/post_new_job_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';

import '../../utils/constants.dart';

class PostNewJobPage extends StatefulWidget {
  final bool isEditing;
  final JobsData myJob;
  final Function() refreshData;
  PostNewJobPage({this.isEditing, this.myJob, this.refreshData});

  // static const tag = 'post_new_job';
  @override
  _PostNewJobPageState createState() => _PostNewJobPageState();
}

class _PostNewJobPageState extends State<PostNewJobPage> {
  TextEditingController _caseTitle = TextEditingController();
  TextEditingController _courtRoom = TextEditingController();
  TextEditingController _date = TextEditingController();
  TextEditingController _time = TextEditingController();
  TextEditingController _description = TextEditingController();
  TextEditingController _name = TextEditingController();
  TextEditingController _additionalCost = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool hasConnection;
  String regionId = "";
  String townId = "";
  String courtId = "";
  String briefTypeId = "";
  ChargesData briefType;

  String regionName = "";
  String townName = "";
  String courtName = "";
  String briefTypeName = "";
  bool _isEditing = false;

  double standardCharges = 0;
  double additionalCost = 0;
  double totalPerformersFee = 0;
  double appCharges = 0;
  double discount = 0;
  double netCharge = 0;
  double netAmount = 0;

  @override
  void initState() {
    _isEditing = widget.isEditing;
    if (_isEditing == true) {
      if (widget.myJob != null) {
        _caseTitle.text = widget.myJob?.title ?? '';
        _courtRoom.text = widget.myJob?.courtRoom ?? '';
        _date.text = widget.myJob?.jobDate ?? '';
        _time.text = widget.myJob?.jobTime ?? '';
        _description.text = widget.myJob?.description ?? '';
        _name.text = widget.myJob?.magistrateOrJudge ?? '';

        if (widget.myJob.town != null) {
          townId = widget.myJob.town.id;
          townName = widget.myJob.town.name;
        }
        if (widget.myJob.court != null) {
          courtId = widget.myJob.court.id;
          courtName = widget.myJob.court.name;
        }

        if (widget.myJob.briefType != null) {
          briefType = widget.myJob.briefType;
          briefTypeId = widget.myJob.briefType.id;
          briefTypeName = widget.myJob.briefType.caseType;
        }
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<PostNewJobModel>(
      onModelReady: (model) async {
        Map<String, dynamic> data = {"page": "all", "is_active": true};
        hasConnection = await checkConnection();
        if (!hasConnection) {
          showToast("Check Internet Connection");
          return;
        }
        await model.getRegions(data);
        await model.getAppCharges();
        await model.getBriefType(data);
      },
      builder: (context, model, child) => Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.white,
            resizeToAvoidBottomInset: true,
            body: Column(
              children: [
                Container(
                  height: 129,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: fromHex(blue),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(40),
                      bottomRight: Radius.circular(40),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        "",
                        style: TextStyle(
                            color: fromHex(gold_dark),
                            fontWeight: FontWeight.w900,
                            fontSize: 25),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  _isEditing ? "Edit My Job" : "Post New Job",
                  style: TextStyle(
                      color: fromHex(blue),
                      fontWeight: FontWeight.w800,
                      fontSize: 25),
                ),
                Expanded(
                  child: model.state == ViewState.Idle
                      ? SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _caseTitle,
                                    keyboardType: TextInputType.text,
                                    textCapitalization:
                                        TextCapitalization.words,
                                    style: TextStyle(color: fromHex(blue)),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Please Enter case title";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      isDense: true,
                                      hintText: "Title",
                                      labelText: "Case Title",
                                      labelStyle:
                                          TextStyle(color: fromHex(blue)),
                                      hintStyle: TextStyle(
                                        color: Colors.blueGrey[400],
                                      ),
                                      border: getBorder(),
                                      focusedBorder: getBorder(),
                                      enabledBorder: getBorder(),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _description,
                                    keyboardType: TextInputType.text,
                                    textCapitalization:
                                        TextCapitalization.sentences,
                                    style: TextStyle(color: fromHex(blue)),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Please Enter case description";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      isDense: true,
                                      hintText: "Enter description here...",
                                      labelText: "Case Description",
                                      labelStyle:
                                          TextStyle(color: fromHex(blue)),
                                      hintStyle: TextStyle(
                                        color: Colors.blueGrey[400],
                                      ),
                                      border: getBorder(),
                                      focusedBorder: getBorder(),
                                      enabledBorder: getBorder(),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _date,
                                    keyboardType: TextInputType.text,
                                    style: TextStyle(color: fromHex(blue)),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Please Enter date";
                                      }
                                      return null;
                                    },
                                    onTap: () async {
                                      DatePicker.showDatePicker(context,
                                          showTitleActions: true,
                                          minTime: DateTime.now(),
                                          maxTime: DateTime.now()
                                              .add(Duration(days: 1200)),
                                          theme: DatePickerTheme(
                                            headerColor: fromHex(blue),
                                            backgroundColor: Colors.white,
                                            itemStyle: TextStyle(
                                              color: fromHex(blue),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18,
                                            ),
                                            cancelStyle: TextStyle(
                                              color: fromHex(gold_lite),
                                              fontSize: 24,
                                            ),
                                            doneStyle: TextStyle(
                                              color: fromHex(gold_lite),
                                              fontSize: 24,
                                            ),
                                          ), onChanged: (date) {
                                        print('change $date in time zone ' +
                                            date.timeZoneOffset.inHours
                                                .toString());
                                      }, onConfirm: (date) {
                                        _date.text = df2.format(date);
                                      },
                                          currentTime: DateTime.now(),
                                          locale: LocaleType.en);
                                    },
                                    enableInteractiveSelection: false,
                                    autofocus: false,
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      isDense: true,
                                      hintText: "01-01-2022",
                                      labelText: "Case Date",
                                      labelStyle:
                                          TextStyle(color: fromHex(blue)),
                                      hintStyle: TextStyle(
                                        color: Colors.blueGrey[400],
                                      ),
                                      border: getBorder(),
                                      focusedBorder: getBorder(),
                                      enabledBorder: getBorder(),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _time,
                                    keyboardType: TextInputType.text,
                                    style: TextStyle(color: fromHex(blue)),
                                    enableInteractiveSelection: false,
                                    autofocus: false,
                                    readOnly: true,
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Please Enter Time";
                                      }
                                      return null;
                                    },
                                    onTap: () async {
                                      DatePicker.showTimePicker(context,
                                          showTitleActions: true,
                                          showSecondsColumn: false,
                                          theme: DatePickerTheme(
                                            headerColor: fromHex(blue),
                                            backgroundColor: Colors.white,
                                            itemStyle: TextStyle(
                                              color: fromHex(blue),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18,
                                            ),
                                            cancelStyle: TextStyle(
                                              color: fromHex(gold_lite),
                                              fontSize: 24,
                                            ),
                                            doneStyle: TextStyle(
                                              color: fromHex(gold_lite),
                                              fontSize: 24,
                                            ),
                                          ), onChanged: (date) {
                                        print('change $date in time zone ' +
                                            date.timeZoneOffset.inHours
                                                .toString());
                                      }, onConfirm: (date) {
                                        _time.text = tf.format(date);
                                      },
                                          currentTime: DateTime.now(),
                                          locale: LocaleType.en);
                                    },
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      isDense: true,
                                      hintText: "09:30",
                                      labelText: "Case Time",
                                      labelStyle:
                                          TextStyle(color: fromHex(blue)),
                                      hintStyle: TextStyle(
                                        color: Colors.blueGrey[400],
                                      ),
                                      border: getBorder(),
                                      focusedBorder: getBorder(),
                                      enabledBorder: getBorder(),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _name,
                                    keyboardType: TextInputType.text,
                                    textCapitalization:
                                        TextCapitalization.words,
                                    style: TextStyle(color: fromHex(blue)),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Please enter magistrate's name";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      isDense: true,
                                      hintText: "Name",
                                      labelText: "Magistrate's Name",
                                      labelStyle:
                                          TextStyle(color: fromHex(blue)),
                                      hintStyle: TextStyle(
                                        color: Colors.blueGrey[400],
                                      ),
                                      border: getBorder(),
                                      focusedBorder: getBorder(),
                                      enabledBorder: getBorder(),
                                    ),
                                  ),
                                ),

                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: DropdownSearch<RegionsData>(
                                          mode: Mode.BOTTOM_SHEET,
                                          showClearButton: false,
                                          showSearchBox: true,
                                          searchBoxDecoration: InputDecoration(
                                              hintText: "Search Regions",
                                              hintStyle: TextStyle(
                                                  color: Colors.grey[100]),
                                              prefixIcon: Icon(Icons.search,
                                                  color: Colors.white),
                                              filled: true,
                                              fillColor: Colors.white10,
                                              border: InputBorder.none,
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none),
                                          dropdownSearchDecoration:
                                              InputDecoration(
                                                  filled: true,
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                          horizontal: 10,
                                                          vertical: 0),
                                                  fillColor: Colors.transparent,
                                                  border: getBorder(),
                                                  enabledBorder: getBorder(),
                                                  focusedBorder: getBorder()),
                                          items: model.regions,
                                          itemAsString: (RegionsData p) =>
                                              "${p.name ?? "Undefined"}",
                                          autoValidateMode: AutovalidateMode
                                              .onUserInteraction,
                                          validator: (RegionsData u) {
                                            if (regionName.isEmpty) {
                                              return "Please choose region";
                                            }
                                            return null;
                                          },
                                          onChanged:
                                              (RegionsData regionData) async {
                                            regionId = regionData.id;
                                            regionName = regionData.name;
                                            Map<String, dynamic> data = {
                                              "page": "all",
                                              "is_active": true,
                                              "region": "${regionData.id}"
                                            };
                                            await model.getTowns(data);
                                            setState(() {
                                              townId = "";
                                              townName = "";
                                              courtName = "";
                                              courtId = "";
                                            });
                                          },
                                          dropdownBuilder: _regionDropDown,
                                          popupBackgroundColor: fromHex(blue),
                                          popupItemBuilder: _regionPopUp,
                                          dropDownButton: Icon(
                                            Icons.arrow_drop_down_circle,
                                            color: fromHex(gold_lite),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: DropdownSearch<TownsData>(
                                          mode: Mode.BOTTOM_SHEET,
                                          showClearButton: false,
                                          showSearchBox: true,
                                          searchBoxDecoration: InputDecoration(
                                              hintText: "Search Towns",
                                              hintStyle: TextStyle(
                                                  color: Colors.grey[100]),
                                              prefixIcon: Icon(Icons.search,
                                                  color: Colors.white),
                                              filled: true,
                                              fillColor: Colors.white10,
                                              border: InputBorder.none,
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none),
                                          dropdownSearchDecoration:
                                              InputDecoration(
                                                  filled: true,
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                          horizontal: 10,
                                                          vertical: 0),
                                                  fillColor: Colors.transparent,
                                                  border: getBorder(),
                                                  enabledBorder: getBorder(),
                                                  focusedBorder: getBorder()),
                                          items: model.towns,
                                          itemAsString: (TownsData p) =>
                                              "${p.name ?? "Undefined"}",
                                          autoValidateMode: AutovalidateMode
                                              .onUserInteraction,
                                          validator: (TownsData u) {
                                            if (townName.isEmpty) {
                                              return "Please choose town";
                                            }
                                            return null;
                                          },
                                          onChanged:
                                              (TownsData townData) async {
                                            townId = townData.id;
                                            townName = townData.name;
                                            Map<String, dynamic> data = {
                                              "page": "all",
                                              "is_active": true,
                                              "town": "${townData.id}"
                                            };
                                            await model.getCourts(data);
                                            setState(() {
                                              courtId = "";
                                              courtName = "";
                                            });
                                          },
                                          dropdownBuilder: _townDropDown,
                                          popupBackgroundColor: fromHex(blue),
                                          popupItemBuilder: _townPopUp,
                                          dropDownButton: Icon(
                                            Icons.arrow_drop_down_circle,
                                            color: fromHex(gold_lite),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: DropdownSearch<CourtsData>(
                                          mode: Mode.BOTTOM_SHEET,
                                          showClearButton: false,
                                          showSearchBox: true,
                                          searchBoxDecoration: InputDecoration(
                                              hintText: "Search Courts",
                                              hintStyle: TextStyle(
                                                  color: Colors.grey[100]),
                                              prefixIcon: Icon(Icons.search,
                                                  color: Colors.white),
                                              filled: true,
                                              fillColor: Colors.white10,
                                              border: InputBorder.none,
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none),
                                          dropdownSearchDecoration:
                                              InputDecoration(
                                                  filled: true,
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                          horizontal: 10,
                                                          vertical: 0),
                                                  fillColor: Colors.transparent,
                                                  border: getBorder(),
                                                  enabledBorder: getBorder(),
                                                  focusedBorder: getBorder()),
                                          items: model.courts,
                                          itemAsString: (CourtsData p) =>
                                              "${p.name ?? "Undefined"}",
                                          autoValidateMode: AutovalidateMode
                                              .onUserInteraction,
                                          validator: (CourtsData u) {
                                            if (courtName.isEmpty) {
                                              return "Please choose court";
                                            }
                                            return null;
                                          },
                                          onChanged: (CourtsData data) {
                                            courtId = data.id;
                                            courtName = data.name;
                                          },
                                          dropdownBuilder: _courtDropDown,
                                          popupBackgroundColor: fromHex(blue),
                                          popupItemBuilder: _courtPopUp,
                                          dropDownButton: Icon(
                                            Icons.arrow_drop_down_circle,
                                            color: fromHex(gold_lite),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _courtRoom,
                                    keyboardType: TextInputType.text,
                                    style: TextStyle(color: fromHex(blue)),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Please Enter Court Room";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      isDense: true,
                                      hintText: "1",
                                      labelText: "Court Room",
                                      labelStyle:
                                          TextStyle(color: fromHex(blue)),
                                      hintStyle: TextStyle(
                                        color: Colors.blueGrey[400],
                                      ),
                                      border: getBorder(),
                                      focusedBorder: getBorder(),
                                      enabledBorder: getBorder(),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: DropdownSearch<ChargesData>(
                                          mode: Mode.BOTTOM_SHEET,
                                          showClearButton: false,
                                          showSearchBox: true,
                                          searchBoxDecoration: InputDecoration(
                                              hintText: "Search Brief Type",
                                              hintStyle: TextStyle(
                                                  color: Colors.grey[100]),
                                              prefixIcon: Icon(Icons.search,
                                                  color: Colors.white),
                                              filled: true,
                                              fillColor: Colors.white10,
                                              border: InputBorder.none,
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none),
                                          dropdownSearchDecoration:
                                              InputDecoration(
                                                  filled: true,
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                          horizontal: 10,
                                                          vertical: 0),
                                                  fillColor: Colors.transparent,
                                                  border: getBorder(),
                                                  enabledBorder: getBorder(),
                                                  focusedBorder: getBorder()),
                                          items: model.briefTypes,
                                          itemAsString: (ChargesData p) =>
                                              "${p.caseType ?? "Undefined"}",
                                          autoValidateMode: AutovalidateMode
                                              .onUserInteraction,
                                          validator: (ChargesData u) {
                                            if (briefTypeName.isEmpty) {
                                              return "Please choose brief type";
                                            }
                                            return null;
                                          },
                                          onChanged: (ChargesData data) {
                                            setState(() {
                                              briefTypeId = data.id;
                                              briefTypeName = data.caseType;
                                              briefType = data;
                                              standardCharges = data.amount;
                                              totalPerformersFee =
                                                  standardCharges +
                                                      additionalCost;
                                              appCharges = model.serviceSetup
                                                          .transactionCost >
                                                      (totalPerformersFee *
                                                          (model.serviceSetup.transactionRate/100))
                                                  ? model.serviceSetup
                                                      .transactionCost
                                                  : (totalPerformersFee *
                                                  (model.serviceSetup.transactionRate/100));
                                              discount = getCurrentUser()
                                                  .freeCredits
                                                  .amountPerJob;
                                              netCharge = appCharges - discount;
                                              netAmount = totalPerformersFee +
                                                  netCharge;
                                            });
                                          },
                                          dropdownBuilder: _briefTypeDropDown,
                                          popupBackgroundColor: fromHex(blue),
                                          popupItemBuilder: _briefTypePopUp,
                                          dropDownButton: Icon(
                                            Icons.arrow_drop_down_circle,
                                            color: fromHex(gold_lite),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),

                                briefType != null
                                    ? Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 25, vertical: 5),
                                        child: TextFormField(
                                          controller: _additionalCost,
                                          keyboardType: TextInputType.number,
                                          style:
                                              TextStyle(color: fromHex(blue)),
                                          onChanged: (value) {
                                            setState(() {
                                              additionalCost =
                                                  double.tryParse(value) ?? 0;
                                              totalPerformersFee =
                                                  standardCharges +
                                                      additionalCost;
                                              appCharges = model.serviceSetup
                                                          .transactionCost >
                                                      (totalPerformersFee *
                                                          (model.serviceSetup.transactionRate/100))
                                                  ? model.serviceSetup
                                                      .transactionCost
                                                  : (totalPerformersFee *
                                                  (model.serviceSetup.transactionRate/100));
                                              discount = getCurrentUser()
                                                  .freeCredits
                                                  .amountPerJob;
                                              netCharge = appCharges - discount;
                                              netAmount = totalPerformersFee +
                                                  netCharge;
                                            });
                                          },
                                          decoration: InputDecoration(
                                            filled: true,
                                            fillColor: Colors.white,
                                            isDense: true,
                                            hintText: "500",
                                            labelText:
                                                "Additional Cost (Fuel, Lunch, ...etc)",
                                            labelStyle:
                                                TextStyle(color: fromHex(blue)),
                                            hintStyle: TextStyle(
                                              color: Colors.blueGrey[400],
                                            ),
                                            border: getBorder(),
                                            focusedBorder: getBorder(),
                                            enabledBorder: getBorder(),
                                          ),
                                        ),
                                      )
                                    : Container(),

                                briefType != null
                                    ? Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 25, vertical: 10),
                                        child: Column(
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                  "${briefType?.caseType ?? ''}",
                                                  style: TextStyle(
                                                      color: fromHex(gold_dark),
                                                      fontWeight:
                                                          FontWeight.w800,
                                                      fontSize: 16),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                    child: Text(
                                                  'Standard Charges',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 14),
                                                )),
                                                Text(
                                                  "${cf.format(standardCharges)}",
                                                  style:
                                                      TextStyle(fontSize: 14),
                                                )
                                              ],
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                    child: Text(
                                                  'Additional Cost',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 14),
                                                )),
                                                Text(
                                                  "${cf.format(additionalCost)}",
                                                  style:
                                                      TextStyle(fontSize: 14),
                                                )
                                              ],
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Text(
                                                    "Total Performer's Fee",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Text(
                                                  "${cf.format(totalPerformersFee)}",
                                                  style:
                                                      TextStyle(fontSize: 14),
                                                )
                                              ],
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Text(
                                                    "App Charges ",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Text(
                                                  "${cf.format(appCharges)}",
                                                  style:
                                                      TextStyle(fontSize: 14),
                                                )
                                              ],
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Text(
                                                    "Discount from free credits",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Text(
                                                  "${cf.format(discount)}",
                                                  style:
                                                      TextStyle(fontSize: 14),
                                                )
                                              ],
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Text(
                                                    "Net App Charge",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Text(
                                                  "${cf.format(netCharge)}",
                                                  style:
                                                      TextStyle(fontSize: 14),
                                                )
                                              ],
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Text(
                                                    "Net Amount Deductible",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 16),
                                                  ),
                                                ),
                                                Text(
                                                  "${cf.format(netAmount)}",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 16),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),

                                ///
                                ///
                                SizedBox(
                                  height: 20,
                                ),
                              ],
                            ),
                          ),
                        )
                      : Center(
                          child: CircularProgressIndicator(
                            backgroundColor: fromHex(gold_dark),
                            valueColor: new AlwaysStoppedAnimation<Color>(
                              fromHex(grey),
                            ),
                          ),
                        ),
                ),
                model.state == ViewState.Idle
                    ? Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 25, vertical: 15),
                        child: RaisedButton(
                          elevation: 5.0,
                          onPressed: () async {
                            hasConnection = await checkConnection();
                            if (!hasConnection) {
                              showToast("Check Internet Connection");
                              return;
                            }
                            if (_isEditing) {
                              if (_formKey.currentState.validate()) {
                                Map<String, dynamic> data = {
                                  "number_of_bids": 0,
                                  "status": "draft",
                                  "title": "${_caseTitle.text}",
                                  "court": "$courtId",
                                  "town": "$townId",
                                  "magistrate_or_judge": "${_name.text}",
                                  "court_room": "${_courtRoom.text}",
                                  "job_date": "${_date.text}",
                                  "additional_fees": additionalCost,
                                  "job_time": "${_time.text}",
                                  "brief_type": "$briefTypeId",
                                  "description": "${_description.text}",
                                  "created_at": "${widget.myJob.createdAt}",
                                  "is_active": widget.myJob.isActive,
                                  "user": "${getCurrentUser().id}"
                                };
                                try {
                                  var r = await model.editJob(
                                      data, widget.myJob.id);
                                  showToast(r.status);
                                  Navigator.of(context).pop();
                                  widget.refreshData();
                                } catch (e) {
                                  showToast(e.toString());
                                }
                              }
                            } else {
                              if (_formKey.currentState.validate()) {
                                Map<String, dynamic> data = {
                                  "title": "${_caseTitle.text}",
                                  "court": "$courtId",
                                  "town": "$townId",
                                  "magistrate_or_judge": "${_name.text}",
                                  "court_room": "${_courtRoom.text}",
                                  "job_date": "${_date.text}",
                                  "additional_fees": additionalCost,
                                  "job_time": "${_time.text}",
                                  "brief_type": "$briefTypeId",
                                  "description": "${_description.text}"
                                };
                                print(data);
                                try {
                                  var r = await model.createNewJob(data);
                                  showToast(r.message);
                                  Navigator.of(context).pop();
                                  widget.refreshData();
                                } catch (e) {
                                  print(e.toString());
                                  showToast(e.toString());
                                }
                              }
                            }
                          },
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20),
                            ),
                          ),
                          color: fromHex(blue),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 15.0),
                            child: Center(
                              child: Text(
                                'Submit',
                                style: TextStyle(
                                    color: fromHex(gold_dark), fontSize: 18),
                              ),
                            ),
                          ),
                        ),
                      )
                    : Container(),
              ],
            ),
          ),
          model.state == ViewState.Idle
              ? Positioned(
                  left: 20,
                  top: 35,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: fromHex(blue),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    mini: true,
                    tooltip: "go back",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(gold_dark),
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  Widget _regionDropDown(
      BuildContext context, RegionsData item, String itemDesignation) {
    return Container(
      child: Text(
        regionName.isNotEmpty
            ? "$regionName"
            : "${item?.name ?? 'Select Region'}",
        style: TextStyle(
          color: fromHex(blue),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _regionPopUp(BuildContext context, RegionsData item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: fromHex(gold_lite)),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item.name ?? 'undefined',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _townDropDown(
      BuildContext context, TownsData item, String itemDesignation) {
    return Container(
      child: Text(
        townName.isNotEmpty ? "$townName" : "Select Town",
        style: TextStyle(
          color: fromHex(blue),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _townPopUp(BuildContext context, TownsData item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item.name ?? 'undefined',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _courtDropDown(
      BuildContext context, CourtsData item, String itemDesignation) {
    return Container(
      child: Text(
        courtName.isNotEmpty ? "$courtName" : "Select Court",
        style: TextStyle(
          color: fromHex(blue),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _courtPopUp(BuildContext context, CourtsData item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item.name ?? 'undefined',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _briefTypeDropDown(
      BuildContext context, ChargesData item, String itemDesignation) {
    return Container(
      child: Text(
        briefTypeName.isNotEmpty ? "$briefTypeName" : "Select Brief Type",
        style: TextStyle(
          color: fromHex(blue),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _briefTypePopUp(
      BuildContext context, ChargesData item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item.caseType ?? 'undefined',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }
}
