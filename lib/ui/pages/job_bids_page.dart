import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_job_bids_response.dart';
import 'package:wakilishwa/core/viewmodels/jobs_bids_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';
import 'package:wakilishwa/utils/constants.dart';

import 'bidder_profile.dart';
import 'chat_page.dart';
import 'home.dart';

class JobBidsPage extends StatefulWidget {
  final String id;
  JobBidsPage(this.id);
  @override
  _JobBidsPageState createState() => _JobBidsPageState();
}

class _JobBidsPageState extends State<JobBidsPage> {
  bool hasConnection;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/bg.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Scaffold(
            key: _scaffoldKey,
            backgroundColor: fromHex(blue).withOpacity(0.7),
            body: Column(
              children: [
                Expanded(
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Column(
                          children: [
                            Text(
                              'BIDS',
                              style: TextStyle(
                                color: fromHex(gold_dark),
                                fontWeight: FontWeight.w800,
                                fontSize: 28,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding:
                      EdgeInsets.only(top: 45, right: 20, left: 20, bottom: 25),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40)),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      // SizedBox(
                      //   height: 15,
                      // ),
                      BaseView<JobsBidModel>(
                        onModelReady: (model) async {
                          hasConnection = await checkConnection();
                          Map<String, dynamic> data = {
                            "page": "all",
                            "is_active": true,
                            "status": "active"
                          };
                          await model.getJobBids(data, widget.id);
                        },
                        builder: (context, model, child) => Container(
                          height: MediaQuery.of(context).size.height * 0.55,
                          child: model.state == ViewState.Idle
                              ? SingleChildScrollView(
                                  physics: BouncingScrollPhysics(),
                                  child: _bidsListView(
                                      context, model.jobBids, model),
                                )
                              : Center(
                                  child: CircularProgressIndicator(
                                    backgroundColor: fromHex(gold_dark),
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                      fromHex(grey),
                                    ),
                                  ),
                                ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Positioned(
          left: 20,
          top: 35,
          child: FloatingActionButton(
            foregroundColor: Colors.grey,
            backgroundColor: fromHex(blue),
            onPressed: () {
              print("called");
              Navigator.of(context).pop();
            },
            mini: true,
            tooltip: "go back",
            child: Icon(
              Icons.keyboard_backspace_sharp,
              color: fromHex(gold_dark),
            ),
          ),
        ),
      ],
    );
  }

  _bidsListView(
      BuildContext context, List<JobBids> jobBids, JobsBidModel model) {
    return jobBids != null
        ? jobBids.isNotEmpty
            ? ListView.separated(
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                itemCount: jobBids.length,
                separatorBuilder: (context, index) => Padding(
                  padding: EdgeInsets.symmetric(vertical: 5),
                  child: Divider(
                    indent: 15,
                    endIndent: 15,
                    color: fromHex(gold_lite),
                  ),
                ),
                itemBuilder: (context, index) => InkWell(
                  onTap: () {
                    _bidsOptions(jobBids[index], context, model);
                  },
                  child: Container(
                    child: Row(
                      children: [
                        CircleAvatar(
                          backgroundColor: fromHex(gold_dark).withOpacity(0.06),
                          child: Text(
                            "${jobBids[index]?.user?.name?.first[0]?.toUpperCase() ?? ''}",
                            style: TextStyle(
                                color: fromHex(blue),
                                fontSize: 18,
                                fontWeight: FontWeight.w800),
                          ),
                        ),
                        Expanded(
                            child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(
                                  "${jobBids[index]?.user?.name?.first ?? ''} ${jobBids[index]?.user?.name?.others ?? ''}",
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                    color: fromHex(gold_dark),
                                  ),
                                ),
                                Text(
                                  "${jobBids[index]?.user?.accountType?.name ?? ''}",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w100,
                                    color: fromHex(blue),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    "Check Out Bidder's Profile",
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontStyle: FontStyle.italic,
                                      color: fromHex(blue),
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Text(
                                  "${Jiffy("${jobBids[index].createdAt}", dateFormat).fromNow()}",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontStyle: FontStyle.italic,
                                      color: fromHex(blue)),
                                )
                              ],
                            )
                          ],
                        ))
                      ],
                    ),
                  ),
                ),
              )
            : Container(
                child: Center(
                  child: Text(
                    "No bids on the job",
                  ),
                ),
              )
        : Container(
            child: Center(
              child: Text(
                "An Error occurred while loading data",
              ),
            ),
          );
  }

  void _bidsOptions(JobBids jobBid, BuildContext context, JobsBidModel model) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        clipBehavior: Clip.antiAlias,
        context: _scaffoldKey.currentContext,
        builder: (context) {
          return SingleChildScrollView(
            child: Container(
              color: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                  color: fromHex(blue),
                  // color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(40),
                      topRight: const Radius.circular(40)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      ListTile(
                        onTap: () async {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  UserProfileDetails(
                                      jobBid.user, "BIDDER'S PROFILE"),
                            ),
                          );
                        },
                        title: Center(
                          child: Text(
                            "View Bidder's Profile ",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      ListTile(
                        onTap: () async {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (BuildContext context) => ChatPage(
                                id: "${jobBid.id}",
                                type: "bids",
                                senderToken:
                                    "${getCurrentUser().pushToken ?? ''}",
                                otherToken: "${jobBid?.user?.pushToken ?? ''}",
                              ),
                            ),
                          );
                        },
                        title: Center(
                          child: Text(
                            "Chat with Bidder",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      ListTile(
                        onTap: () async {
                          hasConnection = await checkConnection();
                          if (!hasConnection) {
                            showToast("Check Internet Connection");
                            return;
                          }
                          // Navigator.pop(context);
                          _awardContacts(context, jobBid, model);
                        },
                        title: Center(
                          child: Text(
                            'Award Contract',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  Future<void> _awardContacts(
      BuildContext context, JobBids jobBid, JobsBidModel model) async {
    try {
      var r = await model.awardContract(jobBid.id);
      showToast(r.message);
      // Navigator.of(context).pop();
      Navigator.of(context).pushNamedAndRemoveUntil(
          HomePage.tag, (Route<dynamic> route) => false);
    } catch (e) {
      showToast(e.toString());
      print(e.toString());
    }
  }
}
