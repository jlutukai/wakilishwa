import 'package:flutter/material.dart';
import 'package:wakilishwa/ui/widgets/account_type.dart';
import 'package:wakilishwa/ui/widgets/auth_info.dart';
import 'package:wakilishwa/ui/widgets/green_clipper.dart';
import 'package:wakilishwa/ui/widgets/id_verification.dart';
import 'package:wakilishwa/ui/widgets/personal_info.dart';

import '../../utils/constants.dart';

class RegistrationPage extends StatefulWidget {
  static const tag = 'registration';
  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  int _currentIndex = 0;
  PageController _pageController;

  @override
  void initState() {
    deleteAddUserData();
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void onDataChange(int index) {
    print("pressed");
    setState(() {
      _pageController.animateToPage(index,
          duration: Duration(milliseconds: 300), curve: Curves.easeInToLinear);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.white,
          resizeToAvoidBottomInset: true,
          body: Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.25,
                margin: EdgeInsets.only(bottom: 60),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  // color: fromHex(blue),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(40),
                    bottomRight: Radius.circular(40),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Expanded(
                child: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _pageController,
                  onPageChanged: (index) {
                    setState(() {
                      _currentIndex = index;
                    });
                  },
                  children: <Widget>[
                    AccountType(onDataChange),
                    IdVerification(onDataChange),
                    PersonalInfo(onDataChange),
                    AuthInfo(onDataChange),
                  ],
                ),
              ),
            ],
          ),
        ),
        ClipPath(
          clipper: GreenClipper(),
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/bg.png"),
                fit: BoxFit.fitWidth,
                colorFilter: new ColorFilter.mode(
                    fromHex(blue).withOpacity(0.9), BlendMode.dstOver),
              ),
            ),
          ),
        ),
        Positioned(
          left: 20,
          top: 35,
          child: Visibility(
            visible: _currentIndex == 0,
            child: FloatingActionButton(
              foregroundColor: Colors.grey,
              backgroundColor: fromHex(blue),
              onPressed: () {
                Navigator.of(context).pop();
              },
              mini: true,
              tooltip: "go back",
              child: Icon(
                Icons.keyboard_backspace_sharp,
                color: fromHex(gold_dark),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
