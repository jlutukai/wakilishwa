import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/services/PushNotificationsService.dart';
import 'package:wakilishwa/core/viewmodels/login_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';
import 'package:wakilishwa/ui/pages/home.dart';
import 'package:wakilishwa/ui/pages/registration_page.dart';
import 'package:wakilishwa/ui/pages/reset_password.dart';
import 'package:wakilishwa/ui/widgets/green_clipper.dart';

import '../../locator.dart';
import '../../utils/constants.dart';

class LoginPage extends StatefulWidget {
  static const tag = 'login';
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();
  PushNotificationService _pushNotification =
      locator<PushNotificationService>();
  final _formKey = GlobalKey<FormState>();
  bool _isHidden = true;
  String pass = "";

  var _outLineBorder = OutlineInputBorder(
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(20.0),
      bottomRight: Radius.circular(20.0),
      bottomLeft: Radius.circular(20.0),
    ),
    borderSide: BorderSide(color: fromHex(gold_dark), width: 1.0),
  );

  @override
  Widget build(BuildContext context) {
    return BaseView<LoginModel>(
      builder: (context, model, child) => Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            _loginDetails(context, model),
            ClipPath(
              clipper: GreenClipper(),
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/bg.png"),
                    fit: BoxFit.fitWidth,
                    colorFilter: new ColorFilter.mode(
                        fromHex(blue).withOpacity(0.9), BlendMode.dstOver),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  _loginDetails(BuildContext context, LoginModel model) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.25,
            ),
            Stack(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(bottom: 40, left: 50, right: 50),
                    child: SvgPicture.asset(
                      "assets/logo_light.svg",
                      height: 100,
                      width: 600,
                    ),
                  ),
                ),
                Positioned(
                  bottom: 1,
                  left: 50,
                  right: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          SizedBox(
                            height: 5,
                          ),
                          Text("WAKILISHWA",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: fromHex(blue),
                                  fontWeight: FontWeight.w800,
                                  fontSize: 16)),
                          Text("...find a lawyer",
                              textAlign: TextAlign.center,
                              style: GoogleFonts.pacifico(
                                  textStyle:
                                      TextStyle(color: fromHex(gold_dark)))),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Form(
              key: _formKey,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 25),
                    child: TextFormField(
                      controller: _email,
                      keyboardType: TextInputType.emailAddress,
                      style: TextStyle(color: fromHex(blue)),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please Enter your email address/ Phone Number";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        isDense: true,
                        hintText: "johndoe@email.com",
                        labelText: "Email / Phone Number",
                        labelStyle: TextStyle(color: fromHex(blue)),
                        hintStyle: TextStyle(
                          color: Colors.blueGrey[400],
                        ),
                        border: _outLineBorder,
                        focusedBorder: _outLineBorder,
                        enabledBorder: _outLineBorder,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 25),
                    padding: EdgeInsets.only(top: 15),
                    child: TextFormField(
                      controller: _password,
                      keyboardType: TextInputType.visiblePassword,
                      obscureText: _isHidden,
                      style: TextStyle(color: fromHex(blue)),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please your password";
                        }
                        return null;
                      },
                      onChanged: (v) {
                        setState(() {
                          pass = v;
                        });
                      },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white10,
                        isDense: true,
                        labelText: "Password",
                        labelStyle: TextStyle(color: fromHex(blue)),
                        hintText: "Your password",
                        hintStyle: TextStyle(
                          color: Colors.blueGrey[400],
                        ),
                        border: _outLineBorder,
                        focusedBorder: _outLineBorder,
                        enabledBorder: _outLineBorder,
                        suffixIcon: pass.isNotEmpty
                            ? IconButton(
                                onPressed: _toggleVisibility,
                                icon: _isHidden
                                    ? Icon(
                                        Icons.visibility_off,
                                        color: fromHex(grey),
                                      )
                                    : Icon(
                                        Icons.visibility,
                                        color: fromHex(gold_dark),
                                      ),
                              )
                            : null,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 30,
                      ),
                      Text(
                        'Forgot Password?',
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pushNamed(ResetPassword.tag);
                        },
                        child: Text(
                          ' RESET PASSWORD',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: fromHex(blue),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  model.state == ViewState.Idle
                      ? InkWell(
                          onTap: () async {
                            bool hasConnection = await checkConnection();
                            if (!hasConnection) {
                              showToast("Check Internet Connection");
                              return;
                            }
                            if (_formKey.currentState.validate()) {
                              Map<String, String> data = {
                                "email": "${_email.text}",
                                "password": "${_password.text}"
                              };
                              // Map<String, String> data = {
                              //   "email": "karurijoel@gmail.com",
                              //   "password": "2020"
                              // };

                              // Map<String, String> data = {
                              //   "email": "otienoalvin44@gmil.com",
                              //   "password": "1234"
                              // };
                              String fcmToken =
                                  await _pushNotification.getFCMToken();
                              try {
                                var r = await model.login(data);
                                showToast(r.status);
                                Map<String, String> data1 = {
                                  "push_token": "$fcmToken"
                                };
                                try {
                                  print(getToken());
                                  var d = await model.updateToken(data1);
                                  Navigator.pushNamedAndRemoveUntil(
                                      context,
                                      HomePage.tag,
                                      (Route<dynamic> route) => false);
                                } catch (err) {
                                  try {
                                    Map<String, dynamic> error =
                                        jsonDecode(err.toString());
                                    showToast(error['errors'][0]['message']);
                                  } catch (a) {}
                                }
                              } catch (e) {
                                try {
                                  Map<String, dynamic> error =
                                      jsonDecode(e.toString());
                                  showToast(error['errors'][0]['message']);
                                } catch (a) {}
                              }
                            }
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 15, horizontal: 97),
                            decoration: BoxDecoration(
                              color: fromHex(blue),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                bottomLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                              ),
                            ),
                            child: Text(
                              'SIGN IN',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          ),
                        )
                      : Center(
                          child: CircularProgressIndicator(
                            backgroundColor: fromHex(gold_dark),
                            valueColor: new AlwaysStoppedAnimation<Color>(
                              fromHex(grey),
                            ),
                          ),
                        ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Not Registered?',
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pushNamed(RegistrationPage.tag);
                        },
                        child: Text(
                          ' REGISTER HERE',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: fromHex(blue),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
