import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/viewmodels/request_password_reset_model.dart';
import 'package:wakilishwa/ui/widgets/green_clipper.dart';
import 'package:wakilishwa/utils/constants.dart';

import 'base_view.dart';
import 'login.dart';

class ChangePasswordPage extends StatefulWidget {
  final String code;
  ChangePasswordPage(this.code);
  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  TextEditingController _pass = TextEditingController();
  TextEditingController _confirmPass = TextEditingController();
  String pass = "";
  String confirmPass = "";

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return BaseView<RequestPasswordResetModel>(
      builder: (context, model, child) => Stack(
        children: [
          Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.white,
            body: Stack(
              children: [
                _loginDetails(context, model),
                ClipPath(
                  clipper: GreenClipper(),
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/bg.png"),
                        fit: BoxFit.fitWidth,
                        colorFilter: new ColorFilter.mode(
                            fromHex(blue).withOpacity(0.9), BlendMode.dstOver),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          model.state == ViewState.Idle
              ? Positioned(
                  left: 20,
                  top: 35,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: fromHex(blue),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    mini: true,
                    tooltip: "go back",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(gold_dark),
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  _loginDetails(BuildContext context, RequestPasswordResetModel model) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Container(
          height: MediaQuery.of(context).size.height,
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 200, bottom: 40),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            bottom: 40, left: 50, right: 50),
                        child: SvgPicture.asset(
                          "assets/logo_light.svg",
                          height: 100,
                          width: 600,
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 1,
                      left: 50,
                      right: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(
                            children: [
                              SizedBox(
                                height: 5,
                              ),
                              Text("WAKILISHWA",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: fromHex(blue),
                                      fontWeight: FontWeight.w800,
                                      fontSize: 16)),
                              Text("...find a lawyer",
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.pacifico(
                                      textStyle: TextStyle(
                                          color: fromHex(gold_dark)))),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 25, vertical: 5),
                    child: TextFormField(
                      controller: _pass,
                      keyboardType: TextInputType.visiblePassword,
                      obscureText: true,
                      style: TextStyle(color: fromHex(blue)),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please Enter your pass word";
                        }
                        if (confirmPass != value) {
                          return "Passwords do not match";
                        }
                        return null;
                      },
                      onChanged: (value) {
                        setState(() {
                          pass = value;
                        });
                      },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        isDense: true,
                        hintText: "************",
                        labelText: "Password",
                        labelStyle: TextStyle(color: fromHex(blue)),
                        hintStyle: TextStyle(
                          color: Colors.blueGrey[400],
                        ),
                        border: getBorder(),
                        focusedBorder: getBorder(),
                        enabledBorder: getBorder(),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 25, vertical: 5),
                    child: TextFormField(
                      controller: _confirmPass,
                      keyboardType: TextInputType.visiblePassword,
                      obscureText: true,
                      style: TextStyle(color: fromHex(blue)),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please Re-Enter your password";
                        }
                        if (pass != value) {
                          return "Passwords do not match";
                        }
                        return null;
                      },
                      onChanged: (value) {
                        setState(() {
                          confirmPass = value;
                        });
                      },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        isDense: true,
                        hintText: "*********",
                        labelText: "Confirm Password",
                        labelStyle: TextStyle(color: fromHex(blue)),
                        hintStyle: TextStyle(
                          color: Colors.blueGrey[400],
                        ),
                        border: getBorder(),
                        focusedBorder: getBorder(),
                        enabledBorder: getBorder(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  model.state == ViewState.Idle
                      ? InkWell(
                          onTap: () async {
                            bool hasConnection = await checkConnection();
                            if (!hasConnection) {
                              showToast("Check Internet Connection");
                              return;
                            }
                            if (_formKey.currentState.validate()) {
                              Map<String, String> data = {
                                "code": "${widget.code}",
                                "password": "${_pass.text}",
                                "password_confirm": "${_confirmPass.text}"
                              };
                              try {
                                var r = await model.resetPassword(data);
                                showToast(r?.message ?? '');
                                Navigator.pushNamedAndRemoveUntil(
                                    context,
                                    LoginPage.tag,
                                    (Route<dynamic> route) => false);
                              } catch (e) {
                                try {
                                  Map<String, dynamic> error =
                                      jsonDecode(e.toString());
                                  showToast(error['errors'][0]['message']);
                                } catch (a) {}
                              }
                            }
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 15, horizontal: 97),
                            decoration: BoxDecoration(
                              color: fromHex(blue),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                bottomLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                              ),
                            ),
                            child: Text(
                              'REQUEST',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          ),
                        )
                      : Center(
                          child: CircularProgressIndicator(
                            backgroundColor: fromHex(gold_dark),
                            valueColor: new AlwaysStoppedAnimation<Color>(
                              fromHex(grey),
                            ),
                          ),
                        ),
                  SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
