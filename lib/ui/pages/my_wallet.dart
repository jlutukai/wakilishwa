import 'package:flutter/material.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/viewmodels/wallet_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';

import '../../utils/constants.dart';

class MyWallet extends StatefulWidget {
  static const tag = 'my_wallet';
  @override
  _MyWalletState createState() => _MyWalletState();
}

class _MyWalletState extends State<MyWallet> {
  bool hasConnection;

  @override
  Widget build(BuildContext context) {
    return BaseView<WalletModel>(
      onModelReady: (model) async {
        hasConnection = await checkConnection();
        if (!hasConnection) {
          showToast("Check Internet Connection");
          return;
        }
        Map<String, dynamic> data = {"page": "all"};
        await model.getWalletDetails();
        await model.getTransactions(data);
      },
      builder: (context, model, child) => Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.white,
            resizeToAvoidBottomInset: true,
            body: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.25,
                      margin: EdgeInsets.only(bottom: 60),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: fromHex(blue),
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(40),
                          bottomRight: Radius.circular(40),
                        ),
                      ),
                    ),
                    Positioned(
                        bottom: 1,
                        right: 0,
                        left: 0,
                        child: CircleAvatar(
                          radius: 60,
                          backgroundColor: Colors.white,
                          child: CircleAvatar(
                            radius: 55,
                            backgroundColor: fromHex(gold_lite),
                            child: Icon(
                              Icons.account_balance_wallet,
                              color: Colors.white,
                              size: 66,
                            ),
                          ),
                        ))
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'WALLET',
                  style: TextStyle(
                      color: fromHex(blue),
                      fontSize: 20,
                      fontWeight: FontWeight.w800),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width - 100,
                      height: 105,
                      margin: EdgeInsets.only(
                          top: 30, bottom: 30, right: 20, left: 20),
                      padding:
                          EdgeInsets.symmetric(horizontal: 38, vertical: 20),
                      decoration: BoxDecoration(
                        color: fromHex(gold_dark),
                        borderRadius: BorderRadius.all(
                          Radius.circular(30),
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Current Balance',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w800,
                                        fontSize: 18),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "${cf.format(model.wallet?.amountBalance ?? 0)}",
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w800,
                                  color: fromHex(blue),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
                Expanded(
                  child: model.state == ViewState.Idle
                      ? _transactions(context, model)
                      : Center(
                          child: CircularProgressIndicator(
                            backgroundColor: fromHex(gold_dark),
                            valueColor: new AlwaysStoppedAnimation<Color>(
                              fromHex(grey),
                            ),
                          ),
                        ),
                ),
              ],
            ),
          ),
          Positioned(
            left: 20,
            top: 35,
            child: FloatingActionButton(
              foregroundColor: Colors.grey,
              backgroundColor: fromHex(blue),
              onPressed: () {
                Navigator.of(context).pop();
              },
              mini: true,
              tooltip: "go back",
              child: Icon(
                Icons.keyboard_backspace_sharp,
                color: fromHex(gold_dark),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _transactions(BuildContext context, WalletModel model) {
    return model.transactions != null
        ? model.transactions.isNotEmpty
            ? ListView.separated(
                itemCount: model.transactions?.length ?? 0,
                physics: BouncingScrollPhysics(),
                separatorBuilder: (context, index) => Divider(
                  indent: 15,
                  endIndent: 15,
                  color: fromHex(gold_lite),
                ),
                itemBuilder: (context, index) => Container(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.arrow_drop_down_circle_rounded,
                            color: fromHex(gold_lite),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              "${model.transactions[index]?.transactionType ?? ''}"
                                  .capitalize(),
                              style: TextStyle(
                                fontSize: 12,
                                color: fromHex(blue),
                              ),
                              textAlign: TextAlign.start,
                            ),
                          ),
                          Text(
                            "${cf.format(model.transactions[index]?.amount ?? 0)}",
                            style: TextStyle(
                                color: fromHex(blue),
                                fontSize: 15,
                                fontWeight: FontWeight.w800),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            "${model.transactions[index].status}",
                            style: TextStyle(
                                color: fromHex(blue),
                                fontWeight: FontWeight.w100,
                                fontSize: 10,
                                fontStyle: FontStyle.italic),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              )
            : Center(
                child: Text("No Transactions Done Yet"),
              )
        : Center(
            child: Container(
              child: Text("An error occurred while loading the data"),
            ),
          );
  }
}
