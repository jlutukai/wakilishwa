import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:wakilishwa/core/services/PushNotificationsService.dart';
import 'package:wakilishwa/utils/constants.dart';

import '../../locator.dart';
import 'home.dart';
import 'intro_page.dart';
import 'otp_page.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  PushNotificationService _pushNotification =
      locator<PushNotificationService>();
  @override
  void initState() {
    _pushNotification.initialise();
    Future.delayed(Duration(seconds: 5), () {
      if (getAddUserData() == null) {
        if (getToken() != null && getToken().isNotEmpty) {
          Navigator.pushNamedAndRemoveUntil(
              context, HomePage.tag, (Route<dynamic> route) => false);
        } else {
          Navigator.pushNamedAndRemoveUntil(
              context, IntoPage.tag, (Route<dynamic> route) => false);
        }
      } else {
        if (getAddUserData().hasFinishedRegistration == null) {
          if (getToken() != null && getToken().isNotEmpty) {
            Navigator.pushNamedAndRemoveUntil(
                context, HomePage.tag, (Route<dynamic> route) => false);
          } else {
            Navigator.pushNamedAndRemoveUntil(
                context, IntoPage.tag, (Route<dynamic> route) => false);
          }
        } else {
          if (getAddUserData().hasActivatedAccount == null) {
            if (getAddUserData().hasFinishedRegistration) {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                    builder: (BuildContext context) =>
                        OTPPage("login", getAddUserData().email),
                  ),
                  (Route<dynamic> route) => false);
            }
          } else {
            if (getAddUserData().hasFinishedRegistration &&
                getAddUserData().hasActivatedAccount) {
              if (getToken() != null && getToken().isNotEmpty) {
                Navigator.pushNamedAndRemoveUntil(
                    context, HomePage.tag, (Route<dynamic> route) => false);
              } else {
                Navigator.pushNamedAndRemoveUntil(
                    context, IntoPage.tag, (Route<dynamic> route) => false);
              }
            }
          }
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: fromHex(blue),
      body: Column(
        children: [
          Expanded(
            child: Center(
              child: Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 40),
                    child: SvgPicture.asset(
                      "assets/logo_dark.svg",
                      height: 200,
                      width: 200,
                    ),
                  ),
                  Positioned(
                    bottom: 5,
                    left: 35,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 5,
                        ),
                        Text("WAKILISHWA",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w800,
                                fontSize: 24)),
                        Text("...find a lawyer",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.pacifico(
                                textStyle:
                                    TextStyle(color: fromHex(gold_dark)))),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
