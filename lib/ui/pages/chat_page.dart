import 'package:bubble/bubble.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/viewmodels/chat_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';
import 'package:wakilishwa/utils/constants.dart';

class ChatPage extends StatefulWidget {
  static const tag = 'chat';
  final String id;
  final String type;
  final String senderToken;
  final String otherToken;
  ChatPage({this.id, this.type, this.senderToken, this.otherToken});
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  double height, width;
  TextEditingController messageController = TextEditingController();
  ScrollController scrollController = ScrollController();

  Future<void> callback() async {
    if (messageController.text.length > 0) {
      await _firestore
          .collection("messages")
          .doc(widget.id)
          .collection(widget.type)
          .add({
        'message': messageController.text,
        'from_id': getCurrentUser().id,
        'name': getCurrentUser().name.first,
        'date': DateTime.now().toIso8601String().toString(),
      });
      messageController.clear();
      scrollController.animateTo(
        scrollController.position.minScrollExtent,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return BaseView<ChatModel>(
      builder: (context, model, child) => Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Stack(
                children: [
                  Container(
                    height: 129,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: fromHex(blue),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(40),
                        bottomRight: Radius.circular(40),
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          "Chats",
                          style: TextStyle(
                              color: fromHex(gold_dark),
                              fontWeight: FontWeight.w900,
                              fontSize: 25),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    left: 20,
                    top: 35,
                    child: FloatingActionButton(
                      foregroundColor: Colors.grey,
                      backgroundColor: fromHex(blue),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      mini: true,
                      tooltip: "go back",
                      child: Icon(
                        Icons.keyboard_backspace_sharp,
                        color: fromHex(gold_dark),
                      ),
                    ),
                  )
                ],
              ),
              Expanded(
                child: StreamBuilder<QuerySnapshot>(
                  stream: _firestore
                      .collection("messages")
                      .doc(widget.id)
                      .collection(widget.type)
                      .orderBy('date', descending: true)
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData)
                      return Center(
                        child: CircularProgressIndicator(
                          backgroundColor: fromHex(gold_dark),
                          valueColor: new AlwaysStoppedAnimation<Color>(
                            fromHex(grey),
                          ),
                        ),
                      );

                    List<QueryDocumentSnapshot> docs = snapshot.data.docs;

                    List<Widget> messages = docs
                        .map((doc) => doc['from_id'] == getCurrentUser().id
                            ? MyMessageWidget(
                                content: doc['message'],
                                time: doc['date'],
                                name: doc['name'],
                                isSame: false,
                                hasHeading: true,
                              )
                            : RecievedMessageWidget(
                                content: doc['message'],
                                time: doc['date'],
                                name: doc['name'],
                                isSame: false,
                                hasHeading: true,
                              ))
                        .toList();

                    return ListView(
                      reverse: true,
                      controller: scrollController,
                      children: <Widget>[
                        ...messages,
                      ],
                    );
                  },
                ),
              ),
              Container(
                child: buildInputArea(model),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildChatInput() {
    return Container(
      width: width * 0.7,
      padding: const EdgeInsets.all(2.0),
      margin: const EdgeInsets.only(left: 40.0),
      child: TextField(
        decoration: InputDecoration.collapsed(
            hintText: 'Send a message...',
            hintStyle: TextStyle(color: fromHex(blue))),
        controller: messageController,
      ),
    );
  }

  Widget buildSendButton(ChatModel model) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        FloatingActionButton(
          backgroundColor: fromHex(blue),
          // mini: true,
          heroTag: "send_message",
          onPressed: () async {
            bool hasConnection = await checkConnection();
            if (!hasConnection) {
              showToast("Check Internet Connection");
              return;
            }
            callback();
            Map<String, dynamic> data = {
              "to": "${widget.otherToken}",
              "collapse_key": "type_a",
              "notification": {
                "body": "${messageController.text}",
                "title": "New Message from ${getCurrentUser().name.first}"
              },
              "data": {
                "click_action": "FLUTTER_NOTIFICATION_CLICK",
                "screen": "test",
                "chatId": "${widget.id}",
                "type": "${widget.type}",
                "senderToken": "${widget.senderToken}",
                "token": "${widget.otherToken}",
                "senderId": "${getCurrentUser().id}"
              }
            };
            try {
              print(data);
              var r = await model.sendFCMMessage(data);
              scrollController.animateTo(
                scrollController.position.minScrollExtent,
                curve: Curves.easeOut,
                duration: const Duration(milliseconds: 300),
              );
            } catch (e) {
              print(e);
            }
          },
          child: Icon(
            Icons.send,
            color: fromHex(gold_dark),
          ),
        ),
      ],
    );
  }

  Widget buildInputArea(ChatModel model) {
    return Container(
      height: height * 0.1,
      width: width,
      child: Row(
        children: <Widget>[
          buildChatInput(),
          Expanded(
            child: model.state == ViewState.Idle
                ? buildSendButton(model)
                : Center(
                    child: CircularProgressIndicator(
                      backgroundColor: fromHex(gold_dark),
                      valueColor: new AlwaysStoppedAnimation<Color>(
                        fromHex(grey),
                      ),
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}

class ChatPageArguments {
  String id;
  String type;

  ChatPageArguments(this.id, this.type);
}

class MyMessageWidget extends StatelessWidget {
  final String content;
  final String time;
  final String name;
  final bool isSame;
  final bool hasHeading;

  const MyMessageWidget(
      {Key key,
      this.content,
      this.time,
      this.name,
      this.isSame,
      this.hasHeading})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // print()
    return Bubble(
        margin: BubbleEdges.zero,
        alignment: Alignment.topRight,
        nip: BubbleNip.no,
        color: Colors.transparent,
        padding: BubbleEdges.zero,
        elevation: 0,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Bubble(
              margin: BubbleEdges.only(top: 10),
              alignment: Alignment.topRight,
              nip: !isSame ? BubbleNip.rightBottom : BubbleNip.no,
              color: Colors.blue.shade50,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    Jiffy(DateTime.tryParse(time)).format("HH:mm"),
                    style: TextStyle(color: Colors.grey, fontSize: 11),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        "you",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic,
                            color: fromHex(blue),
                            fontSize: 12),
                      ),
                      Container(
                        constraints: BoxConstraints(
                            minWidth: 80,
                            maxWidth: MediaQuery.of(context).size.width - 200),
                        child: Bubble(
                          color: Colors.transparent,
                          elevation: 0,
                          padding: BubbleEdges.all(0),
                          child: RichText(
                            text: TextSpan(
                              style: TextStyle(
                                  fontSize: 14.0, color: Colors.black),
                              children: <TextSpan>[
                                TextSpan(text: "${content}"),
//                                          TextSpan(
//                                              text: '@marylyn',style: TextStyle(color: Colors.blue,)
//                                          ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 10,
            ),
            !isSame
                ? CircleAvatar(
                    radius: 15.0,
                    backgroundColor: fromHex(blue),
                    child: Center(
                      child: Text(
                        name.substring(0, 1).capitalize(),
                        style: TextStyle(
                          color: fromHex(gold_dark),
                        ),
                      ),
                    ),
                  )
                : Container(
                    width: 24,
                  ),
            SizedBox(
              width: 5,
            ),
          ],
        ));
  }
}

class RecievedMessageWidget extends StatelessWidget {
  final String content;
  final String time;
  final String name;
  final bool isSame;
  final bool hasHeading;

  const RecievedMessageWidget(
      {Key key,
      this.content,
      this.time,
      this.name,
      this.isSame,
      this.hasHeading})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Bubble(
      margin: BubbleEdges.zero,
      alignment: Alignment.topLeft,
      nip: BubbleNip.no,
      color: Colors.transparent,
      padding: BubbleEdges.zero,
      elevation: 0,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          SizedBox(
            width: 5,
          ),
          !isSame
              ? CircleAvatar(
                  radius: 15.0,
                  backgroundColor: fromHex(blue),
                  child: Center(
                    child: Text(
                      name.substring(0, 1).capitalize(),
                      style: TextStyle(color: fromHex(gold_dark)),
                    ),
                  ),
                )
              : Container(
                  width: 24,
                ),
          SizedBox(
            width: 10,
          ),
          Bubble(
            margin: BubbleEdges.only(top: 10),
            alignment: Alignment.topLeft,
            nip: !isSame ? BubbleNip.leftBottom : BubbleNip.no,
            color: Colors.teal.shade50,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "$name",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic,
                          color: fromHex(blue),
                          fontSize: 12),
                    ),
                    Container(
                        constraints: BoxConstraints(
                            minWidth: 80,
                            maxWidth: MediaQuery.of(context).size.width - 200),
                        child: Bubble(
                          color: Colors.transparent,
                          elevation: 0,
                          padding: BubbleEdges.all(0),
                          child: RichText(
                            text: TextSpan(
                              style: TextStyle(
                                  fontSize: 14.0, color: Colors.black),
                              children: <TextSpan>[
                                TextSpan(text: "$content"),
//                                          TextSpan(
//                                              text: '@marylyn',style: TextStyle(color: Colors.blue,)
//                                          ),
                              ],
                            ),
                          ),
                        )),
                  ],
                ),
                Text(
                  Jiffy(DateTime.tryParse(time)).format("HH:mm"),
                  style: TextStyle(color: Colors.grey, fontSize: 11),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
