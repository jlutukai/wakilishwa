import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/local/local_models.dart';
import 'package:wakilishwa/core/models/regions_response.dart';
import 'package:wakilishwa/core/models/towns_response.dart';
import 'package:wakilishwa/core/viewmodels/edit_profile_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';
import 'package:wakilishwa/utils/constants.dart';

import 'home.dart';

class Profile extends StatefulWidget {
  static const tag = 'profile';
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  bool hasConnection;
  TextEditingController _fname = TextEditingController();
  TextEditingController _lname = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _phone = TextEditingController();
  TextEditingController _idNumber = TextEditingController();
  TextEditingController _passport = TextEditingController();
  TextEditingController _description = TextEditingController();

  String accountTypeId = "";
  String countryId = "";
  String regionId = "";
  String townId = "";

  String accountTypeName = "";
  String countryName = "";
  String countryCode = "";
  String regionName = "";
  String townName = "";

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    var u = getCurrentUser();
    _fname.text = u?.name?.first ?? '';
    _lname.text = u?.name?.others ?? '';
    _email.text = u?.email ?? '';
    _phone.text = u?.phone ?? '';
    _idNumber.text = u?.idNumber ?? '';
    _passport.text = u?.passportNumber ?? '';
    accountTypeId = u?.accountType?.id ?? '';
    accountTypeName = u?.accountType?.name ?? '';
    countryId = u?.country?.id ?? '';
    countryName = u?.country?.name ?? '';
    countryCode = u?.country?.code ?? '';
    regionId = u?.region?.id ?? '';
    regionName = u?.region?.name ?? '';
    townId = u?.town?.id ?? '';
    townName = u?.town?.name ?? '';
    _description.text = u?.description ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<EditProfileModel>(
      onModelReady: (model) async {
        Map<String, dynamic> data = {"page": "all", "is_active": true};
        hasConnection = await checkConnection();
        if (!hasConnection) {
          showToast("Check Internet Connection");
          return;
        }

        await model.getRegions(data);
        await model.getAccountTypes(data);
        // await model.getTowns(data);
        await model.getCountries(data);
      },
      builder: (context, model, child) => Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.white,
            resizeToAvoidBottomInset: true,
            body: Column(
              children: [
                Container(
                  height: 129,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: fromHex(blue),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(40),
                      bottomRight: Radius.circular(40),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        "PROFILE",
                        style: TextStyle(
                            color: fromHex(gold_dark),
                            fontWeight: FontWeight.w900,
                            fontSize: 25),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: model.state == ViewState.Idle
                      ? SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _fname,
                                    keyboardType: TextInputType.text,
                                    textCapitalization:
                                        TextCapitalization.words,
                                    style: TextStyle(color: fromHex(blue)),
                                    enableInteractiveSelection: false,
                                    autofocus: false,
                                    readOnly: true,
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Please Enter your first name";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
                                        isDense: true,
                                        hintText: "John",
                                        labelText: "First Name",
                                        labelStyle:
                                            TextStyle(color: fromHex(blue)),
                                        hintStyle: TextStyle(
                                          color: Colors.blueGrey[400],
                                        ),
                                        border: getBorder(),
                                        focusedBorder: getBorder(),
                                        enabledBorder: getBorder()),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _lname,
                                    keyboardType: TextInputType.text,
                                    textCapitalization:
                                        TextCapitalization.words,
                                    style: TextStyle(color: fromHex(blue)),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Please Enter your other names";
                                      }
                                      return null;
                                    },
                                    enableInteractiveSelection: false,
                                    autofocus: false,
                                    readOnly: true,
                                    decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
                                        isDense: true,
                                        hintText: "Doe",
                                        labelText: "Other Names",
                                        labelStyle:
                                            TextStyle(color: fromHex(blue)),
                                        hintStyle: TextStyle(
                                          color: Colors.blueGrey[400],
                                        ),
                                        border: getBorder(),
                                        focusedBorder: getBorder(),
                                        enabledBorder: getBorder()),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _email,
                                    keyboardType: TextInputType.emailAddress,
                                    style: TextStyle(color: fromHex(blue)),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Please Enter your email address";
                                      }
                                      return null;
                                    },
                                    enableInteractiveSelection: false,
                                    autofocus: false,
                                    readOnly: true,
                                    decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
                                        isDense: true,
                                        hintText: "johndoe@email.com",
                                        labelText: "Email",
                                        labelStyle:
                                            TextStyle(color: fromHex(blue)),
                                        hintStyle: TextStyle(
                                          color: Colors.blueGrey[400],
                                        ),
                                        border: getBorder(),
                                        focusedBorder: getBorder(),
                                        enabledBorder: getBorder()),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _phone,
                                    keyboardType: TextInputType.phone,
                                    style: TextStyle(color: fromHex(blue)),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Please Enter your phone number";
                                      }
                                      return null;
                                    },
                                    enableInteractiveSelection: false,
                                    autofocus: false,
                                    readOnly: true,
                                    decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
                                        isDense: true,
                                        hintText: "0701000000",
                                        labelText: "Phone",
                                        labelStyle:
                                            TextStyle(color: fromHex(blue)),
                                        hintStyle: TextStyle(
                                          color: Colors.blueGrey[400],
                                        ),
                                        border: getBorder(),
                                        focusedBorder: getBorder(),
                                        enabledBorder: getBorder()),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _idNumber,
                                    keyboardType: TextInputType.number,
                                    style: TextStyle(color: fromHex(blue)),
                                    validator: (value) {
                                      if (value.isEmpty &&
                                          _passport.text.isEmpty) {
                                        return "Please Enter your ID Number";
                                      }
                                      return null;
                                    },
                                    enableInteractiveSelection: false,
                                    autofocus: false,
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      isDense: true,
                                      hintText: "00000001",
                                      labelText: "ID Number",
                                      labelStyle:
                                          TextStyle(color: fromHex(blue)),
                                      hintStyle: TextStyle(
                                        color: Colors.blueGrey[400],
                                      ),
                                      border: getBorder(),
                                      focusedBorder: getBorder(),
                                      enabledBorder: getBorder(),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _passport,
                                    keyboardType: TextInputType.text,
                                    textCapitalization:
                                        TextCapitalization.characters,
                                    style: TextStyle(color: fromHex(blue)),
                                    validator: (value) {
                                      if (value.isEmpty &&
                                          _idNumber.text.isEmpty) {
                                        return "Please Enter your passport number";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      isDense: true,
                                      hintText: "AK000000",
                                      labelText: "Passport Number",
                                      labelStyle:
                                          TextStyle(color: fromHex(blue)),
                                      hintStyle: TextStyle(
                                        color: Colors.blueGrey[400],
                                      ),
                                      border: getBorder(),
                                      focusedBorder: getBorder(),
                                      enabledBorder: getBorder(),
                                      suffixIcon: Icon(
                                        Icons.edit,
                                        color: fromHex(blue),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: TextFormField(
                                    controller: _description,
                                    keyboardType: TextInputType.text,
                                    textCapitalization:
                                        TextCapitalization.sentences,
                                    maxLines: 4,
                                    style: TextStyle(color: fromHex(blue)),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Please add your bio";
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      alignLabelWithHint: true,
                                      filled: true,
                                      fillColor: Colors.white,
                                      isDense: true,
                                      hintText: "Enter your bio here...",
                                      labelText: "About You",
                                      labelStyle:
                                          TextStyle(color: fromHex(blue)),
                                      hintStyle: TextStyle(
                                        color: Colors.blueGrey[400],
                                      ),
                                      border: getBorder(),
                                      focusedBorder: getBorder(),
                                      enabledBorder: getBorder(),
                                      suffixIcon: Icon(
                                        Icons.edit,
                                        color: fromHex(blue),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: DropdownSearch<AccountTypeData>(
                                          mode: Mode.BOTTOM_SHEET,
                                          showClearButton: false,
                                          showSearchBox: true,
                                          searchBoxDecoration: InputDecoration(
                                              hintText: "Search Account Types",
                                              hintStyle: TextStyle(
                                                  color: Colors.grey[100]),
                                              prefixIcon: Icon(Icons.search,
                                                  color: Colors.white),
                                              filled: true,
                                              fillColor: Colors.white10,
                                              border: InputBorder.none,
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none),
                                          dropdownSearchDecoration:
                                              InputDecoration(
                                                  filled: true,
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                          horizontal: 10,
                                                          vertical: 0),
                                                  fillColor: Colors.transparent,
                                                  border: getBorder(),
                                                  enabledBorder: getBorder(),
                                                  focusedBorder: getBorder()),
                                          items: model.accountTypes,
                                          itemAsString: (AccountTypeData p) =>
                                              "${p.name ?? "Undefined"}",
                                          autoValidateMode: AutovalidateMode
                                              .onUserInteraction,
                                          validator: (AccountTypeData u) {
                                            if (accountTypeName.isEmpty) {
                                              return "Please choose Account Type";
                                            }
                                            return null;
                                          },
                                          onChanged: (AccountTypeData data) {
                                            accountTypeId = data.id;
                                            accountTypeName = data.name;
                                          },
                                          dropdownBuilder: _accountTypeDrop,
                                          popupBackgroundColor: fromHex(blue),
                                          popupItemBuilder: _accountTypePopUp,
                                          dropDownButton: Icon(
                                            Icons.arrow_drop_down_circle,
                                            color: fromHex(gold_lite),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: DropdownSearch<CountryData>(
                                          mode: Mode.BOTTOM_SHEET,
                                          showClearButton: false,
                                          showSearchBox: true,
                                          searchBoxDecoration: InputDecoration(
                                              hintText: "Search Countries",
                                              hintStyle: TextStyle(
                                                  color: Colors.grey[100]),
                                              prefixIcon: Icon(Icons.search,
                                                  color: Colors.white),
                                              filled: true,
                                              fillColor: Colors.white10,
                                              border: InputBorder.none,
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none),
                                          dropdownSearchDecoration:
                                              InputDecoration(
                                                  filled: true,
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                          horizontal: 10,
                                                          vertical: 0),
                                                  fillColor: Colors.transparent,
                                                  border: getBorder(),
                                                  enabledBorder: getBorder(),
                                                  focusedBorder: getBorder()),
                                          items: model.countries,
                                          itemAsString: (CountryData p) =>
                                              "${p.name ?? "Undefined"}",
                                          autoValidateMode: AutovalidateMode
                                              .onUserInteraction,
                                          validator: (CountryData u) {
                                            if (countryName.isEmpty) {
                                              return "Please choose country";
                                            }
                                            return null;
                                          },
                                          onChanged:
                                              (CountryData countryData) async {
                                            countryId = countryData.id;
                                            countryName = countryData.name;
                                          },
                                          dropdownBuilder: _countryDropDown,
                                          popupBackgroundColor: fromHex(blue),
                                          popupItemBuilder: _countryPopUp,
                                          dropDownButton: Icon(
                                            Icons.arrow_drop_down_circle,
                                            color: fromHex(gold_lite),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: DropdownSearch<RegionsData>(
                                          mode: Mode.BOTTOM_SHEET,
                                          showClearButton: false,
                                          showSearchBox: true,
                                          searchBoxDecoration: InputDecoration(
                                              hintText: "Search Regions",
                                              hintStyle: TextStyle(
                                                  color: Colors.grey[100]),
                                              prefixIcon: Icon(Icons.search,
                                                  color: Colors.white),
                                              filled: true,
                                              fillColor: Colors.white10,
                                              border: InputBorder.none,
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none),
                                          dropdownSearchDecoration:
                                              InputDecoration(
                                                  filled: true,
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                          horizontal: 10,
                                                          vertical: 0),
                                                  fillColor: Colors.transparent,
                                                  border: getBorder(),
                                                  enabledBorder: getBorder(),
                                                  focusedBorder: getBorder()),
                                          items: model.regions,
                                          itemAsString: (RegionsData p) =>
                                              "${p.name ?? "Undefined"}",
                                          autoValidateMode: AutovalidateMode
                                              .onUserInteraction,
                                          validator: (RegionsData u) {
                                            if (regionName.isEmpty) {
                                              return "Please choose region";
                                            }
                                            return null;
                                          },
                                          onChanged:
                                              (RegionsData regionData) async {
                                            regionId = regionData.id;
                                            regionName = regionData.name;
                                            Map<String, dynamic> data = {
                                              "page": "all",
                                              "is_active": true,
                                              "region": "${regionData.id}"
                                            };
                                            await model.getTowns(data);
                                            setState(() {
                                              townId = "";
                                              townName = "";
                                            });
                                          },
                                          dropdownBuilder: _regionDrop,
                                          popupBackgroundColor: fromHex(blue),
                                          popupItemBuilder: _regionPopUp,
                                          dropDownButton: Icon(
                                            Icons.arrow_drop_down_circle,
                                            color: fromHex(gold_lite),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 25, vertical: 5),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: DropdownSearch<TownsData>(
                                          mode: Mode.BOTTOM_SHEET,
                                          showClearButton: false,
                                          showSearchBox: true,
                                          searchBoxDecoration: InputDecoration(
                                              hintText: "Search Town",
                                              hintStyle: TextStyle(
                                                  color: Colors.grey[100]),
                                              prefixIcon: Icon(Icons.search,
                                                  color: Colors.white),
                                              filled: true,
                                              fillColor: Colors.white10,
                                              border: InputBorder.none,
                                              enabledBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none),
                                          dropdownSearchDecoration:
                                              InputDecoration(
                                                  filled: true,
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.symmetric(
                                                          horizontal: 10,
                                                          vertical: 0),
                                                  fillColor: Colors.transparent,
                                                  border: getBorder(),
                                                  enabledBorder: getBorder(),
                                                  focusedBorder: getBorder()),
                                          items: model.towns,
                                          itemAsString: (TownsData p) =>
                                              "${p.name ?? "Undefined"}",
                                          autoValidateMode: AutovalidateMode
                                              .onUserInteraction,
                                          validator: (TownsData u) {
                                            if (townName.isEmpty) {
                                              return "Please choose town";
                                            }
                                            return null;
                                          },
                                          onChanged: (TownsData townData) {
                                            townId = townData.id;
                                            townName = townData.name;
                                          },
                                          dropdownBuilder: _townDropDown,
                                          popupBackgroundColor: fromHex(blue),
                                          popupItemBuilder: _townPopUp,
                                          dropDownButton: Icon(
                                            Icons.arrow_drop_down_circle,
                                            color: fromHex(gold_lite),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                              ],
                            ),
                          ),
                        )
                      : Center(
                          child: CircularProgressIndicator(
                            backgroundColor: fromHex(gold_dark),
                            valueColor: new AlwaysStoppedAnimation<Color>(
                              fromHex(grey),
                            ),
                          ),
                        ),
                ),
                model.state == ViewState.Idle
                    ? Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 25, vertical: 15),
                        child: RaisedButton(
                          elevation: 5.0,
                          onPressed: () async {
                            hasConnection = await checkConnection();
                            if (!hasConnection) {
                              showToast("Check Internet Connection");
                              return;
                            }
                            if (_formKey.currentState.validate()) {
                              Name n =
                                  Name(first: _fname.text, others: _lname.text);
                              Map<String, dynamic> data = {
                                "name": n.toJson(),
                                "is_super_admin": getCurrentUser().isSuperAdmin,
                                "user_type": "${getCurrentUser().userType}",
                                "created_at": "${getCurrentUser().createdAt}",
                                "description": "${_description.text}",
                                "is_active": true,
                                "is_approved": false,
                                "email": "${_email.text}",
                                "phone": "${_phone.text}",
                                "account_type": "$accountTypeId",
                                "id_number": "${_idNumber.text}",
                                "passport_number": "${_passport.text}",
                                "admission_number":
                                    "${getCurrentUser().admissionNumber}",
                                "country": "$countryId",
                                "region": "$regionId",
                                "town": "$townId",
                                "photo": ""
                              };
                              print(data);
                              try {
                                var r = await model.updateUserDetails(data);
                                showToast(r.status);
                                Navigator.pushNamedAndRemoveUntil(
                                    context,
                                    HomePage.tag,
                                    (Route<dynamic> route) => false);
                              } catch (e) {
                                showToast(e.toString());
                              }
                            }
                          },
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20),
                            ),
                          ),
                          color: fromHex(blue),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 15.0),
                            child: Center(
                              child: Text(
                                'Update',
                                style: TextStyle(
                                    color: fromHex(gold_dark), fontSize: 18),
                              ),
                            ),
                          ),
                        ),
                      )
                    : Container(),
              ],
            ),
          ),
          model.state == ViewState.Idle
              ? Positioned(
                  left: 20,
                  top: 35,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: fromHex(blue),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    mini: true,
                    tooltip: "go back",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(gold_dark),
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  Widget _accountTypeDrop(
      BuildContext context, AccountTypeData item, String itemDesignation) {
    return Container(
      child: Text(
        accountTypeName.isNotEmpty
            ? "$accountTypeName"
            : "${item?.name ?? 'Select Town'}",
        style: TextStyle(
          color: fromHex(blue),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _accountTypePopUp(
      BuildContext context, AccountTypeData item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item.name ?? 'undefined',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _townDropDown(
      BuildContext context, TownsData item, String itemDesignation) {
    return Container(
      child: Text(
        townName.isNotEmpty ? "$townName" : "${item?.name ?? 'Select Town'}",
        style: TextStyle(
          color: fromHex(blue),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _townPopUp(BuildContext context, TownsData item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item.name ?? 'undefined',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _countryDropDown(
      BuildContext context, CountryData item, String itemDesignation) {
    return Container(
      child: Text(
        countryName.isNotEmpty
            ? "$countryName"
            : "${item?.name ?? 'Select Country'}",
        style: TextStyle(
          color: fromHex(blue),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _countryPopUp(
      BuildContext context, CountryData item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item.name ?? 'undefined',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _regionDrop(
      BuildContext context, RegionsData item, String itemDesignation) {
    return Container(
      child: Text(
        regionName.isNotEmpty
            ? "$regionName"
            : "${item?.name ?? 'Select Region'}",
        style: TextStyle(
          color: fromHex(blue),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _regionPopUp(BuildContext context, RegionsData item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item.name ?? 'undefined',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }
}
