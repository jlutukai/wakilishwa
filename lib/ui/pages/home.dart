import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wakilishwa/core/viewmodels/alerts_model.dart';
import 'package:wakilishwa/ui/pages/profile.dart';
import 'package:wakilishwa/ui/pages/reports.dart';
import 'package:wakilishwa/ui/widgets/alerts.dart';
import 'package:wakilishwa/ui/widgets/bids.dart';
import 'package:wakilishwa/ui/widgets/contracts.dart';
import 'package:wakilishwa/ui/widgets/jobs.dart';
import 'package:wakilishwa/ui/widgets/user_profile.dart';
import 'package:wakilishwa/utils/constants.dart';

import '../../locator.dart';
import 'load_e_wallet.dart';
import 'login.dart';
import 'my_transactions.dart';
import 'my_wallet.dart';
import 'withdraw_to_mpesa.dart';

class HomePage extends StatefulWidget {
  static const tag = 'home_page';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  int _jobsIndex = 0;
  PageController _pageController;
  AlertsModel _model = locator<AlertsModel>();

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
    Map<String, dynamic> data = {"page": "all"};
    _model.getAlerts(data);
    // deleteAddUserData();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void onDataChange(int index, [int jobsIndex]) {
    setState(() {
      if (jobsIndex != null) {
        _jobsIndex = jobsIndex;
      }
      _pageController.animateToPage(index,
          duration: Duration(milliseconds: 10), curve: Curves.easeOut);
    });
  }

  onWillPop(context) async {
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => onWillPop(context),
      child: SafeArea(
        child: Stack(
          children: [
            Scaffold(
                backgroundColor: Colors.transparent,
                body: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _pageController,
                  onPageChanged: (index) {
                    setState(() {
                      _currentIndex = index;
                    });
                  },
                  children: <Widget>[
                    UserProfile(onDataChange),
                    JobsWidget(_jobsIndex),
                    Bids(),
                    Contracts(),
                    Alerts(),
                  ],
                ),
                bottomNavigationBar: _bottomBarWidget(_currentIndex, context)),
            Positioned(
              left: 20,
              top: 20,
              child: FloatingActionButton(
                foregroundColor: Colors.grey,
                backgroundColor: fromHex(blue),
                onPressed: () {
                  _navmenu(context);
                },
                mini: true,
                heroTag: "menu",
                tooltip: "menu",
                child: Icon(
                  Icons.menu,
                  color: fromHex(gold_dark),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _navmenu(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        clipBehavior: Clip.antiAlias,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Container(
              color: Colors.transparent,
              child: Container(
//                height: MediaQuery.of(context).size.height * 0.75,
                decoration: BoxDecoration(
                  color: fromHex(blue),
                  // color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(40),
                      topRight: const Radius.circular(40)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                  child: Column(
                    // mainAxisSize: MainAxisSize.max,
                    children: [
                      ListTile(
                        title: Center(
                          child: Text(
                            "MY PROFILE",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w100,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.pushNamed(context, Profile.tag);
                        },
                      ),
                      ListTile(
                        title: Center(
                          child: Text(
                            "MY WALLET",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w100,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) => MyWallet()),
                          );
                        },
                      ),
                      ListTile(
                        title: Center(
                          child: Text(
                            "LOAD E-WALLET",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w100,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    LoadEWallet()),
                          );
                        },
                      ),
                      ListTile(
                        title: Center(
                          child: Text(
                            "WITHDRAW TO MPESA",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w100,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    WithdrawTOMpesa()),
                          );
                        },
                      ),
                      ListTile(
                        title: Center(
                          child: Text(
                            "MY TRANSACTIONS",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w100,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    MyTransactions()),
                          );
                        },
                      ),
                      ListTile(
                        title: Center(
                          child: Text(
                            "REPORTS",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w100,
                              fontSize: 18,
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      Reports()));
                        },
                      ),
                      ListTile(
                        onTap: () {
                          deleteToken();
                          deleteCurrentUser();
                          deleteAccountInfo();
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              LoginPage.tag, (Route<dynamic> route) => false);
                        },
                        title: Center(
                          child: Text(
                            'SIGN OUT',
                            style: TextStyle(
                              color: fromHex(gold_dark),
                              fontWeight: FontWeight.w100,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  void _onItemTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  void _navigateToScreens(int index) {
    setState(() {
      _currentIndex = index;
      _pageController.animateToPage(index,
          duration: Duration(milliseconds: 10), curve: Curves.easeOut);
    });
  }

  _bottomBarWidget(int currentIndex, BuildContext context) {
    int notificationCount = int.tryParse(getNotificationCount() ?? "0");
    if (currentIndex == 0) {
      return BottomAppBar(
          elevation: 0,
          color: fromHex(blue),
          child: Container(
            height: 60,
            child: Row(mainAxisSize: MainAxisSize.max,
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 0;
                        });
                        _navigateToScreens(0);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.account_balance,
                            color: Colors.white,
                          ),
                          Text(
                            'HOME',
                            style: TextStyle(
                              color: fromHex(gold_lite),
                              // fontWeight: FontWeight.w200,
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 1;
                        });
                        _navigateToScreens(1);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.work,
                            color: fromHex(gold_dark),
                          ),
                          Text(
                            'JOBS',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 2;
                        });
                        _navigateToScreens(2);
                      },
                      child: Column(
                        children: <Widget>[
                          SvgPicture.asset(
                            "assets/mace.svg",
                            width: 24,
                            height: 24,
                            color: fromHex(gold_dark),
                          ),
                          Text(
                            'BIDS',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 3;
                        });
                        _navigateToScreens(3);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.assignment,
                            color: fromHex(gold_dark),
                          ),
                          Text(
                            'CONTRACTS',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 4;
                        });
                        _navigateToScreens(4);
                      },
                      child: Column(
                        children: <Widget>[
                          notificationCount > 0
                              ? Badge(
                                  badgeContent: Text(
                                    "$notificationCount",
                                    style: TextStyle(color: fromHex(blue)),
                                  ),
                                  badgeColor: fromHex(gold_dark),
                                  child: Icon(
                                    Icons.notifications,
                                    color: fromHex(gold_dark),
                                  ),
                                )
                              : Icon(
                                  Icons.notifications,
                                  color: fromHex(gold_dark),
                                ),
                          Text(
                            'ALERTS',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ]),
          ));
    } else if (currentIndex == 1) {
      return BottomAppBar(
          elevation: 0,
          color: Colors.white,
          child: Container(
            height: 60,
            child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 0;
                        });
                        _navigateToScreens(0);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.account_balance,
                            color: fromHex(gold_dark),
                          ),
                          Text(
                            'HOME',
                            style: TextStyle(
                              color: fromHex(blue),
                              // fontWeight: FontWeight.w200,
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 1;
                        });
                        _navigateToScreens(1);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.work,
                            color: fromHex(blue),
                          ),
                          Text(
                            'JOBS',
                            style: TextStyle(
                              color: fromHex(gold_dark),
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 2;
                        });
                        _navigateToScreens(2);
                      },
                      child: Column(
                        children: <Widget>[
                          SvgPicture.asset(
                            "assets/mace.svg",
                            width: 24,
                            height: 24,
                            color: fromHex(gold_dark),
                          ),
                          Text(
                            'BIDS',
                            style: TextStyle(
                              color: fromHex(blue),
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 3;
                        });
                        _navigateToScreens(3);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.assignment,
                            color: fromHex(gold_dark),
                          ),
                          Text(
                            'CONTRACTS',
                            style: TextStyle(
                              color: fromHex(blue),
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 4;
                        });
                        _navigateToScreens(4);
                      },
                      child: Column(
                        children: <Widget>[
                          notificationCount > 0
                              ? Badge(
                                  badgeContent: Text(
                                    "$notificationCount",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  badgeColor: fromHex(gold_dark),
                                  child: Icon(
                                    Icons.notifications,
                                    color: fromHex(gold_dark),
                                  ),
                                )
                              : Icon(
                                  Icons.notifications,
                                  color: fromHex(gold_dark),
                                ),
                          Text(
                            'ALERTS',
                            style: TextStyle(
                              color: fromHex(blue),
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ]),
          ));
    } else if (currentIndex == 2) {
      return BottomAppBar(
          elevation: 0,
          color: Colors.white,
          child: Container(
            height: 60,
            child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 0;
                        });
                        _navigateToScreens(0);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.account_balance,
                            color: fromHex(gold_dark),
                          ),
                          Text(
                            'HOME',
                            style: TextStyle(
                              color: fromHex(blue),
                              // fontWeight: FontWeight.w200,
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 1;
                        });
                        _navigateToScreens(1);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.work,
                            color: fromHex(gold_dark),
                          ),
                          Text(
                            'JOBS',
                            style: TextStyle(
                              color: fromHex(blue),
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 2;
                        });
                        _navigateToScreens(2);
                      },
                      child: Column(
                        children: <Widget>[
                          SvgPicture.asset(
                            "assets/mace.svg",
                            width: 24,
                            height: 24,
                            color: fromHex(blue),
                          ),
                          Text(
                            'BIDS',
                            style: TextStyle(
                              color: fromHex(gold_dark),
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 3;
                        });
                        _navigateToScreens(3);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.assignment,
                            color: fromHex(gold_dark),
                          ),
                          Text(
                            'CONTRACTS',
                            style: TextStyle(
                              color: fromHex(blue),
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 4;
                        });
                        _navigateToScreens(4);
                      },
                      child: Column(
                        children: <Widget>[
                          notificationCount > 0
                              ? Badge(
                                  badgeContent: Text(
                                    "$notificationCount",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  badgeColor: fromHex(gold_dark),
                                  child: Icon(
                                    Icons.notifications,
                                    color: fromHex(gold_dark),
                                  ),
                                )
                              : Icon(
                                  Icons.notifications,
                                  color: fromHex(gold_dark),
                                ),
                          Text(
                            'ALERTS',
                            style: TextStyle(
                              color: fromHex(blue),
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ]),
          ));
    } else if (currentIndex == 3) {
      return BottomAppBar(
          elevation: 0,
          color: fromHex(blue).withOpacity(0.7),
          child: Container(
            height: 60,
            child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 0;
                        });
                        _navigateToScreens(0);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.account_balance,
                            color: fromHex(gold_lite),
                          ),
                          Text(
                            'HOME',
                            style: TextStyle(
                              color: Colors.white,
                              // fontWeight: FontWeight.w200,
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_dark),
                    width: .5,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 1;
                        });
                        _navigateToScreens(1);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.work,
                            color: fromHex(gold_lite),
                          ),
                          Text(
                            'JOBS',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_dark),
                    width: .5,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 2;
                        });
                        _navigateToScreens(2);
                      },
                      child: Column(
                        children: <Widget>[
                          SvgPicture.asset(
                            "assets/mace.svg",
                            width: 24,
                            height: 24,
                            color: fromHex(gold_lite),
                          ),
                          Text(
                            'BIDS',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_dark),
                    width: .5,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 3;
                        });
                        _navigateToScreens(3);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.assignment,
                            color: Colors.white,
                          ),
                          Text(
                            'CONTRACTS',
                            style: TextStyle(
                              color: fromHex(gold_lite),
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_dark),
                    width: .5,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 4;
                        });
                        _navigateToScreens(4);
                      },
                      child: Column(
                        children: <Widget>[
                          notificationCount > 0
                              ? Badge(
                                  badgeContent: Text(
                                    "$notificationCount",
                                    style: TextStyle(color: fromHex(blue)),
                                  ),
                                  badgeColor: fromHex(gold_dark),
                                  child: Icon(
                                    Icons.notifications,
                                    color: fromHex(gold_lite),
                                  ),
                                )
                              : Icon(
                                  Icons.notifications,
                                  color: fromHex(gold_lite),
                                ),
                          Text(
                            'ALERTS',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ]),
          ));
    } else if (currentIndex == 4) {
      return BottomAppBar(
          elevation: 0,
          color: Colors.white,
          child: Container(
            height: 60,
            child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 0;
                        });
                        _navigateToScreens(0);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.account_balance,
                            color: fromHex(gold_dark),
                          ),
                          Text(
                            'HOME',
                            style: TextStyle(
                              color: fromHex(blue),
                              // fontWeight: FontWeight.w200,
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 1;
                        });
                        _navigateToScreens(1);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.work,
                            color: fromHex(gold_dark),
                          ),
                          Text(
                            'JOBS',
                            style: TextStyle(
                              color: fromHex(blue),
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 2;
                        });
                        _navigateToScreens(2);
                      },
                      child: Column(
                        children: <Widget>[
                          SvgPicture.asset(
                            "assets/mace.svg",
                            width: 24,
                            height: 24,
                            color: fromHex(gold_dark),
                          ),
                          Text(
                            'BIDS',
                            style: TextStyle(
                              color: fromHex(blue),
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 3;
                        });
                        _navigateToScreens(3);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.assignment,
                            color: fromHex(gold_dark),
                          ),
                          Text(
                            'CONTRACTS',
                            style: TextStyle(
                              color: fromHex(blue),
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: fromHex(gold_lite),
                    width: 1,
                    margin: EdgeInsets.symmetric(vertical: 10),
                  ),
                  Expanded(
                    child: FlatButton(
                      minWidth: 30,
                      padding: EdgeInsets.symmetric(vertical: 10),
                      onPressed: () {
                        setState(() {
                          this._currentIndex = 4;
                        });
                        _navigateToScreens(4);
                      },
                      child: Column(
                        children: <Widget>[
                          notificationCount > 0
                              ? Badge(
                                  badgeContent: Text(
                                    "$notificationCount",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  badgeColor: fromHex(gold_dark),
                                  child: Icon(
                                    Icons.notifications,
                                    color: fromHex(blue),
                                  ),
                                )
                              : Icon(
                                  Icons.notifications,
                                  color: fromHex(blue),
                                ),
                          Text(
                            'ALERTS',
                            style: TextStyle(
                              color: fromHex(gold_dark),
                              fontSize: 7,
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ]),
          ));
    }
  }
}

alerts() {
  return Container(
    color: Colors.red,
  );
}

contracts() {
  return Container(
    color: Colors.yellow,
  );
}

bids() {
  return Container(
    color: Colors.purple,
  );
}

homeWidget(BuildContext context) {
  return Container(
    decoration: BoxDecoration(
      image: DecorationImage(
        image: AssetImage("assets/bg.png"),
        fit: BoxFit.cover,
      ),
    ),
    child: Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        children: [
          Expanded(
            child: Container(
              height: MediaQuery.of(context).size.height * .25,
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 45, right: 20, left: 20, bottom: 25),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40), topRight: Radius.circular(40)),
              color: fromHex(blue),
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Adv. John Smith',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              color: fromHex(gold_dark),
                            ),
                          ),
                          Text(
                            'Advocate',
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          'Ratings  ',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 11,
                          ),
                        ),
                        RatingBar.builder(
                          initialRating: 5,
                          minRating: 1,
                          direction: Axis.horizontal,
                          allowHalfRating: true,
                          itemCount: 5,
                          itemSize: 12.7,
                          ignoreGestures: true,
                          itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                          itemBuilder: (context, _) => Icon(
                            Icons.star,
                            color: fromHex(gold_lite),
                          ),
                          onRatingUpdate: (rating) {
                            print(rating);
                          },
                        ),
                      ],
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Divider(
                    thickness: 1.5,
                    color: Colors.grey.shade800,
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'About',
                      style: TextStyle(
                        color: fromHex(gold_dark),
                        fontWeight: FontWeight.bold,
                        fontSize: 13,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                      style: TextStyle(color: Colors.white, fontSize: 12),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Divider(
                    thickness: 1.5,
                    color: Colors.grey.shade800,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'MY JOBS',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.arrow_drop_down_circle,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              'VIEW MY JOBS',
                              style: TextStyle(
                                  color: fromHex(gold_dark), fontSize: 10),
                            )
                          ],
                        )
                      ],
                    ),
                    Text(
                      "10",
                      style: TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                          fontWeight: FontWeight.w900),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Divider(
                    thickness: 1.5,
                    color: Colors.grey.shade800,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'MY POSTED JOBS',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.arrow_drop_down_circle,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              'VIEW JOBS I POSTED',
                              style: TextStyle(
                                  color: fromHex(gold_dark), fontSize: 10),
                            )
                          ],
                        )
                      ],
                    ),
                    Text(
                      "9",
                      style: TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                          fontWeight: FontWeight.w900),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Divider(
                    thickness: 1.5,
                    color: Colors.grey.shade800,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'CONTRACTS',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.arrow_drop_down_circle,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              'VIEW MY CONTRACTS',
                              style: TextStyle(
                                  color: fromHex(gold_dark), fontSize: 10),
                            )
                          ],
                        )
                      ],
                    ),
                    Text(
                      "9",
                      style: TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                          fontWeight: FontWeight.w900),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    ),
  );
}

// bottomNavigationBar: Theme(
// data: Theme.of(context).copyWith(
// canvasColor: Colors.transparent,
// primaryColor: Colors.red,
// ),
// child: BottomNavigationBar(
// currentIndex: _currentIndex,
// onTap: (int index) {
// setState(() {
// this._currentIndex = index;
// });
// _navigateToScreens(index);
// },
// type: BottomNavigationBarType.shifting,
// items: [
// new BottomNavigationBarItem(
// backgroundColor: Colors.white,
// icon: _currentIndex == 0
// ? Container(
// child: Icon(
// Icons.account_balance,
// color: fromHex(blue),
// ),
// )
// : Container(
// child: Icon(
// Icons.account_balance,
// color: Colors.blueGrey[300],
// ),
// ),
// label: "Home"),
// BottomNavigationBarItem(
// icon: _currentIndex == 1
// ? Container(
// child: Icon(
// Icons.work,
// color: fromHex(gold_lite),
// ),
// )
// : Container(
// child: Icon(
// Icons.work,
// color: fromHex(blue),
// ),
// ),
// label: "jobs",
// ),
// new BottomNavigationBarItem(
// icon: _currentIndex == 2
// ? Container(
// child: Icon(
// Icons.clear,
// color: fromHex('#ffe37a'),
// ),
// )
// : Container(
// child: Icon(
// Icons.clear,
// color: Colors.blueGrey[300],
// ),
// ),
// label: "bids",
// ),
// BottomNavigationBarItem(
// icon: _currentIndex == 3
// ? Container(
// child: Icon(
// Icons.assignment,
// color: fromHex('#ffe37a'),
// ),
// )
// : Container(
// child: Icon(
// Icons.assignment,
// color: Colors.blueGrey[300],
// ),
// ),
// label: "contracts",
// ),
// BottomNavigationBarItem(
// icon: _currentIndex == 4
// ? Container(
// child: Icon(
// Icons.notifications,
// color: fromHex('#ffe37a'),
// ),
// )
// : Container(
// child: Icon(
// Icons.notifications,
// color: Colors.blueGrey[300],
// ),
// ),
// label: "Alerts",
// ),
// ],
// ),
// ),
