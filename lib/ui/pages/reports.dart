import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/viewmodels/reports_model.dart';
import 'package:wakilishwa/ui/pages/transactions_report.dart';

import '../../utils/constants.dart';
import 'base_view.dart';

class Reports extends StatefulWidget {
  static const tag = 'reports';
  @override
  _ReportsState createState() => _ReportsState();
}

class _ReportsState extends State<Reports> {
  int _radioValue = -1;
  TextEditingController _startDate = TextEditingController();
  TextEditingController _endDate = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BaseView<ReportsModel>(
      builder: (context, model, child) => Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.white,
            resizeToAvoidBottomInset: true,
            body: Form(
              key: _formKey,
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height * 0.25,
                        margin: EdgeInsets.only(bottom: 60),
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: fromHex(blue),
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(40),
                            bottomRight: Radius.circular(40),
                          ),
                        ),
                      ),
                      Positioned(
                          bottom: 1,
                          right: 0,
                          left: 0,
                          child: CircleAvatar(
                            radius: 60,
                            backgroundColor: Colors.white,
                            child: CircleAvatar(
                              radius: 55,
                              backgroundColor: fromHex(gold_lite),
                              child: Icon(
                                Icons.receipt,
                                color: Colors.white,
                                size: 66,
                              ),
                            ),
                          ))
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'REPORTS',
                    style: TextStyle(
                        color: fromHex(blue),
                        fontSize: 20,
                        fontWeight: FontWeight.w900),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Theme(
                    data: ThemeData(accentColor: fromHex(grey)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Radio(
                          value: 0,
                          activeColor: fromHex(gold_lite),
                          groupValue: _radioValue,
                          onChanged: _handleRadioValueChange,
                        ),
                        Text(
                          '1 Month',
                          style: TextStyle(
                            fontSize: 12.0,
                            color: fromHex(blue),
                          ),
                        ),
                        Radio(
                          value: 1,
                          groupValue: _radioValue,
                          activeColor: fromHex(gold_lite),
                          onChanged: _handleRadioValueChange,
                        ),
                        Text(
                          '3 Months',
                          style: TextStyle(
                            fontSize: 12.0,
                            color: fromHex(blue),
                          ),
                        ),
                        Radio(
                          value: 2,
                          groupValue: _radioValue,
                          activeColor: fromHex(gold_lite),
                          onChanged: _handleRadioValueChange,
                        ),
                        Text(
                          '6 Months',
                          style: TextStyle(
                            fontSize: 12.0,
                            color: fromHex(blue),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'CUSTOM DATE(S)',
                    style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 12,
                        color: fromHex(blue)),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 25, vertical: 5),
                    child: TextFormField(
                      controller: _startDate,
                      keyboardType: TextInputType.text,
                      style: TextStyle(color: fromHex(blue)),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please choose start date";
                        }
                        return null;
                      },
                      enableInteractiveSelection: false,
                      autofocus: false,
                      readOnly: true,
                      onTap: () async {
                        DatePicker.showDatePicker(context,
                            showTitleActions: true,
                            minTime: DateTime(1918, 3, 5),
                            maxTime: DateTime.now(),
                            theme: DatePickerTheme(
                              headerColor: fromHex(blue),
                              backgroundColor: Colors.white,
                              itemStyle: TextStyle(
                                color: fromHex(blue),
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                              ),
                              cancelStyle: TextStyle(
                                color: fromHex(gold_lite),
                                fontSize: 24,
                              ),
                              doneStyle: TextStyle(
                                color: fromHex(gold_lite),
                                fontSize: 24,
                              ),
                            ), onChanged: (date) {
                          print('change $date in time zone ' +
                              date.timeZoneOffset.inHours.toString());
                        }, onConfirm: (date) {
                          _startDate.text = date.toString().substring(0, 10);
                        }, currentTime: DateTime.now(), locale: LocaleType.en);
                      },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        isDense: true,
                        hintText: "date",
                        labelText: "ENTER START DATE",
                        labelStyle: TextStyle(color: fromHex(blue)),
                        hintStyle: TextStyle(
                          color: Colors.blueGrey[400],
                        ),
                        border: getBorder(),
                        focusedBorder: getBorder(),
                        enabledBorder: getBorder(),
                        disabledBorder: getBorder(),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 25, vertical: 5),
                    child: TextFormField(
                      controller: _endDate,
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.characters,
                      style: TextStyle(color: fromHex(blue)),
                      enableInteractiveSelection: false,
                      autofocus: false,
                      readOnly: true,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please choose end date";
                        }
                        return null;
                      },
                      onTap: () async {
                        DatePicker.showDatePicker(context,
                            showTitleActions: true,
                            minTime: DateTime(1918, 3, 5),
                            maxTime: DateTime.now(),
                            theme: DatePickerTheme(
                              headerColor: fromHex(blue),
                              backgroundColor: Colors.white,
                              itemStyle: TextStyle(
                                color: fromHex(blue),
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                              ),
                              cancelStyle: TextStyle(
                                color: fromHex(gold_lite),
                                fontSize: 24,
                              ),
                              doneStyle: TextStyle(
                                color: fromHex(gold_lite),
                                fontSize: 24,
                              ),
                            ), onChanged: (date) {
                          print('change $date in time zone ' +
                              date.timeZoneOffset.inHours.toString());
                        }, onConfirm: (date) {
                          _endDate.text = date.toString().substring(0, 10);
                        }, currentTime: DateTime.now(), locale: LocaleType.en);
                      },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        isDense: true,
                        hintText: "date",
                        labelText: "ENTER END DATE",
                        labelStyle: TextStyle(color: fromHex(blue)),
                        hintStyle: TextStyle(
                          color: Colors.blueGrey[400],
                        ),
                        border: getBorder(),
                        focusedBorder: getBorder(),
                        enabledBorder: getBorder(),
                        disabledBorder: getBorder(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  model.state == ViewState.Idle
                      ? InkWell(
                          onTap: () async {
                            bool hasConnection = await checkConnection();
                            if (!hasConnection) {
                              showToast("Check Internet Connection");
                              return;
                            }
                            if (_formKey.currentState.validate()) {
                              Map<String, dynamic> data = {
                                "startDate": "${_startDate.text}",
                                "endDate": "${_startDate.text}",
                                "page": "all"
                              };
                              try {
                                var r = await model.getReports(data);
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        TransactionsReport(model.transactions),
                                  ),
                                );
                              } catch (e) {
                                try {
                                  Map<String, dynamic> error =
                                      jsonDecode(e.toString());
                                  showToast(error['errors'][0]['message']);
                                } catch (a) {}
                              }
                            }
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.symmetric(
                                vertical: 15, horizontal: 34),
                            margin: EdgeInsets.symmetric(horizontal: 25),
                            decoration: BoxDecoration(
                              color: fromHex(blue),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                bottomLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                              ),
                            ),
                            child: Center(
                              child: Text(
                                'REQUEST REPORT',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w900),
                              ),
                            ),
                          ),
                        )
                      : Center(
                          child: CircularProgressIndicator(
                            backgroundColor: fromHex(gold_dark),
                            valueColor: new AlwaysStoppedAnimation<Color>(
                              fromHex(grey),
                            ),
                          ),
                        ),
                  SizedBox(
                    height: 30,
                  ),
                  Expanded(child: Container()),
                  Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: fromHex(blue),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            left: 20,
            top: 35,
            child: FloatingActionButton(
              foregroundColor: Colors.grey,
              backgroundColor: fromHex(blue),
              onPressed: () {
                Navigator.of(context).pop();
              },
              mini: true,
              tooltip: "go back",
              child: Icon(
                Icons.keyboard_backspace_sharp,
                color: fromHex(gold_dark),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _handleRadioValueChange(int value) {
    print(value);
    setState(() {
      _radioValue = value;
    });
    if (value == 0) {
      setState(() {
        _startDate.text = DateTime.now()
            .subtract(Duration(days: 30))
            .toString()
            .substring(0, 10);
        _endDate.text = DateTime.now().toString().substring(0, 10);
      });
    }
    if (value == 1) {
      setState(() {
        _startDate.text = DateTime.now()
            .subtract(Duration(days: 90))
            .toString()
            .substring(0, 10);
        _endDate.text = DateTime.now().toString().substring(0, 10);
      });
    }
    if (value == 2) {
      setState(() {
        _startDate.text = DateTime.now()
            .subtract(Duration(days: 180))
            .toString()
            .substring(0, 10);
        _endDate.text = DateTime.now().toString().substring(0, 10);
      });
    }
  }
}
