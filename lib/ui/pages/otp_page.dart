import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sms_otp_auto_verify/sms_otp_auto_verify.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/local/local_models.dart';
import 'package:wakilishwa/core/viewmodels/otp_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';
import 'package:wakilishwa/ui/pages/login.dart';
import 'package:wakilishwa/ui/widgets/green_clipper.dart';
import 'package:wakilishwa/utils/constants.dart';

import 'change_password_page.dart';

class OTPPage extends StatefulWidget {
  final String s;
  final String email;

  OTPPage(this.s, this.email);
  @override
  _OTPPageState createState() => _OTPPageState();
}

class _OTPPageState extends State<OTPPage> {
  int _otpCodeLength = 6;
  bool _isLoadingButton = false;
  bool _enableButton = false;
  String _otpCode = "";
  String email = "";
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    email = widget.email;
    _getSignatureCode();
    super.initState();
  }

  /// get signature code
  _getSignatureCode() async {
    String signature = await SmsRetrieved.getAppSignature();
    print("signature $signature");
  }

  _onSubmitOtp(OTPModel model) async {
    bool hasConnection = await checkConnection();
    if (!hasConnection) {
      showToast("Check Internet Connection");
      return;
    }
    setState(() {
      _isLoadingButton = !_isLoadingButton;
      _verifyOtpCode(model);
    });
  }

  _onOtpCallBack(String otpCode, bool isAutoFill, OTPModel model) async {
    bool hasConnection = await checkConnection();
    if (!hasConnection) {
      showToast("Check Internet Connection");
      return;
    }
    setState(() {
      this._otpCode = otpCode;
      if (otpCode.length == _otpCodeLength && isAutoFill) {
        _enableButton = false;
        _isLoadingButton = true;
        _verifyOtpCode(model);
      } else if (otpCode.length == _otpCodeLength && !isAutoFill) {
        _enableButton = true;
        _isLoadingButton = false;
      } else {
        _enableButton = false;
      }
    });
  }

  _verifyOtpCode(OTPModel model) async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (widget.s == "login") {
      Map<String, String> data = {"code": "$_otpCode"};
      try {
        var r = await model.verifyOTP(data);
        showToast(r?.message ?? r?.status ?? '');
        CreateAccountBody c = getAddUserData();
        c.hasActivatedAccount = true;
        setAddUserData(c);
        setState(() {
          _isLoadingButton = false;
          _enableButton = false;
        });
        deleteAddUserData();
        Navigator.pushNamedAndRemoveUntil(
            context, LoginPage.tag, (Route<dynamic> route) => false);
      } catch (e) {
        setState(() {
          _isLoadingButton = false;
          _enableButton = false;
        });
        try {
          Map<String, dynamic> error = jsonDecode(e.toString());
          showToast(error['errors'][0]['message']);
        } catch (a) {}
      }
    } else {
      setState(() {
        _isLoadingButton = !_isLoadingButton;
      });
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => ChangePasswordPage(_otpCode)),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<OTPModel>(
      onModelReady: (model) async {
        bool hasConnection = await checkConnection();
        if (!hasConnection) {
          showToast("Check Internet Connection");
          return;
        }
      },
      builder: (context, model, child) => Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.white,
            resizeToAvoidBottomInset: true,
            body: Container(
              child: Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * .30,
                  ),
                  Expanded(
                    child: model.state == ViewState.Idle
                        ? SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Container(
                              height: MediaQuery.of(context).size.height * .70,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Account Verification",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            color: fromHex(blue),
                                            fontSize: 20),
                                      ),
                                    ],
                                  ),
                                  TextFieldPin(
                                      filled: true,
                                      filledColor:
                                          fromHex(grey).withOpacity(0.2),
                                      codeLength: _otpCodeLength,
                                      boxSize: 46,
                                      filledAfterTextChange: false,
                                      textStyle: TextStyle(fontSize: 16),
                                      borderStyle: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: fromHex(gold_lite),
                                              width: 2),
                                          borderRadius:
                                              BorderRadius.circular(34)),
                                      onOtpCallback: (code, isAutoFill) async {
                                        await _onOtpCallBack(
                                            code, isAutoFill, model);
                                      }),
                                  InkWell(
                                    onTap: () async {
                                      bool hasConnection =
                                          await checkConnection();
                                      if (!hasConnection) {
                                        showToast("Check Internet Connection");
                                        return;
                                      }
                                      Map<String, String> data = {
                                        "username": "$email"
                                      };
                                      try {
                                        var r = await model.resendOTP(data);
                                        showToast(r.message);
                                      } catch (e) {
                                        try {
                                          Map<String, dynamic> error =
                                              jsonDecode(e.toString());
                                          showToast(
                                              error['errors'][0]['message']);
                                        } catch (a) {}
                                      }
                                    },
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          "Didn't Receive Code? ",
                                          style: TextStyle(
                                            color: fromHex(blue),
                                          ),
                                        ),
                                        Text(
                                          "Resend",
                                          style: TextStyle(
                                              color: fromHex(blue),
                                              fontWeight: FontWeight.w800),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin:
                                        EdgeInsets.only(left: 20, right: 20),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            _isLoadingButton
                                                ? Center(
                                                    child: Container(
                                                      margin: EdgeInsets.only(
                                                          right: 30),
                                                      child:
                                                          CircularProgressIndicator(
                                                        backgroundColor:
                                                            fromHex(gold_dark),
                                                        valueColor:
                                                            new AlwaysStoppedAnimation<
                                                                Color>(
                                                          fromHex(grey),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                : InkWell(
                                                    onTap: _enableButton
                                                        ? () async {
                                                            await _onSubmitOtp(
                                                                model);
                                                          }
                                                        : null,
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              vertical: 15,
                                                              horizontal: 34),
                                                      margin:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 25),
                                                      decoration: BoxDecoration(
                                                        color: fromHex(blue),
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  20),
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  20),
                                                          bottomRight:
                                                              Radius.circular(
                                                                  20),
                                                        ),
                                                      ),
                                                      child: Center(
                                                        child: Text(
                                                          'Verify',
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 15,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w900),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                        : Center(
                            child: CircularProgressIndicator(
                              backgroundColor: fromHex(gold_dark),
                              valueColor: new AlwaysStoppedAnimation<Color>(
                                fromHex(grey),
                              ),
                            ),
                          ),
                  ),
                ],
              ),
            ),
          ),
          ClipPath(
            clipper: GreenClipper(),
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/bg.png"),
                  fit: BoxFit.fitWidth,
                  colorFilter: new ColorFilter.mode(
                      fromHex(blue).withOpacity(0.9), BlendMode.dstOver),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
//702660953
