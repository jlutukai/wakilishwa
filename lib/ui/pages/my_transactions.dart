import 'package:flutter/material.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/viewmodels/transactions_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';

import '../../utils/constants.dart';

class MyTransactions extends StatefulWidget {
  static const tag = 'my_transactions';
  @override
  _MyTransactionsState createState() => _MyTransactionsState();
}

class _MyTransactionsState extends State<MyTransactions> {
  bool hasConnection;
  @override
  Widget build(BuildContext context) {
    return BaseView<TransactionsModel>(
      onModelReady: (model) async {
        hasConnection = await checkConnection();
        if (!hasConnection) {
          showToast("Check Internet Connection");
          return;
        }
        Map<String, dynamic> data = {"page": "all"};
        await model.getTransactions(data);
      },
      builder: (context, model, child) => Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.white,
            resizeToAvoidBottomInset: true,
            body: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.25,
                      margin: EdgeInsets.only(bottom: 60),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: fromHex(blue),
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(40),
                          bottomRight: Radius.circular(40),
                        ),
                      ),
                    ),
                    Positioned(
                        bottom: 1,
                        right: 0,
                        left: 0,
                        child: CircleAvatar(
                          radius: 60,
                          backgroundColor: Colors.white,
                          child: CircleAvatar(
                            radius: 55,
                            backgroundColor: fromHex(gold_lite),
                            child: Icon(
                              Icons.receipt,
                              color: Colors.white,
                              size: 66,
                            ),
                          ),
                        ))
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'MY TRANSACTIONS',
                  style: TextStyle(
                      color: fromHex(blue),
                      fontSize: 20,
                      fontWeight: FontWeight.w900),
                ),
                SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: model.state == ViewState.Idle
                      ? _transactions(context, model)
                      : Center(
                          child: CircularProgressIndicator(
                            backgroundColor: fromHex(gold_dark),
                            valueColor: new AlwaysStoppedAnimation<Color>(
                              fromHex(grey),
                            ),
                          ),
                        ),
                ),
              ],
            ),
          ),
          Positioned(
            left: 20,
            top: 35,
            child: FloatingActionButton(
              foregroundColor: Colors.grey,
              backgroundColor: fromHex(blue),
              onPressed: () {
                Navigator.of(context).pop();
              },
              mini: true,
              tooltip: "go back",
              child: Icon(
                Icons.keyboard_backspace_sharp,
                color: fromHex(gold_dark),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _transactions(BuildContext context, TransactionsModel model) {
    return model.transactions != null
        ? model.transactions.isNotEmpty
            ? ListView.separated(
                itemCount: model.transactions.length,
                physics: BouncingScrollPhysics(),
                separatorBuilder: (context, index) => Divider(
                      indent: 15,
                      endIndent: 15,
                      color: fromHex(gold_lite),
                    ),
                itemBuilder: (context, index) => Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${model.transactions[index]?.createdAt?.substring(0, 10) ?? ''}",
                            style: TextStyle(
                                color: fromHex(gold_lite),
                                fontSize: 12,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.arrow_drop_down_circle_rounded,
                                color: fromHex(gold_lite),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: Text(
                                  "${model.transactions[index]?.transactionType ?? ''}"
                                      .capitalize(),
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: fromHex(blue),
                                  ),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                              Text(
                                "${cf.format(model.transactions[index]?.amount ?? 0)}",
                                style: TextStyle(
                                    color: fromHex(blue),
                                    fontSize: 15,
                                    fontWeight: FontWeight.w900),
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                "${model.transactions[index].status}",
                                style: TextStyle(
                                    color: fromHex(blue),
                                    fontWeight: FontWeight.w100,
                                    fontSize: 10,
                                    fontStyle: FontStyle.italic),
                              )
                            ],
                          )
                        ],
                      ),
                    ))
            : Container(
                height: 150,
                child: Center(
                  child: Text("No Transactions Done Yet"),
                ),
              )
        : Container(
            height: 150,
            child: Center(
              child: Text("An error occurred while loading the data"),
            ),
          );
  }
}
