import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:wakilishwa/core/models/local/local_models.dart';
import 'package:wakilishwa/core/viewmodels/user_details_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';

import '../../utils/constants.dart';

class UserProfileDetails extends StatefulWidget {
  final UserData user;
  final String s;
  UserProfileDetails(this.user, this.s);

  // static const tag = 'bidders_profile';
  @override
  _UserProfileDetailsState createState() => _UserProfileDetailsState();
}

class _UserProfileDetailsState extends State<UserProfileDetails> {
  @override
  Widget build(BuildContext context) {
    return BaseView<UserDetailsModel>(
      onModelReady: (model) async {
        bool hasConnection = await checkConnection();
        if (!hasConnection) {
          showToast("check internet connection");
        }
        await model.getUserDetails(widget.user.id);
      },
      builder: (context, model, child) => Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/bg.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: Scaffold(
              backgroundColor: Colors.transparent,
              body: Column(
                children: [
                  Expanded(
                    child: Container(
                      height: MediaQuery.of(context).size.height * .25,
                    ),
                  ),
                  Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width - 120,
                    decoration: BoxDecoration(
                      color: fromHex(gold_dark),
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20),
                        topLeft: Radius.circular(20),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        //BIDDER'S PROFILE
                        "${widget.s ?? 'PROFILE'}",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        top: 45, right: 20, left: 20, bottom: 25),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40)),
                      color: fromHex(blue),
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Adv. ${widget.user?.name?.first ?? ''} ${widget.user?.name?.others ?? ''}",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: fromHex(gold_dark),
                                    ),
                                  ),
                                  Text(
                                    'Advocate',
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.white,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  'Ratings  ',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 11,
                                  ),
                                ),
                                RatingBar.builder(
                                  initialRating:
                                      model.ratings ?? 0,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: true,
                                  itemCount: 5,
                                  itemSize: 12.7,
                                  ignoreGestures: true,
                                  unratedColor: fromHex(grey),
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 2.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: fromHex(gold_lite),
                                  ),
                                  onRatingUpdate: (rating) {
                                    print(rating);
                                  },
                                ),
                              ],
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Divider(
                            thickness: 1.5,
                            color: Colors.grey.shade800,
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'LSK REGISTRATION NUMBER',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w100,
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "${widget.user?.admissionNumber ?? ''}",
                              style: TextStyle(
                                  color: fromHex(gold_dark),
                                  fontSize: 18,
                                  fontWeight: FontWeight.w800),
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Divider(
                            thickness: 1.5,
                            color: Colors.grey.shade800,
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text(
                                  'About',
                                  style: TextStyle(
                                    color: fromHex(gold_dark),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 13,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "${model.user?.description ?? ''}",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 12),
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Divider(
                            thickness: 1.5,
                            color: Colors.grey.shade800,
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'TOWN',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w200,
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "${widget.user?.town?.name ?? ''}",
                              style: TextStyle(
                                  color: fromHex(gold_dark),
                                  fontSize: 18,
                                  fontWeight: FontWeight.w800),
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Divider(
                            thickness: 1.5,
                            color: Colors.grey.shade800,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'TOTAL JOBS POSTED',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.arrow_drop_down_circle,
                                      color: Colors.white,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      'VIEW JOBS POSTED',
                                      style: TextStyle(
                                          color: fromHex(gold_dark),
                                          fontSize: 10),
                                    )
                                  ],
                                )
                              ],
                            ),
                            Text(
                              "${model.reports?.jobs?.posted ?? "0"}",
                              style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w900),
                            )
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Divider(
                            thickness: 1.5,
                            color: Colors.grey.shade800,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'TOTAL JOBS PERFORMED',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.arrow_drop_down_circle,
                                      color: Colors.white,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      'VIEW JOBS PERFORMED',
                                      style: TextStyle(
                                          color: fromHex(gold_dark),
                                          fontSize: 10),
                                    )
                                  ],
                                )
                              ],
                            ),
                            Text(
                              "${model.reports?.jobs?.completed ?? "0"}",
                              style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w900),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            left: 20,
            top: 35,
            child: FloatingActionButton(
              foregroundColor: Colors.grey,
              backgroundColor: fromHex(blue),
              onPressed: () {
                Navigator.of(context).pop();
              },
              mini: true,
              tooltip: "go back",
              child: Icon(
                Icons.keyboard_backspace_sharp,
                color: fromHex(gold_dark),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
