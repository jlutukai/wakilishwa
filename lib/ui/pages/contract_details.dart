import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_my_contracts_response.dart';
import 'package:wakilishwa/core/viewmodels/contract_details_model.dart';
import 'package:wakilishwa/ui/pages/base_view.dart';
import 'package:wakilishwa/ui/widgets/active_contracts_widget.dart';
import 'package:wakilishwa/utils/constants.dart';

import 'bidder_profile.dart';
import 'chat_page.dart';

class ContractDetails extends StatefulWidget {
  final Contracts contract;
  final String s;
  ContractDetails(this.contract, this.s);

  @override
  _ContractDetailsState createState() => _ContractDetailsState();
}

class _ContractDetailsState extends State<ContractDetails> {
  bool hasBeen = false;
  bool hasBeen1 = false;
  bool hasConnection;
  String senderToken = "";
  String token = "";
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    hasBeen = widget.contract.status == 'finished' ||
        widget.contract.status == 'returned';
    hasBeen1 = widget.contract.status == 'active' ||
        widget.contract.status == 'returned';
    senderToken = getCurrentUser().pushToken;
    if (widget.contract.user.id == getCurrentUser().id) {
      token = widget.contract.job.user.pushToken;
    }
    if (widget.contract.job.user.id == getCurrentUser().id) {
      token = widget.contract.user.pushToken;
    }
    print("sender = ${getCurrentUser().pushToken}");
    print("other = $token");
    print("${widget.contract.status}  $hasBeen");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacementNamed(context, ActiveContractsWidget.tag);
        return false;
      },
      child: BaseView<ContractDetailsModel>(
        builder: (context, model, child) => Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/bg.png"),
                  fit: BoxFit.cover,
                ),
              ),
              child: Scaffold(
                key: _scaffoldKey,
                backgroundColor: fromHex(blue).withOpacity(0.7),
                body: SingleChildScrollView(
                  physics: NeverScrollableScrollPhysics(),
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Column(
                                children: [
                                  Text(
                                    "Adv. ${getCurrentUser()?.name?.first ?? ''} ${getCurrentUser()?.name?.others ?? ''}",
                                    style: TextStyle(
                                      color: fromHex(gold_dark),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "${Jiffy("${widget.contract?.createdAt ?? ''}", dateFormat).fromNow()}",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w100),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              top: 45, right: 20, left: 20, bottom: 25),
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(40),
                                topRight: Radius.circular(40)),
                            color: Colors.white,
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Contract Details',
                                    style: TextStyle(
                                        color: fromHex(blue),
                                        fontSize: 20,
                                        fontWeight: FontWeight.w900),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Container(
                                constraints: BoxConstraints(
                                    maxHeight:
                                        MediaQuery.of(context).size.height *
                                            0.55),
                                child: model.state == ViewState.Idle
                                    ? SingleChildScrollView(
                                        physics: BouncingScrollPhysics(),
                                        child: Column(
                                          children: [
                                            widget.contract.job.user.id !=
                                                    getCurrentUser().id
                                                ? InkWell(
                                                    onTap: () {
                                                      Navigator.of(context)
                                                          .push(
                                                        MaterialPageRoute(
                                                          builder: (BuildContext
                                                                  context) =>
                                                              UserProfileDetails(
                                                                  widget
                                                                      .contract
                                                                      .job
                                                                      .user,
                                                                  "JOB OWNER'S PROFILE"),
                                                        ),
                                                      );
                                                    },
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          "Job Owner's Profile",
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w800,
                                                            color: fromHex(
                                                                gold_dark),
                                                          ),
                                                        ),
                                                        Row(
                                                          children: [
                                                            Expanded(
                                                              child: Text(
                                                                "Check Out Job Owner's Profile",
                                                                textAlign:
                                                                    TextAlign
                                                                        .start,
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontSize: 12,
                                                                ),
                                                              ),
                                                            ),
                                                            Icon(
                                                              Icons
                                                                  .arrow_drop_down_circle,
                                                              color: fromHex(
                                                                  gold_dark),
                                                            )
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  )
                                                : InkWell(
                                                    onTap: () {
                                                      Navigator.of(context)
                                                          .push(
                                                        MaterialPageRoute(
                                                          builder: (BuildContext
                                                                  context) =>
                                                              UserProfileDetails(
                                                                  widget
                                                                      .contract
                                                                      .user,
                                                                  "JOB PERFORMER'S PROFILE"),
                                                        ),
                                                      );
                                                    },
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          "Job Performer's Profile",
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w800,
                                                            color: fromHex(
                                                                gold_dark),
                                                          ),
                                                        ),
                                                        Row(
                                                          children: [
                                                            Expanded(
                                                              child: Text(
                                                                "Check Out Job Performer's Profile",
                                                                textAlign:
                                                                    TextAlign
                                                                        .start,
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontSize: 12,
                                                                ),
                                                              ),
                                                            ),
                                                            Icon(
                                                              Icons
                                                                  .arrow_drop_down_circle,
                                                              color: fromHex(
                                                                  gold_dark),
                                                            )
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 5.0),
                                              child: Divider(
                                                thickness: .5,
                                                color: Colors.grey.shade800,
                                              ),
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Job Title",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    color: fromHex(gold_dark),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                        "${widget.contract?.job?.title ?? ''}",
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 5.0),
                                              child: Divider(
                                                thickness: .5,
                                                color: Colors.grey.shade800,
                                              ),
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Job Description",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    color: fromHex(gold_dark),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                        "${widget.contract?.job?.description ?? ''}",
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                            widget.contract.feedback != null
                                                ? widget.contract.feedback
                                                        .isNotEmpty
                                                    ? Column(
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    vertical:
                                                                        5.0),
                                                            child: Divider(
                                                              thickness: .5,
                                                              color: Colors.grey
                                                                  .shade800,
                                                            ),
                                                          ),
                                                          Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Text(
                                                                "Performer's Feedback",
                                                                style:
                                                                    TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w800,
                                                                  color: fromHex(
                                                                      gold_dark),
                                                                ),
                                                              ),
                                                              Row(
                                                                children: [
                                                                  Expanded(
                                                                    child: Text(
                                                                      "${widget.contract?.feedback ?? ''}",
                                                                      textAlign:
                                                                          TextAlign
                                                                              .start,
                                                                      style:
                                                                          TextStyle(
                                                                        color: Colors
                                                                            .black,
                                                                        fontSize:
                                                                            12,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              )
                                                            ],
                                                          ),
                                                        ],
                                                      )
                                                    : Container()
                                                : Container(),
                                            widget.contract.returnFeedback !=
                                                    null
                                                ? widget.contract.returnFeedback
                                                        .isNotEmpty
                                                    ? Column(
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    vertical:
                                                                        5.0),
                                                            child: Divider(
                                                              thickness: .5,
                                                              color: Colors.grey
                                                                  .shade800,
                                                            ),
                                                          ),
                                                          Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Text(
                                                                "Job Owner's Feedback",
                                                                style:
                                                                    TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w800,
                                                                  color: fromHex(
                                                                      gold_dark),
                                                                ),
                                                              ),
                                                              Row(
                                                                children: [
                                                                  Expanded(
                                                                    child: Text(
                                                                      "${widget.contract?.returnFeedback ?? ''}",
                                                                      textAlign:
                                                                          TextAlign
                                                                              .start,
                                                                      style:
                                                                          TextStyle(
                                                                        color: Colors
                                                                            .black,
                                                                        fontSize:
                                                                            12,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              )
                                                            ],
                                                          ),
                                                        ],
                                                      )
                                                    : Container()
                                                : Container(),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 5.0),
                                              child: Divider(
                                                thickness: .5,
                                                color: Colors.grey.shade800,
                                              ),
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Date",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    color: fromHex(gold_dark),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                        "${widget.contract?.job?.jobDate ?? ''}",
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 5.0),
                                              child: Divider(
                                                thickness: .5,
                                                color: Colors.grey.shade800,
                                              ),
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Time",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    color: fromHex(gold_dark),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                        "${widget.contract?.job?.jobTime ?? ''}",
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 5.0),
                                              child: Divider(
                                                thickness: .5,
                                                color: Colors.grey.shade800,
                                              ),
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Town",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    color: fromHex(gold_dark),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                        "${widget.contract?.job?.town?.name ?? ''}",
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 5.0),
                                              child: Divider(
                                                thickness: .5,
                                                color: Colors.grey.shade800,
                                              ),
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Court Room",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    color: fromHex(gold_dark),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                        "${widget.contract?.job?.court?.name ?? ''}",
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),
                                                    Text(
                                                      "${widget.contract?.job?.courtRoom ?? ''}",
                                                      style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 12,
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 5.0),
                                              child: Divider(
                                                thickness: .5,
                                                color: Colors.grey.shade800,
                                              ),
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Magistrate",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    color: fromHex(gold_dark),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                        "${widget.contract?.job?.magistrateOrJudge ?? ''}",
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 5.0),
                                              child: Divider(
                                                thickness: .5,
                                                color: Colors.grey.shade800,
                                              ),
                                            ),
                                            widget.contract.status !=
                                                    'completed'
                                                ? InkWell(
                                                    onTap: () {
                                                      Navigator.of(context)
                                                          .push(
                                                        MaterialPageRoute(
                                                          builder: (BuildContext
                                                                  context) =>
                                                              ChatPage(
                                                            id: "${widget.contract.id}",
                                                            type: "contracts",
                                                            senderToken:
                                                                "${getCurrentUser().pushToken ?? ''}",
                                                            otherToken:
                                                                "$token",
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          "Chat",
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w800,
                                                            color: fromHex(
                                                                gold_dark),
                                                          ),
                                                        ),
                                                        Row(
                                                          children: [
                                                            Expanded(
                                                              child: Text(
                                                                "Get to communicate with the Job ${getCurrentUser().id == widget.contract.job.user.id ? "performer" : "job owner"}",
                                                                textAlign:
                                                                    TextAlign
                                                                        .start,
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontSize: 12,
                                                                ),
                                                              ),
                                                            ),
                                                            Icon(
                                                              Icons
                                                                  .arrow_drop_down_circle,
                                                              color: fromHex(
                                                                  gold_dark),
                                                            )
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  )
                                                : Container(),
                                            widget.contract.status != 'complete'
                                                ? Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 5.0),
                                                    child: Divider(
                                                      thickness: .5,
                                                      color:
                                                          Colors.grey.shade800,
                                                    ),
                                                  )
                                                : Container(),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Brief Type",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    color: fromHex(gold_dark),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                        "${widget.contract?.job?.briefType?.caseType ?? ''}"
                                                            .capitalize(),
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 5.0),
                                              child: Divider(
                                                thickness: .5,
                                                color: Colors.grey.shade800,
                                              ),
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Status",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    color: fromHex(gold_dark),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                        "${widget.contract?.status ?? ''}"
                                                            .capitalize(),
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 5.0),
                                              child: Divider(
                                                thickness: .5,
                                                color: Colors.grey.shade800,
                                              ),
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Amount",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w800,
                                                    color: fromHex(gold_dark),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Text(
                                                        "${cf.format((widget.contract?.job?.additionalFees ?? 0) + (widget.contract?.job?.briefType?.amount ?? 0))}",
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 30,
                                            ),
                                            model.state == ViewState.Idle
                                                ? widget.contract.status ==
                                                            "finished" ||
                                                        widget.contract
                                                                .status ==
                                                            'returned'
                                                    ? widget.contract.job.user
                                                                .id ==
                                                            getCurrentUser().id
                                                        ? hasBeen
                                                            ? _buttons(
                                                                context, model)
                                                            : Container()
                                                        : Container()
                                                    : Container()
                                                : Container(),
                                            model.state == ViewState.Idle
                                                ? widget.contract.status ==
                                                            "active" ||
                                                        widget.contract
                                                                .status ==
                                                            "returned"
                                                    ? widget.contract.job.user
                                                                .id !=
                                                            getCurrentUser().id
                                                        ? hasBeen1
                                                            ? _finishBtn(
                                                                context, model)
                                                            : Container()
                                                        : Container()
                                                    : Container()
                                                : Container(),
                                          ],
                                        ),
                                      )
                                    : Center(
                                        child: CircularProgressIndicator(
                                          backgroundColor: fromHex(gold_dark),
                                          valueColor:
                                              new AlwaysStoppedAnimation<Color>(
                                            fromHex(grey),
                                          ),
                                        ),
                                      ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            model.state == ViewState.Idle
                ? Positioned(
                    left: 20,
                    top: 35,
                    child: FloatingActionButton(
                      foregroundColor: Colors.grey,
                      backgroundColor: fromHex(blue),
                      heroTag: "go back to contracts",
                      onPressed: () {
                        Navigator.pushReplacementNamed(
                            context, ActiveContractsWidget.tag);
                      },
                      mini: true,
                      tooltip: "go back",
                      child: Icon(
                        Icons.keyboard_backspace_sharp,
                        color: fromHex(gold_dark),
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  Future<void> _setAsRejected(BuildContext context, Contracts contract,
      String s, ContractDetailsModel model) async {
    TextEditingController _description = TextEditingController();
    final _formKey1 = GlobalKey<FormState>();
    await showDialog(
      context: _scaffoldKey.currentContext,
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.white,
        child: StatefulBuilder(
          builder: (context, StateSetter setState) => Container(
            color: Colors.transparent,
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Form(
              key: _formKey1,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "$s",
                      style: TextStyle(
                          color: fromHex(gold_lite),
                          fontWeight: FontWeight.w800,
                          fontSize: 18),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      child: TextFormField(
                        controller: _description,
                        keyboardType: TextInputType.text,
                        textAlignVertical: TextAlignVertical.top,
                        textCapitalization: TextCapitalization.sentences,
                        maxLines: 4,
                        style: TextStyle(color: fromHex(blue)),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Please provide reason for return";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          alignLabelWithHint: true,
                          filled: true,
                          fillColor: Colors.white,
                          isDense: true,
                          hintText: "Enter reason for return here...",
                          labelText: "Reason for Returning ",
                          labelStyle: TextStyle(color: fromHex(blue)),
                          hintStyle: TextStyle(
                            color: Colors.blueGrey[400],
                          ),
                          border: getBorder(),
                          focusedBorder: getBorder(),
                          enabledBorder: getBorder(),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        if (_formKey1.currentState.validate()) {
                          Map<String, dynamic> data = {
                            "status": "returned",
                            "return_feedback": "${_description.text}"
                          };
                          _markAsRejected(context, contract, data, model);
                          Navigator.of(context).pop();
                        }
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 34),
                        margin: EdgeInsets.symmetric(horizontal: 25),
                        decoration: BoxDecoration(
                          color: fromHex(blue),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'Submit',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _setAsApproved(
    BuildContext context,
    Contracts contract,
    String s,
    ContractDetailsModel model,
  ) async {
    TextEditingController _description = TextEditingController();
    double rate = 0.0;
    await showDialog(
      context: _scaffoldKey.currentContext,
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.white,
        child: StatefulBuilder(
          builder: (context, StateSetter setState) => Container(
            color: Colors.transparent,
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Container(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "$s",
                      style: TextStyle(
                          color: fromHex(gold_lite),
                          fontWeight: FontWeight.w800,
                          fontSize: 18),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: RatingBar.builder(
                            initialRating: 0,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemSize: 30.7,
                            unratedColor: fromHex(grey),
                            itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: fromHex(gold_lite),
                            ),
                            onRatingUpdate: (rating) {
                              setState(() {
                                rate = rating;
                              });
                            },
                          ),
                        ),
                        Text("$rate")
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      child: TextFormField(
                        controller: _description,
                        keyboardType: TextInputType.text,
                        textAlignVertical: TextAlignVertical.top,
                        textCapitalization: TextCapitalization.sentences,
                        maxLines: 4,
                        style: TextStyle(color: fromHex(blue)),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Please Enter Review";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          alignLabelWithHint: true,
                          filled: true,
                          fillColor: Colors.white,
                          isDense: true,
                          hintText: "Enter Review here...",
                          labelText: "Review Comment",
                          labelStyle: TextStyle(color: fromHex(blue)),
                          hintStyle: TextStyle(
                            color: Colors.blueGrey[400],
                          ),
                          border: getBorder(),
                          focusedBorder: getBorder(),
                          enabledBorder: getBorder(),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        Map<String, dynamic> data = {
                          "status": "completed",
                          "rating": rate,
                          "comment": "${_description.text}"
                        };
                        _submitAsDone(context, contract, model, data);
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 34),
                        margin: EdgeInsets.symmetric(horizontal: 25),
                        decoration: BoxDecoration(
                          color: fromHex(blue),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'Submit',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _markAsRejected(BuildContext context, Contracts contract,
      Map<String, dynamic> data, ContractDetailsModel model) async {
    try {
      var r = await model.markAsRejected(data, contract.id);
      showToast(r.status);
      setState(() {
        hasBeen = true;
      });
    } catch (e) {
      try {
        Map<String, dynamic> error = jsonDecode(e.toString());
        showToast(error['errors'][0]['message']);
      } catch (a) {}
    }
  }

  Future<void> _submitAsDone(BuildContext context, Contracts contract,
      ContractDetailsModel model, Map<String, dynamic> data) async {
    try {
      var r = await model.markAsComplete(data, contract.id);
      showToast(r.status);
      setState(() {
        hasBeen = true;
      });
    } catch (e) {
      try {
        Map<String, dynamic> error = jsonDecode(e.toString());
        showToast(error['errors'][0]['message']);
      } catch (a) {}
    }
  }

  _buttons(BuildContext context, ContractDetailsModel model) {
    return Wrap(
      children: [
        Container(
          width: 200,
          padding: EdgeInsets.symmetric(vertical: 10),
          child: InkWell(
            onTap: () {
              _setAsRejected(context, widget.contract, "FeedBack", model);
            },
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 15, horizontal: 34),
              margin: EdgeInsets.symmetric(horizontal: 25),
              decoration: BoxDecoration(
                color: Colors.deepOrange.shade900,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
              ),
              child: Center(
                child: Text(
                  'RETURN',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.w900),
                ),
              ),
            ),
          ),
        ),
        Container(
          width: 200,
          padding: EdgeInsets.symmetric(vertical: 10),
          child: InkWell(
            onTap: () {
              _setAsApproved(context, widget.contract, "Rate Performer", model);
            },
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 15, horizontal: 34),
              margin: EdgeInsets.symmetric(horizontal: 25),
              decoration: BoxDecoration(
                color: Colors.green.shade900,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(20),
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
              ),
              child: Center(
                child: Text(
                  'APPROVE',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.w900),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  _finishBtn(BuildContext context, ContractDetailsModel model) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: InkWell(
        onTap: () async {
          hasConnection = await checkConnection();
          if (!hasConnection) {
            showToast("Check Internet Connection");
            return;
          }
          DateTime date = DateFormat("MM/dd/yyyy")
              .parse(widget.contract?.job?.jobDate ?? '');
          print("job_date ${date.isAfter(DateTime.now())}  ${DateTime.now()}");
          if (date.isBefore(DateTime.now())) {
            _getOwnersRatings(context, widget.contract, "Feed Back", model);
          } else {
            showToast("Job Date has not reached");
          }
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 34),
          margin: EdgeInsets.symmetric(horizontal: 25),
          decoration: BoxDecoration(
            color: Colors.green.shade900,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
          ),
          child: Center(
            child: Text(
              'SUBMIT AS DONE',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.w900),
            ),
          ),
        ),
      ),
    );
  }

  void _getOwnersRatings(BuildContext context, Contracts contract, String s,
      ContractDetailsModel model) async {
    TextEditingController _description = TextEditingController();
    final _formKey1 = GlobalKey<FormState>();
    await showDialog(
      context: _scaffoldKey.currentContext,
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.white,
        child: StatefulBuilder(
          builder: (context, StateSetter setState) => Container(
            color: Colors.transparent,
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Form(
              key: _formKey1,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "$s",
                      style: TextStyle(
                          color: fromHex(gold_lite),
                          fontWeight: FontWeight.w800,
                          fontSize: 18),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      child: TextFormField(
                        controller: _description,
                        keyboardType: TextInputType.text,
                        textAlignVertical: TextAlignVertical.top,
                        textCapitalization: TextCapitalization.sentences,
                        maxLines: 4,
                        style: TextStyle(color: fromHex(blue)),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Please provide Feedback on the Job";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          alignLabelWithHint: true,
                          filled: true,
                          fillColor: Colors.white,
                          isDense: true,
                          hintText: "Enter FeedBack here...",
                          labelText: "FeedBack",
                          labelStyle: TextStyle(color: fromHex(blue)),
                          hintStyle: TextStyle(
                            color: Colors.blueGrey[400],
                          ),
                          border: getBorder(),
                          focusedBorder: getBorder(),
                          enabledBorder: getBorder(),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    InkWell(
                      onTap: () {
                        if (_formKey1.currentState.validate()) {
                          Map<String, dynamic> data = {
                            "feedback": "${_description.text}"
                          };
                          _markAsFinished(context, contract, model, data);
                          Navigator.of(context).pop();
                        }
                      },
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 34),
                        margin: EdgeInsets.symmetric(horizontal: 25),
                        decoration: BoxDecoration(
                          color: fromHex(blue),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'Submit',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w900),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _markAsFinished(BuildContext context, Contracts contract,
      ContractDetailsModel model, Map<String, dynamic> data) async {
    try {
      var r = await model.markAsFinished(data, contract.id);
      showToast(r.status);
      setState(() {
        hasBeen1 = false;
      });
    } catch (e) {
      try {
        Map<String, dynamic> error = jsonDecode(e.toString());
        showToast(error['errors'][0]['message']);
      } catch (a) {}
    }
  }
}
