import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:wakilishwa/utils/constants.dart';

import 'login.dart';

class IntoPage extends StatefulWidget {
  static const tag = 'intro';
  @override
  _IntoPageState createState() => _IntoPageState();
}

class _IntoPageState extends State<IntoPage> {
  int _current = 0;
  List<SliderStuff> sliderList = [];

  @override
  void initState() {
    sliderList.add(SliderStuff("Don’t Stress,\n Wakilishwa...",
        "Find the right advocate to hold your brief, in any town and Court across the country"));
    sliderList.add(SliderStuff("Don’t Stress,\n Wakilishwa...",
        "Find the right advocate to hold your brief, in any town and Court across the country"));
    sliderList.add(SliderStuff("Don’t Stress,\n Wakilishwa...",
        "Find the right advocate to hold your brief, in any town and Court across the country"));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/bg.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Scaffold(
        backgroundColor: fromHex(blue).withOpacity(0.7), //#002D5E
        body: Column(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Stack(
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: const EdgeInsets.only(
                              bottom: 40, left: 50, right: 50),
                          child: Image.asset(
                            "assets/logo_bg.png",
                            width: 200,
                            height: 200,
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 25,
                        left: 50,
                        right: 50,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              children: [
                                SizedBox(
                                  height: 5,
                                ),
                                Text("WAKILISHWA",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w800,
                                        fontSize: 16)),
                                Text("...find a lawyer",
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.pacifico(
                                        textStyle: TextStyle(
                                            color: fromHex(gold_dark)))),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    CarouselSlider.builder(
                      itemCount: sliderList.length,
                      itemBuilder:
                          (BuildContext context, int index, int realIndex) {
                        return Container(
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Column(
                                    children: [
                                      Text(
                                        "${sliderList[index].title}",
                                        style: TextStyle(
                                            color: fromHex(gold_lite),
                                            fontWeight: FontWeight.bold,
                                            fontSize: 30),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 48, right: 48, top: 20),
                                child: Text(
                                  "${sliderList[index].description}",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                  ),
                                  maxLines: 8,
                                  textAlign: TextAlign.center,
                                ),
                              )
                            ],
                          ),
                        );
                      },
                      options: CarouselOptions(
                          autoPlay: false,
                          enlargeCenterPage: true,
                          viewportFraction: 1.2,
                          aspectRatio: 1.5,
                          initialPage: 0,
                          onPageChanged: (index, reason) {
                            setState(() {
                              _current = index;
                            });
                          }),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: sliderList.map((url) {
                        int index = sliderList.indexOf(url);
                        return Container(
                          width: 12.0,
                          height: 12.0,
                          margin: EdgeInsets.symmetric(
                              vertical: 5.0, horizontal: 10.0),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: _current == index
                                ? fromHex(gold_lite)
                                : fromHex(gold_dark).withOpacity(0.3),
                          ),
                        );
                      }).toList(),
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ],
                ),
              ),
            ),
            Column(
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pushNamedAndRemoveUntil(context, LoginPage.tag,
                        (Route<dynamic> route) => false);
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 34),
                    margin: EdgeInsets.only(bottom: 40),
                    decoration: BoxDecoration(
                      color: fromHex(gold_lite),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      ),
                    ),
                    child: Text(
                      'ENTER',
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class SliderStuff {
  String title;
  String description;

  SliderStuff(this.title, this.description);
}
