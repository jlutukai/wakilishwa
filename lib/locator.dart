import 'package:get_it/get_it.dart';
import 'package:wakilishwa/core/viewmodels/account_type_model.dart';
import 'package:wakilishwa/core/viewmodels/contract_details_model.dart';
import 'package:wakilishwa/core/viewmodels/deposit_model.dart';
import 'package:wakilishwa/core/viewmodels/edit_profile_model.dart';
import 'package:wakilishwa/core/viewmodels/jobs_feed_model.dart';
import 'package:wakilishwa/core/viewmodels/my_archived_jobs_model.dart';
import 'package:wakilishwa/core/viewmodels/my_contracts_model.dart';
import 'package:wakilishwa/core/viewmodels/otp_model.dart';
import 'package:wakilishwa/core/viewmodels/reports_model.dart';
import 'package:wakilishwa/core/viewmodels/request_password_reset_model.dart';
import 'package:wakilishwa/core/viewmodels/user_details_model.dart';
import 'package:wakilishwa/core/viewmodels/wallet_model.dart';
import 'package:wakilishwa/core/viewmodels/withdraw_model.dart';

import 'core/services/NavigationService.dart';
import 'core/services/PushNotificationsService.dart';
import 'core/services/api.dart';
import 'core/viewmodels/alerts_model.dart';
import 'core/viewmodels/auth_info_model.dart';
import 'core/viewmodels/awarded_contracts_model.dart';
import 'core/viewmodels/chat_model.dart';
import 'core/viewmodels/id_verify_model.dart';
import 'core/viewmodels/jobs_bids_model.dart';
import 'core/viewmodels/jobs_i_performed_model.dart';
import 'core/viewmodels/login_model.dart';
import 'core/viewmodels/my_bids_model.dart';
import 'core/viewmodels/my_jobs_model.dart';
import 'core/viewmodels/personal_info_model.dart';
import 'core/viewmodels/post_new_job_model.dart';
import 'core/viewmodels/transactions_model.dart';
import 'core/viewmodels/user_profile_model.dart';

GetIt locator = GetIt.instance;

void setUpLocator() {
  locator.registerLazySingleton(() => Api());
  locator.registerLazySingleton(() => PushNotificationService());
  locator.registerLazySingleton(() => NavigationService());

  locator.registerFactory(() => LoginModel());
  locator.registerFactory(() => UserProfileModel());
  locator.registerFactory(() => MyJobsModel());
  locator.registerFactory(() => JobsFeedModel());
  locator.registerFactory(() => MyJobBidsModel());
  locator.registerFactory(() => PostNewJobModel());
  locator.registerFactory(() => JobsBidModel());
  locator.registerFactory(() => MyContractsModel());
  locator.registerFactory(() => AwardedContractsModel());
  locator.registerFactory(() => AlertsModel());
  locator.registerFactory(() => EditProfileModel());
  locator.registerFactory(() => WalletModel());
  locator.registerFactory(() => DepositModel());
  locator.registerFactory(() => WithDrawModel());
  locator.registerFactory(() => TransactionsModel());
  locator.registerFactory(() => AccountTypeModel());
  locator.registerFactory(() => IDVerificationModel());
  locator.registerFactory(() => PersonalInfoModel());
  locator.registerFactory(() => AuthInfoModel());
  locator.registerFactory(() => RequestPasswordResetModel());
  locator.registerFactory(() => UserDetailsModel());
  locator.registerFactory(() => MyArchivedJobsModel());
  locator.registerFactory(() => JobsIPerformedModel());
  locator.registerFactory(() => ReportsModel());
  locator.registerFactory(() => ContractDetailsModel());
  locator.registerFactory(() => OTPModel());
  locator.registerFactory(() => ChatModel());
}
