import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:hive/hive.dart';
import 'package:wakilishwa/core/models/account_types.dart';
import 'package:wakilishwa/core/models/award_contract_response.dart';
import 'package:wakilishwa/core/models/brief_types_response.dart';
import 'package:wakilishwa/core/models/countries_response.dart';
import 'package:wakilishwa/core/models/courts_response.dart';
import 'package:wakilishwa/core/models/create_account_response.dart';
import 'package:wakilishwa/core/models/create_job_response.dart';
import 'package:wakilishwa/core/models/current_user.dart';
import 'package:wakilishwa/core/models/deposit_response.dart';
import 'package:wakilishwa/core/models/get_alerts_response.dart';
import 'package:wakilishwa/core/models/get_app_charges.dart';
import 'package:wakilishwa/core/models/get_job_bids_response.dart';
import 'package:wakilishwa/core/models/get_jobs_response.dart';
import 'package:wakilishwa/core/models/get_my_bids_response.dart';
import 'package:wakilishwa/core/models/get_my_contracts_response.dart';
import 'package:wakilishwa/core/models/get_transactions_response.dart';
import 'package:wakilishwa/core/models/get_user_details_response.dart';
import 'package:wakilishwa/core/models/get_wallet_response.dart';
import 'package:wakilishwa/core/models/id_verification_response.dart';
import 'package:wakilishwa/core/models/login_response.dart';
import 'package:wakilishwa/core/models/regions_response.dart';
import 'package:wakilishwa/core/models/request_password_reset_response.dart';
import 'package:wakilishwa/core/models/send_fcm_response.dart';
import 'package:wakilishwa/core/models/towns_response.dart';
import 'package:wakilishwa/core/models/update_user_response.dart';
import 'package:wakilishwa/core/models/verify_advocate_response.dart';
import 'package:wakilishwa/utils/constants.dart';
import 'package:wakilishwa/utils/logging_interceptor.dart';

class Api {
  static const baseUrl = 'https://wakilishwa.com:9443/api';
  Dio _dio;
  String firebaseId = "";
  Box<String> accountInfo;

  Api() {
    BaseOptions options =
        BaseOptions(receiveTimeout: 50000, connectTimeout: 50000);
    _dio = Dio(options)
      ..interceptors.add(LoggingInterceptor())
      ..interceptors
          .add(DioCacheManager(CacheConfig(baseUrl: baseUrl)).interceptor)
      ..options.headers["content-type"] = "application/json";
  }

  Future<LoginResponse> loginUser(Map<String, String> data) async {
    String endPoint = "$baseUrl/users/signin";
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });

    LoginResponse r = LoginResponse.fromJson(response.data);
    if (response.statusCode == 200) {
      setToken("Bearer ${r.token}");
    }
    return r;
  }

  Future<CreateAccountResponse> createUser(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/users/signup";
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    print(response);
    return CreateAccountResponse.fromJson(response.data);
  }

  Future<GetCurrentUserResponse> getCurrentUser() async {
    String endPoint = "$baseUrl/users/currentuser";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.get(endPoint).catchError((e) {
      throw e.response.toString();
    });
    return GetCurrentUserResponse.fromJson(response.data);
  }

  Future<GetUserDetailsResponse> getUserDetails(String id) async {
    String endPoint = "$baseUrl/users/$id";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.get(endPoint).catchError((e) {
      throw e.response.toString();
    });
    return GetUserDetailsResponse.fromJson(response.data);
  }

  Future<UpdateUserResponse> updateUserDetails(
      Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/users/update-profile";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.patch(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    print(response);
    return UpdateUserResponse.fromJson(response.data);
  }

  Future<GetCountriesResponse> getCountries(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/countries";
    // _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetCountriesResponse.fromJson(response.data);
  }

  Future<GetTownsResponse> getTowns(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/towns";
    // _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetTownsResponse.fromJson(response.data);
  }

  Future<GetRegionsResponse> getRegions(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/regions";
    // _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetRegionsResponse.fromJson(response.data);
  }

  Future<GetCourtsResponse> getCourts(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/courts";
    // _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetCourtsResponse.fromJson(response.data);
  }

  Future<GetBriefTypesResponse> getBriefTypes(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/brief-types";
    // _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetBriefTypesResponse.fromJson(response.data);
  }

  Future<GetAccountTypesResponse> getAccountTypes(
      Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/account-types";
    // _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetAccountTypesResponse.fromJson(response.data);
  }

  Future<CreateJobResponse> createNewJob(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/jobs";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    return CreateJobResponse.fromJson(response.data);
  }

  Future<GeneralResponse> editJob(Map<String, dynamic> data, String id) async {
    String endPoint = "$baseUrl/jobs/$id";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.patch(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    return GeneralResponse.fromJson(response.data);
  }

  Future<GetJobsResponse> getMyJobs(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/user/jobs";
    _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetJobsResponse.fromJson(response.data);
  }

  Future<GetJobsResponse> getActiveAndPublishedJobs(
      Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/user/jobs-draft-and-published";
    _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetJobsResponse.fromJson(response.data);
  }

  Future<GetJobsResponse> getJobsFeed(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/jobs";
    _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetJobsResponse.fromJson(response.data);
  }

  Future<GetMyBidsResponse> getMyBids(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/user/bids";
    _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetMyBidsResponse.fromJson(response.data);
  }

  Future<GeneralResponse> cancelBid(
      Map<String, dynamic> data, String id) async {
    String endPoint = "$baseUrl/bids/cancel/$id";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.patch(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    print(response);
    return GeneralResponse.fromJson(response.data);
  }

  Future<GetJobBidsResponse> getJobBids(
      Map<String, dynamic> data, String id) async {
    String endPoint = "$baseUrl/jobs/$id/bids";
    _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetJobBidsResponse.fromJson(response.data);
  }

  Future<GeneralResponse> bidOnAJob(String id) async {
    String endPoint = "$baseUrl/jobs/$id/bids";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.post(endPoint).catchError((e) {
      throw e.response.toString();
    });
    return GeneralResponse.fromJson(response.data);
  }

  Future<GetAlertsResponse> getAlerts(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/notifications";
    _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetAlertsResponse.fromJson(response.data);
  }

  Future<GetAlertsResponse> readAlerts(String id) async {
    String endPoint = "$baseUrl/notifications/$id";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.patch(endPoint).catchError((e) {
      throw e.response.toString();
    });
    return GetAlertsResponse.fromJson(response.data);
  }

  Future<AwardContractResponse> awardContract(String id) async {
    String endPoint = "$baseUrl/contracts/award/$id";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.post(endPoint).catchError((e) {
      throw e.response.toString();
    });
    print(response);
    return AwardContractResponse.fromJson(response.data);
  }

  Future<GetContractsResponse> getMyContracts(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/my/contracts";
    _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetContractsResponse.fromJson(response.data);
  }

  Future<GetContractsResponse> getMyContractsFiltered(
      Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/my/contracts-by-active-finished-and-returned";
    _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetContractsResponse.fromJson(response.data);
  }

  Future<GeneralResponse> markAsComplete(
      Map<String, dynamic> data, String id) async {
    String endPoint = "$baseUrl/contracts/$id";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.patch(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    print(response);
    return GeneralResponse.fromJson(response.data);
  }

  Future<GeneralResponse> markAsFinished(
      Map<String, dynamic> data, String id) async {
    String endPoint = "$baseUrl/contracts/finish/$id";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.patch(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    print(response);
    return GeneralResponse.fromJson(response.data);
  }

  Future<GetContractsResponse> getAwardedContracts(
      Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/contracts/by-awarder";
    _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetContractsResponse.fromJson(response.data);
  }

  Future<GetContractsResponse> getAwardedContractsFiltered(
      Map<String, dynamic> data) async {
    String endPoint =
        "$baseUrl/contracts/by-awarder-by-active-finished-and-returned";
    _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetContractsResponse.fromJson(response.data);
  }

  Future<GetWalletResponse> getWalletDetails() async {
    String endPoint = "$baseUrl/wallet/current-user-wallet";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.get(endPoint).catchError((e) {
      throw e.response.toString();
    });
    return GetWalletResponse.fromJson(response.data);
  }

  Future<GetTransactionsResponse> getTransactions(
      [Map<String, dynamic> data]) async {
    String endPoint = "$baseUrl/transactions";
    _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetTransactionsResponse.fromJson(response.data);
  }

  Future<GetTransactionsResponse> getTransactionsReports(
      [Map<String, dynamic> data]) async {
    String endPoint = "$baseUrl/transactions/search";
    _dio.options.headers["Authorization"] = getToken();
    final response =
        await _dio.get(endPoint, queryParameters: data).catchError((e) {
      throw e.response.toString();
    });
    return GetTransactionsResponse.fromJson(response.data);
  }

  Future<GetAppCharges> getAppCharges() async {
    String endPoint = "$baseUrl/app/service-setup";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.get(endPoint).catchError((e) {
      throw e.response.toString();
    });
    return GetAppCharges.fromJson(response.data);
  }

  Future<DepositResponse> depositCash(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/users/wallet/deposit";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    return DepositResponse.fromJson(response.data);
  }

  Future<DepositResponse> withDrawCash(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/user/withdrawal";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    return DepositResponse.fromJson(response.data);
  }

  Future<VerifyAdvocateResponse> verifyAdmissionNo(
      Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/lsk-search-advocate";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    return VerifyAdvocateResponse.fromJson(response.data);
  }

  Future<IDVerificationResponse> verifyID(Map<String, dynamic> data) async {
    String endPoint = "$baseUrl/verify/national-id";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    return IDVerificationResponse.fromJson(response.data);
  }

  Future<GeneralResponse> requestPasswordRest(Map<String, String> data) async {
    String endPoint = "$baseUrl/users/request-password-rest";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    return GeneralResponse.fromJson(response.data);
  }

  Future<GeneralResponse> resetPassword(Map<String, String> data) async {
    String endPoint = "$baseUrl/users/rest-password";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    return GeneralResponse.fromJson(response.data);
  }

  Future<GeneralResponse> resendOtpCode(Map<String, String> data) async {
    String endPoint = "$baseUrl/users/resend-activation-code";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    return GeneralResponse.fromJson(response.data);
  }

  Future<GeneralResponse> activateAccount(Map<String, String> data) async {
    String endPoint = "$baseUrl/users/activate";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    return GeneralResponse.fromJson(response.data);
  }

  Future<SendFCMResponse> sendFCMMessage(Map<String, dynamic> data) async {
    String endPoint = "https://fcm.googleapis.com/fcm/send";
    _dio.options.headers["Authorization"] = "key=$serverKey";
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    return SendFCMResponse.fromJson(response.data);
  }

  Future<GeneralResponse> updateToken(Map<String, String> data) async {
    String endPoint = "$baseUrl/users/create-push-token";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    return GeneralResponse.fromJson(response.data);
  }

  Future<GeneralResponse> deleteToken() async {
    String endPoint = "$baseUrl/users/delete-push-token";
    _dio.options.headers["Authorization"] = getToken();
    final response = await _dio.delete(endPoint).catchError((e) {
      throw e.response.toString();
    });
    return GeneralResponse.fromJson(response.data);
  }
}

//https://www.getpostman.com/collections/a7361f244c6b78737d9e
