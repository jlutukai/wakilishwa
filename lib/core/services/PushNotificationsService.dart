import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:wakilishwa/ui/pages/chat_page.dart';
import 'package:wakilishwa/utils/constants.dart';

import '../../locator.dart';
import 'NavigationService.dart';

class PushNotificationService {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  final NavigationService navigator = locator<NavigationService>();

  void initialise() {
    // accountInfo = Hive.box<String>("accountInfo");
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        if (message['data']['senderId'] != getCurrentUser().id) {
          showToast(message['notification']['title']);
        }
        print("onMessage: $message");
      },
      onLaunch: (Map<String, dynamic> message) async {
        navigateToPage(message);
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        navigateToPage(message);
        print("onResume: $message");
      },
    );
  }

  Future<String> getFCMToken() async {
    return await _fcm.getToken();
  }

  void navigateToPage(Map<String, dynamic> message) {
    var data = message['data'];
    var r = data['screen'];
    var chatID = message['data']['chatId'];
    var type = message['data']['type'];
    var senderToken = message['data']['senderToken'];
    var token = message['data']['token'];
    var senderId = message['data']['senderId'];

    if (r != null) {
      navigator.navigationKey.currentState.push(
        MaterialPageRoute(
          builder: (BuildContext context) => ChatPage(
            id: "$chatID",
            type: "$type",
            senderToken: token,
            otherToken: senderToken,
          ),
        ),
      );
    }
  }
}

class ChatPageArguments {
  String id;
  String type;

  ChatPageArguments({this.id, this.type});
}
