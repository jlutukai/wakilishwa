import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_user_details_response.dart';
import 'package:wakilishwa/core/services/api.dart';

import '../../locator.dart';
import 'base_model.dart';

class UserDetailsModel extends BaseModel {
  Api _api = locator<Api>();

  User user;
  Reports reports;
  double ratings = 0.0;

  Future getUserDetails(String id) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getUserDetails(id);
      user = r.user;
      reports = r.reports;
      if (reports != null) {
        if (reports.ratings != null) {
          if (reports.ratings.rates != null) {
            if (reports.ratings.rates.isNotEmpty) {
              double sumRatings = 0.0;
              reports.ratings.rates.forEach((element) {
                sumRatings += element.rating ?? 0.0;
                ratings = sumRatings / reports.ratings.rates.length;
              });
            }
          }
        }
      }
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
