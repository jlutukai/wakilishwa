import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/local/local_models.dart';
import 'package:wakilishwa/core/models/regions_response.dart';
import 'package:wakilishwa/core/models/towns_response.dart';
import 'package:wakilishwa/core/models/update_user_response.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';
import 'package:wakilishwa/locator.dart';

class EditProfileModel extends BaseModel {
  Api _api = locator<Api>();

  List<AccountTypeData> accountTypes;
  List<CountryData> countries;
  List<RegionsData> regions;
  List<TownsData> towns;

  Future<UpdateUserResponse> updateUserDetails(
      Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateUserDetails(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getAccountTypes(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getAccountTypes(data);
      accountTypes = r.accountTypes;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getCountries(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getCountries(data);
      countries = r.countries;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getRegions(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getRegions(data);
      regions = r.regions;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getTowns(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getTowns(data);
      towns = r.towns;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
