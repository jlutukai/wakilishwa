import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/send_fcm_response.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';

import '../../locator.dart';

class ChatModel extends BaseModel {
  Api _api = locator<Api>();

  Future<SendFCMResponse> sendFCMMessage(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.sendFCMMessage(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
