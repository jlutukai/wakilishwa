import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/deposit_response.dart';
import 'package:wakilishwa/core/models/get_transactions_response.dart';
import 'package:wakilishwa/core/services/api.dart';

import '../../locator.dart';
import 'base_model.dart';

class WithDrawModel extends BaseModel {
  Api _api = locator<Api>();

  List<Transactions> transactions;

  Future<DepositResponse> withDrawCash(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.withDrawCash(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getTransactions(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getTransactions(data);
      transactions = r.transactions;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
