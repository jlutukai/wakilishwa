import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_transactions_response.dart';
import 'package:wakilishwa/core/models/get_wallet_response.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';

import '../../locator.dart';

class WalletModel extends BaseModel {
  Api _api = locator<Api>();

  Wallet wallet;
  List<Transactions> transactions;

  Future getWalletDetails() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getWalletDetails();
      wallet = r.wallet;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getTransactions(data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getTransactions(data);
      transactions = r.transactions;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
