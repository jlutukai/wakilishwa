import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/brief_types_response.dart';
import 'package:wakilishwa/core/models/courts_response.dart';
import 'package:wakilishwa/core/models/create_job_response.dart';
import 'package:wakilishwa/core/models/get_app_charges.dart';
import 'package:wakilishwa/core/models/regions_response.dart';
import 'package:wakilishwa/core/models/request_password_reset_response.dart';
import 'package:wakilishwa/core/models/towns_response.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';

import '../../locator.dart';

class PostNewJobModel extends BaseModel {
  Api _api = locator<Api>();

  List<RegionsData> regions;
  List<TownsData> towns;
  List<CourtsData> courts;
  List<ChargesData> briefTypes;

  ServiceSetup serviceSetup;

  Future<CreateJobResponse> createNewJob(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.createNewJob(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<GeneralResponse> editJob(Map<String, dynamic> data, String id) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.editJob(data, id);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getAppCharges() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getAppCharges();
      serviceSetup = r.serviceSetup;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getRegions(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getRegions(data);
      regions = r.regions;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getTowns(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getTowns(data);
      towns = r.towns;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getCourts(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getCourts(data);
      courts = r.courts;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getBriefType(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getBriefTypes(data);
      briefTypes = r.charges;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
