import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_jobs_response.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';

import '../../locator.dart';

class MyArchivedJobsModel extends BaseModel {
  Api _api = locator<Api>();

  List<JobsData> myJobs;

  Future getMyJobs(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getMyJobs(data);
      myJobs = r.jobs;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
