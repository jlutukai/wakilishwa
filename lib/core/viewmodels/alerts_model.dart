import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_alerts_response.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';
import 'package:wakilishwa/utils/constants.dart';

import '../../locator.dart';

class AlertsModel extends BaseModel {
  Api _api = locator<Api>();

  List<Notifications> notifications;

  Future getAlerts(Map<String, dynamic> data) async {
    int c = 0;
    setState(ViewState.Busy);
    try {
      var r = await _api.getAlerts(data);
      notifications = r.notifications;
      r.notifications.forEach((element) {
        if (element.isActive) {
          c += 1;
        }
      });
      setNotificationCount(c.toString());
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<GetAlertsResponse> readAlerts(String id) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.readAlerts(id);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
