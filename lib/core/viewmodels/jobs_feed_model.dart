import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_jobs_response.dart';
import 'package:wakilishwa/core/models/request_password_reset_response.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';

import '../../locator.dart';

class JobsFeedModel extends BaseModel {
  Api _api = locator<Api>();

  List<JobsData> jobsFeed;

  Future getJobsFeeds(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getJobsFeed(data);
      jobsFeed = r.jobs;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future refreshData(Map<String, dynamic> data) async {
    try {
      var r = await _api.getJobsFeed(data);
      jobsFeed = r.jobs;
      notifyListeners();
    } catch (e) {
      throw e;
    }
  }

  Future<GeneralResponse> editJob(Map<String, dynamic> data, String id) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.editJob(data, id);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<GeneralResponse> bidOnJob(String id) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.bidOnAJob(id);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
