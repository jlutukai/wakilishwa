import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_my_contracts_response.dart';
import 'package:wakilishwa/core/models/request_password_reset_response.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';

import '../../locator.dart';

class JobsIPerformedModel extends BaseModel {
  Api _api = locator<Api>();

  List<Contracts> contracts;

  Future getMyContracts(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getMyContracts(data);
      contracts = r.contracts;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<GeneralResponse> markAsFinished(
      Map<String, dynamic> data, String id) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.markAsFinished(data, id);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
