import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/login_response.dart';
import 'package:wakilishwa/core/models/request_password_reset_response.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';

import '../../locator.dart';

class LoginModel extends BaseModel {
  Api _api = locator<Api>();

  Future<LoginResponse> login(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.loginUser(data);
      // setToken("Bearer ${r.token}");
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<GeneralResponse> updateToken(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.updateToken(data);
      // setToken("Bearer ${r.token}");
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
