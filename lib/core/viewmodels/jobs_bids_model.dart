import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/award_contract_response.dart';
import 'package:wakilishwa/core/models/get_job_bids_response.dart';
import 'package:wakilishwa/core/services/api.dart';

import '../../locator.dart';
import 'base_model.dart';

class JobsBidModel extends BaseModel {
  Api _api = locator<Api>();

  List<JobBids> jobBids;

  Future getJobBids(Map<String, dynamic> data, String id) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getJobBids(data, id);
      jobBids = r.jobBids;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<AwardContractResponse> awardContract(String id) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.awardContract(id);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
