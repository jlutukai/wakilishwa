import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/request_password_reset_response.dart';
import 'package:wakilishwa/core/services/api.dart';

import '../../locator.dart';
import 'base_model.dart';

class ContractDetailsModel extends BaseModel {
  Api _api = locator<Api>();

  Future<GeneralResponse> markAsComplete(
      Map<String, dynamic> data, String id) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.markAsComplete(data, id);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<GeneralResponse> markAsRejected(
      Map<String, dynamic> data, String id) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.markAsComplete(data, id);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<GeneralResponse> markAsFinished(
      Map<String, dynamic> data, String id) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.markAsFinished(data, id);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
