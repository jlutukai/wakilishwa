import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/request_password_reset_response.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';

import '../../locator.dart';

class OTPModel extends BaseModel {
  Api _api = locator<Api>();

  Future<GeneralResponse> resendOTP(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.resendOtpCode(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<GeneralResponse> verifyOTP(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.activateAccount(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
