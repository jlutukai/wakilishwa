import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/current_user.dart';
import 'package:wakilishwa/core/models/local/local_models.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';

import '../../locator.dart';

class UserProfileModel extends BaseModel {
  Api _api = locator<Api>();

  UserData user;
  Reports reports;
  double ratings = 0.0;

  Future getCurrentUser() async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getCurrentUser();
      user = r.user;
      reports = r.reports;
      if (reports != null) {
        if (reports.ratings != null) {
          if (reports.ratings.rates != null) {
            if (reports.ratings.rates.isNotEmpty) {
              double sumRatings = 0.0;
              reports.ratings.rates.forEach((element) {
                sumRatings += element.rating ?? 0.0;
                ratings = sumRatings / reports.ratings.rates.length;
              });
            }
          }
        }
      }
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
