import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_jobs_response.dart';
import 'package:wakilishwa/core/models/request_password_reset_response.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';

import '../../locator.dart';

class MyJobsModel extends BaseModel {
  Api _api = locator<Api>();

  List<JobsData> myJobs;

  Future getMyBidJobs(Map<String, dynamic> data) async {
    List<JobsData> _jobs;
    setState(ViewState.Busy);
    try {
      var r = await _api.getMyJobs(data);
      if (r.jobs != null) {
        _jobs = List();
        r.jobs.forEach((element) {
          if (element.numberOfBids != 0) {
            _jobs.add(element);
          }
        });
      }
      myJobs = _jobs;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getArchivedJobs(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getMyJobs(data);

      myJobs = r.jobs;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getDraftAndPublishedJobs(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getActiveAndPublishedJobs(data);
      myJobs = r.jobs;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future refreshData(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getActiveAndPublishedJobs(data);
      myJobs = r.jobs;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<GeneralResponse> editJob(Map<String, dynamic> data, String id) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.editJob(data, id);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
