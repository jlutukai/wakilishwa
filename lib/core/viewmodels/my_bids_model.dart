import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_my_bids_response.dart';
import 'package:wakilishwa/core/models/request_password_reset_response.dart';
import 'package:wakilishwa/core/services/api.dart';

import '../../locator.dart';
import 'base_model.dart';

class MyJobBidsModel extends BaseModel {
  Api _api = locator<Api>();

  List<BidsData> jobBids;

  Future getMyBids(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getMyBids(data);
      jobBids = r.bids;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<GeneralResponse> cancelBid(
      Map<String, dynamic> data, String id) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.cancelBid(data, id);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
