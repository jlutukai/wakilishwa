import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/id_verification_response.dart';
import 'package:wakilishwa/core/services/api.dart';

import '../../locator.dart';
import 'base_model.dart';

class IDVerificationModel extends BaseModel {
  Api _api = locator<Api>();

  Results results;
  bool isCorrect;

  Future<IDVerificationResponse> verifyId(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.verifyID(data);
      results = r.results;
      isCorrect = r.isCorrect;
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
