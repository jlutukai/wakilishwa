import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/get_transactions_response.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';

import '../../locator.dart';

class ReportsModel extends BaseModel {
  Api _api = locator<Api>();

  List<Transactions> transactions;

  Future getReports(data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getTransactions(data);
      transactions = r.transactions;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
