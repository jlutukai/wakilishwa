import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/create_account_response.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';

import '../../locator.dart';

class AuthInfoModel extends BaseModel {
  Api _api = locator<Api>();

  Future<CreateAccountResponse> createUser(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.createUser(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
