import 'package:wakilishwa/core/enums/view_state.dart';
import 'package:wakilishwa/core/models/local/local_models.dart';
import 'package:wakilishwa/core/models/verify_advocate_response.dart';
import 'package:wakilishwa/core/services/api.dart';
import 'package:wakilishwa/core/viewmodels/base_model.dart';

import '../../locator.dart';

class AccountTypeModel extends BaseModel {
  Api _api = locator<Api>();

  List<AccountTypeData> accountTypes;

  Advocate advocate;

  Future getAccountTypes(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.getAccountTypes(data);
      accountTypes = r.accountTypes;
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<VerifyAdvocateResponse> verifyAdvocate(
      Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api.verifyAdmissionNo(data);
      advocate = r.advocate;
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
