class GetBriefTypesResponse {
  String status;
  Page page;
  int count;
  List<ChargesData> charges;

  GetBriefTypesResponse({this.status, this.page, this.count, this.charges});

  GetBriefTypesResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    page = json['page'] != null ? new Page.fromJson(json['page']) : null;
    count = json['count'];
    if (json['charges'] != null) {
      charges = new List<ChargesData>();
      json['charges'].forEach((v) {
        charges.add(new ChargesData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.page != null) {
      data['page'] = this.page.toJson();
    }
    data['count'] = this.count;
    if (this.charges != null) {
      data['charges'] = this.charges.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Page {
  int prev;
  int next;
  int current;
  int limit;

  Page({this.prev, this.next, this.current, this.limit});

  Page.fromJson(Map<String, dynamic> json) {
    prev = json['prev'];
    next = json['next'];
    current = json['current'];
    limit = json['limit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['prev'] = this.prev;
    data['next'] = this.next;
    data['current'] = this.current;
    data['limit'] = this.limit;
    return data;
  }
}

class ChargesData {
  String createdAt;
  bool isActive;
  String caseType;
  String description;
  double amount;
  String id;

  ChargesData(
      {this.createdAt,
      this.isActive,
      this.caseType,
      this.description,
      this.amount,
      this.id});

  ChargesData.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    caseType = json['case_type'];
    description = json['description'];
    amount = json['amount']?.toDouble();
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['case_type'] = this.caseType;
    data['description'] = this.description;
    data['amount'] = this.amount;
    data['id'] = this.id;
    return data;
  }
}
