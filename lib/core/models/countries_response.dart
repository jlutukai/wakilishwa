import 'package:wakilishwa/core/models/local/local_models.dart';

class GetCountriesResponse {
  String status;
  Page page;
  int count;
  List<CountryData> countries;

  GetCountriesResponse({this.status, this.page, this.count, this.countries});

  GetCountriesResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    page = json['page'] != null ? new Page.fromJson(json['page']) : null;
    count = json['count'];
    if (json['countries'] != null) {
      countries = new List<CountryData>();
      json['countries'].forEach((v) {
        countries.add(new CountryData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.page != null) {
      data['page'] = this.page.toJson();
    }
    data['count'] = this.count;
    if (this.countries != null) {
      data['countries'] = this.countries.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Page {
  int prev;
  int next;
  int current;
  int limit;

  Page({this.prev, this.next, this.current, this.limit});

  Page.fromJson(Map<String, dynamic> json) {
    prev = json['prev'];
    next = json['next'];
    current = json['current'];
    limit = json['limit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['prev'] = this.prev;
    data['next'] = this.next;
    data['current'] = this.current;
    data['limit'] = this.limit;
    return data;
  }
}

class Countries {
  String createdAt;
  bool isActive;
  String name;
  String dialCode;
  String currencyName;
  String currencyCode;
  String code;
  String id;

  Countries(
      {this.createdAt,
      this.isActive,
      this.name,
      this.dialCode,
      this.currencyName,
      this.currencyCode,
      this.code,
      this.id});

  Countries.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    dialCode = json['dial_code'];
    currencyName = json['currency_name'];
    currencyCode = json['currency_code'];
    code = json['code'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['dial_code'] = this.dialCode;
    data['currency_name'] = this.currencyName;
    data['currency_code'] = this.currencyCode;
    data['code'] = this.code;
    data['id'] = this.id;
    return data;
  }
}
