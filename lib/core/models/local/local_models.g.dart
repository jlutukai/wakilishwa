// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'local_models.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserDataAdapter extends TypeAdapter<UserData> {
  @override
  final int typeId = 1;

  @override
  UserData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UserData(
      name: fields[0] as Name,
      freeCredits: fields[1] as FreeCredits,
      isSuperAdmin: fields[2] as bool,
      userType: fields[3] as String,
      createdAt: fields[4] as String,
      isActive: fields[5] as bool,
      isApproved: fields[6] as bool,
      email: fields[7] as String,
      phone: fields[8] as String,
      accountType: fields[9] as AccountTypeData,
      idNumber: fields[10] as String,
      passportNumber: fields[11] as String,
      admissionNumber: fields[12] as String,
      country: fields[13] as CountryData,
      region: fields[14] as RegionData,
      town: fields[15] as TownData,
      photo: fields[16] as String,
      id: fields[17] as String,
      description: fields[18] as String,
      unreadNotification: fields[19] as int,
      pushToken: fields[20] as String,
    );
  }

  @override
  void write(BinaryWriter writer, UserData obj) {
    writer
      ..writeByte(21)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.freeCredits)
      ..writeByte(2)
      ..write(obj.isSuperAdmin)
      ..writeByte(3)
      ..write(obj.userType)
      ..writeByte(4)
      ..write(obj.createdAt)
      ..writeByte(5)
      ..write(obj.isActive)
      ..writeByte(6)
      ..write(obj.isApproved)
      ..writeByte(7)
      ..write(obj.email)
      ..writeByte(8)
      ..write(obj.phone)
      ..writeByte(9)
      ..write(obj.accountType)
      ..writeByte(10)
      ..write(obj.idNumber)
      ..writeByte(11)
      ..write(obj.passportNumber)
      ..writeByte(12)
      ..write(obj.admissionNumber)
      ..writeByte(13)
      ..write(obj.country)
      ..writeByte(14)
      ..write(obj.region)
      ..writeByte(15)
      ..write(obj.town)
      ..writeByte(16)
      ..write(obj.photo)
      ..writeByte(17)
      ..write(obj.id)
      ..writeByte(18)
      ..write(obj.description)
      ..writeByte(19)
      ..write(obj.unreadNotification)
      ..writeByte(20)
      ..write(obj.pushToken);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class NameAdapter extends TypeAdapter<Name> {
  @override
  final int typeId = 2;

  @override
  Name read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Name(
      first: fields[0] as String,
      others: fields[1] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Name obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.first)
      ..writeByte(1)
      ..write(obj.others);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is NameAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class FreeCreditsAdapter extends TypeAdapter<FreeCredits> {
  @override
  final int typeId = 3;

  @override
  FreeCredits read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return FreeCredits(
      jobAssigned: fields[0] as int,
      jobCount: fields[1] as int,
      amount: fields[3] as double,
      amountPerJob: fields[4] as double,
    );
  }

  @override
  void write(BinaryWriter writer, FreeCredits obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.jobAssigned)
      ..writeByte(1)
      ..write(obj.jobCount)
      ..writeByte(3)
      ..write(obj.amount)
      ..writeByte(4)
      ..write(obj.amountPerJob);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FreeCreditsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class AccountTypeDataAdapter extends TypeAdapter<AccountTypeData> {
  @override
  final int typeId = 4;

  @override
  AccountTypeData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AccountTypeData(
      createdAt: fields[0] as String,
      isActive: fields[1] as bool,
      name: fields[2] as String,
      typeName: fields[3] as String,
      id: fields[4] as String,
    );
  }

  @override
  void write(BinaryWriter writer, AccountTypeData obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.createdAt)
      ..writeByte(1)
      ..write(obj.isActive)
      ..writeByte(2)
      ..write(obj.name)
      ..writeByte(3)
      ..write(obj.typeName)
      ..writeByte(4)
      ..write(obj.id);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AccountTypeDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class CountryDataAdapter extends TypeAdapter<CountryData> {
  @override
  final int typeId = 5;

  @override
  CountryData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CountryData(
      createdAt: fields[0] as String,
      isActive: fields[1] as bool,
      name: fields[2] as String,
      dialCode: fields[3] as String,
      currencyName: fields[4] as String,
      currencyCode: fields[5] as String,
      code: fields[6] as String,
      id: fields[7] as String,
    );
  }

  @override
  void write(BinaryWriter writer, CountryData obj) {
    writer
      ..writeByte(8)
      ..writeByte(0)
      ..write(obj.createdAt)
      ..writeByte(1)
      ..write(obj.isActive)
      ..writeByte(2)
      ..write(obj.name)
      ..writeByte(3)
      ..write(obj.dialCode)
      ..writeByte(4)
      ..write(obj.currencyName)
      ..writeByte(5)
      ..write(obj.currencyCode)
      ..writeByte(6)
      ..write(obj.code)
      ..writeByte(7)
      ..write(obj.id);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CountryDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class RegionDataAdapter extends TypeAdapter<RegionData> {
  @override
  final int typeId = 6;

  @override
  RegionData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return RegionData(
      createdAt: fields[0] as String,
      isActive: fields[1] as bool,
      country: fields[2] as String,
      name: fields[3] as String,
      id: fields[4] as String,
    );
  }

  @override
  void write(BinaryWriter writer, RegionData obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.createdAt)
      ..writeByte(1)
      ..write(obj.isActive)
      ..writeByte(2)
      ..write(obj.country)
      ..writeByte(3)
      ..write(obj.name)
      ..writeByte(4)
      ..write(obj.id);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RegionDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class TownDataAdapter extends TypeAdapter<TownData> {
  @override
  final int typeId = 7;

  @override
  TownData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TownData(
      createdAt: fields[0] as String,
      isActive: fields[1] as bool,
      name: fields[2] as String,
      region: fields[3] as String,
      id: fields[4] as String,
    );
  }

  @override
  void write(BinaryWriter writer, TownData obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.createdAt)
      ..writeByte(1)
      ..write(obj.isActive)
      ..writeByte(2)
      ..write(obj.name)
      ..writeByte(3)
      ..write(obj.region)
      ..writeByte(4)
      ..write(obj.id);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TownDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class CreateAccountBodyAdapter extends TypeAdapter<CreateAccountBody> {
  @override
  final int typeId = 8;

  @override
  CreateAccountBody read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CreateAccountBody(
      email: fields[0] as String,
      phone: fields[1] as String,
      accountType: fields[2] as String,
      name: fields[3] as Name,
      idNumber: fields[4] as String,
      passportNumber: fields[5] as String,
      admissionNumber: fields[6] as String,
      country: fields[7] as String,
      region: fields[8] as String,
      town: fields[9] as String,
      password: fields[10] as String,
      passwordConfirm: fields[11] as String,
      hasActivatedAccount: fields[13] as bool,
      hasFinishedRegistration: fields[12] as bool,
    );
  }

  @override
  void write(BinaryWriter writer, CreateAccountBody obj) {
    writer
      ..writeByte(14)
      ..writeByte(0)
      ..write(obj.email)
      ..writeByte(1)
      ..write(obj.phone)
      ..writeByte(2)
      ..write(obj.accountType)
      ..writeByte(3)
      ..write(obj.name)
      ..writeByte(4)
      ..write(obj.idNumber)
      ..writeByte(5)
      ..write(obj.passportNumber)
      ..writeByte(6)
      ..write(obj.admissionNumber)
      ..writeByte(7)
      ..write(obj.country)
      ..writeByte(8)
      ..write(obj.region)
      ..writeByte(9)
      ..write(obj.town)
      ..writeByte(10)
      ..write(obj.password)
      ..writeByte(11)
      ..write(obj.passwordConfirm)
      ..writeByte(12)
      ..write(obj.hasFinishedRegistration)
      ..writeByte(13)
      ..write(obj.hasActivatedAccount);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CreateAccountBodyAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
