import 'package:hive/hive.dart';

part 'local_models.g.dart';

@HiveType(typeId: 1)
class UserData {
  @HiveField(0)
  Name name;
  @HiveField(1)
  FreeCredits freeCredits;
  @HiveField(2)
  bool isSuperAdmin;
  @HiveField(3)
  String userType;
  @HiveField(4)
  String createdAt;
  @HiveField(5)
  bool isActive;
  @HiveField(6)
  bool isApproved;
  @HiveField(7)
  String email;
  @HiveField(8)
  String phone;
  @HiveField(9)
  AccountTypeData accountType;
  @HiveField(10)
  String idNumber;
  @HiveField(11)
  String passportNumber;
  @HiveField(12)
  String admissionNumber;
  @HiveField(13)
  CountryData country;
  @HiveField(14)
  RegionData region;
  @HiveField(15)
  TownData town;
  @HiveField(16)
  String photo;
  @HiveField(17)
  String id;
  @HiveField(18)
  String description;
  @HiveField(19)
  int unreadNotification;
  @HiveField(20)
  String pushToken;

  UserData(
      {this.name,
      this.freeCredits,
      this.isSuperAdmin,
      this.userType,
      this.createdAt,
      this.isActive,
      this.isApproved,
      this.email,
      this.phone,
      this.accountType,
      this.idNumber,
      this.passportNumber,
      this.admissionNumber,
      this.country,
      this.region,
      this.town,
      this.photo,
      this.id,
      this.description,
      this.unreadNotification,
      this.pushToken});

  UserData.fromJson(Map<String, dynamic> json) {
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
    freeCredits = json['free_credits'] != null
        ? new FreeCredits.fromJson(json['free_credits'])
        : null;
    isSuperAdmin = json['is_super_admin'];
    userType = json['user_type'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isApproved = json['is_approved'];
    email = json['email'];
    phone = json['phone'];
    accountType = json['account_type'] != null
        ? new AccountTypeData.fromJson(json['account_type'])
        : null;
    idNumber = json['id_number'];
    passportNumber = json['passport_number'];
    admissionNumber = json['admission_number'];
    country = json['country'] != null
        ? new CountryData.fromJson(json['country'])
        : null;
    region =
        json['region'] != null ? new RegionData.fromJson(json['region']) : null;
    town = json['town'] != null ? new TownData.fromJson(json['town']) : null;
    // photo = json['photo'];
    id = json['id'];
    description = json['description'];
    unreadNotification = json['unreadNotification'];
    pushToken = json['push_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.freeCredits != null) {
      data['free_credits'] = this.freeCredits.toJson();
    }
    data['is_super_admin'] = this.isSuperAdmin;
    data['user_type'] = this.userType;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_approved'] = this.isApproved;
    data['email'] = this.email;
    data['phone'] = this.phone;
    if (this.accountType != null) {
      data['account_type'] = this.accountType.toJson();
    }
    data['id_number'] = this.idNumber;
    data['passport_number'] = this.passportNumber;
    data['admission_number'] = this.admissionNumber;
    if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    if (this.region != null) {
      data['region'] = this.region.toJson();
    }
    if (this.town != null) {
      data['town'] = this.town.toJson();
    }
    data['photo'] = this.photo;
    data['id'] = this.id;
    data['description'] = this.description;
    data['unreadNotification'] = this.unreadNotification;
    data['push_token'] = this.pushToken;
    return data;
  }
}

@HiveType(typeId: 2)
class Name {
  @HiveField(0)
  String first;
  @HiveField(1)
  String others;

  Name({this.first, this.others});

  Name.fromJson(Map<String, dynamic> json) {
    first = json['first'];
    others = json['others'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first'] = this.first;
    data['others'] = this.others;
    return data;
  }
}

@HiveType(typeId: 3)
class FreeCredits {
  @HiveField(0)
  int jobAssigned;
  @HiveField(1)
  int jobCount;
  @HiveField(3)
  double amount;
  @HiveField(4)
  double amountPerJob;

  FreeCredits(
      {this.jobAssigned, this.jobCount, this.amount, this.amountPerJob});

  FreeCredits.fromJson(Map<String, dynamic> json) {
    jobAssigned = json['job_assigned'];
    jobCount = json['job_count'];
    amount = json['amount']?.toDouble();
    amountPerJob = json['amount_per_job']?.toDouble();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['job_assigned'] = this.jobAssigned;
    data['job_count'] = this.jobCount;
    data['amount'] = this.amount;
    data['amount_per_job'] = this.amountPerJob;
    return data;
  }
}

@HiveType(typeId: 4)
class AccountTypeData {
  @HiveField(0)
  String createdAt;
  @HiveField(1)
  bool isActive;
  @HiveField(2)
  String name;
  @HiveField(3)
  String typeName;
  @HiveField(4)
  String id;

  AccountTypeData(
      {this.createdAt, this.isActive, this.name, this.typeName, this.id});

  AccountTypeData.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    typeName = json['type_name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['type_name'] = this.typeName;
    data['id'] = this.id;
    return data;
  }
}

@HiveType(typeId: 5)
class CountryData {
  @HiveField(0)
  String createdAt;
  @HiveField(1)
  bool isActive;
  @HiveField(2)
  String name;
  @HiveField(3)
  String dialCode;
  @HiveField(4)
  String currencyName;
  @HiveField(5)
  String currencyCode;
  @HiveField(6)
  String code;
  @HiveField(7)
  String id;

  CountryData(
      {this.createdAt,
      this.isActive,
      this.name,
      this.dialCode,
      this.currencyName,
      this.currencyCode,
      this.code,
      this.id});

  CountryData.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    dialCode = json['dial_code'];
    currencyName = json['currency_name'];
    currencyCode = json['currency_code'];
    code = json['code'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['dial_code'] = this.dialCode;
    data['currency_name'] = this.currencyName;
    data['currency_code'] = this.currencyCode;
    data['code'] = this.code;
    data['id'] = this.id;
    return data;
  }
}

@HiveType(typeId: 6)
class RegionData {
  @HiveField(0)
  String createdAt;
  @HiveField(1)
  bool isActive;
  @HiveField(2)
  String country;
  @HiveField(3)
  String name;
  @HiveField(4)
  String id;

  RegionData({this.createdAt, this.isActive, this.country, this.name, this.id});

  RegionData.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    country = json['country'];
    name = json['name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['country'] = this.country;
    data['name'] = this.name;
    data['id'] = this.id;
    return data;
  }
}

@HiveType(typeId: 7)
class TownData {
  @HiveField(0)
  String createdAt;
  @HiveField(1)
  bool isActive;
  @HiveField(2)
  String name;
  @HiveField(3)
  String region;
  @HiveField(4)
  String id;

  TownData({this.createdAt, this.isActive, this.name, this.region, this.id});

  TownData.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    region = json['region'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['region'] = this.region;
    data['id'] = this.id;
    return data;
  }
}

@HiveType(typeId: 8)
class CreateAccountBody {
  @HiveField(0)
  String email;
  @HiveField(1)
  String phone;
  @HiveField(2)
  String accountType;
  @HiveField(3)
  Name name;
  @HiveField(4)
  String idNumber;
  @HiveField(5)
  String passportNumber;
  @HiveField(6)
  String admissionNumber;
  @HiveField(7)
  String country;
  @HiveField(8)
  String region;
  @HiveField(9)
  String town;
  @HiveField(10)
  String password;
  @HiveField(11)
  String passwordConfirm;
  @HiveField(12)
  bool hasFinishedRegistration = false;
  @HiveField(13)
  bool hasActivatedAccount = false;

  CreateAccountBody(
      {this.email,
      this.phone,
      this.accountType,
      this.name,
      this.idNumber,
      this.passportNumber,
      this.admissionNumber,
      this.country,
      this.region,
      this.town,
      this.password,
      this.passwordConfirm,
      this.hasActivatedAccount,
      this.hasFinishedRegistration});

  CreateAccountBody.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    phone = json['phone'];
    accountType = json['account_type'];
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
    idNumber = json['id_number'];
    passportNumber = json['passport_number'];
    admissionNumber = json['admission_number'];
    country = json['country'];
    region = json['region'];
    town = json['town'];
    password = json['password'];
    passwordConfirm = json['password_confirm'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['account_type'] = this.accountType;
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    data['id_number'] = this.idNumber;
    data['passport_number'] = this.passportNumber;
    data['admission_number'] = this.admissionNumber;
    data['country'] = this.country;
    data['region'] = this.region;
    data['town'] = this.town;
    data['password'] = this.password;
    data['password_confirm'] = this.passwordConfirm;
    return data;
  }
}
