import 'package:wakilishwa/core/models/get_jobs_response.dart';
import 'package:wakilishwa/core/models/local/local_models.dart';

class GetMyBidsResponse {
  String status;
  Page page;
  int count;
  List<BidsData> bids;

  GetMyBidsResponse({this.status, this.page, this.count, this.bids});

  GetMyBidsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    page = json['page'] != null ? new Page.fromJson(json['page']) : null;
    count = json['count'];
    if (json['bids'] != null) {
      bids = new List<BidsData>();
      json['bids'].forEach((v) {
        bids.add(new BidsData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.page != null) {
      data['page'] = this.page.toJson();
    }
    data['count'] = this.count;
    if (this.bids != null) {
      data['bids'] = this.bids.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Page {
  int prev;
  int next;
  int current;
  int limit;

  Page({this.prev, this.next, this.current, this.limit});

  Page.fromJson(Map<String, dynamic> json) {
    prev = json['prev'];
    next = json['next'];
    current = json['current'];
    limit = json['limit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['prev'] = this.prev;
    data['next'] = this.next;
    data['current'] = this.current;
    data['limit'] = this.limit;
    return data;
  }
}

class BidsData {
  String status;
  String createdAt;
  bool isActive;
  UserData user;
  JobsData job;
  String id;

  BidsData(
      {this.status,
      this.createdAt,
      this.isActive,
      this.user,
      this.job,
      this.id});

  BidsData.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    user = json['user'] != null ? new UserData.fromJson(json['user']) : null;
    job = json['job'] != null ? new JobsData.fromJson(json['job']) : null;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.job != null) {
      data['job'] = this.job.toJson();
    }
    data['id'] = this.id;
    return data;
  }
}

class UserBid {
  Name name;
  String email;
  String phone;
  AccountType accountType;
  Country country;
  Region region;
  Town town;
  String id;

  UserBid(
      {this.name,
      this.email,
      this.phone,
      this.accountType,
      this.country,
      this.region,
      this.town,
      this.id});

  UserBid.fromJson(Map<String, dynamic> json) {
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
    email = json['email'];
    phone = json['phone'];
    accountType = json['account_type'] != null
        ? new AccountType.fromJson(json['account_type'])
        : null;
    country =
        json['country'] != null ? new Country.fromJson(json['country']) : null;
    region =
        json['region'] != null ? new Region.fromJson(json['region']) : null;
    town = json['town'] != null ? new Town.fromJson(json['town']) : null;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    data['email'] = this.email;
    data['phone'] = this.phone;
    if (this.accountType != null) {
      data['account_type'] = this.accountType.toJson();
    }
    if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    if (this.region != null) {
      data['region'] = this.region.toJson();
    }
    if (this.town != null) {
      data['town'] = this.town.toJson();
    }
    data['id'] = this.id;
    return data;
  }
}

class Name {
  String first;
  String others;

  Name({this.first, this.others});

  Name.fromJson(Map<String, dynamic> json) {
    first = json['first'];
    others = json['others'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first'] = this.first;
    data['others'] = this.others;
    return data;
  }
}

class AccountType {
  String createdAt;
  bool isActive;
  String name;
  String typeName;
  String id;

  AccountType(
      {this.createdAt, this.isActive, this.name, this.typeName, this.id});

  AccountType.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    typeName = json['type_name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['type_name'] = this.typeName;
    data['id'] = this.id;
    return data;
  }
}

class Country {
  String createdAt;
  bool isActive;
  String name;
  String dialCode;
  String currencyName;
  String currencyCode;
  String code;
  String id;

  Country(
      {this.createdAt,
      this.isActive,
      this.name,
      this.dialCode,
      this.currencyName,
      this.currencyCode,
      this.code,
      this.id});

  Country.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    dialCode = json['dial_code'];
    currencyName = json['currency_name'];
    currencyCode = json['currency_code'];
    code = json['code'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['dial_code'] = this.dialCode;
    data['currency_name'] = this.currencyName;
    data['currency_code'] = this.currencyCode;
    data['code'] = this.code;
    data['id'] = this.id;
    return data;
  }
}

class Region {
  String createdAt;
  bool isActive;
  String country;
  String name;
  String id;

  Region({this.createdAt, this.isActive, this.country, this.name, this.id});

  Region.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    country = json['country'];
    name = json['name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['country'] = this.country;
    data['name'] = this.name;
    data['id'] = this.id;
    return data;
  }
}

class Town {
  String createdAt;
  bool isActive;
  String name;
  String region;
  String id;

  Town({this.createdAt, this.isActive, this.name, this.region, this.id});

  Town.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    region = json['region'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['region'] = this.region;
    data['id'] = this.id;
    return data;
  }
}

class Job {
  UserData user;
  String title;
  Court court;
  String jobTime;
  String jobDate;
  BriefType briefType;
  String description;
  String id;

  Job(
      {this.user,
      this.title,
      this.court,
      this.jobTime,
      this.jobDate,
      this.briefType,
      this.description,
      this.id});

  Job.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new UserData.fromJson(json['user']) : null;
    title = json['title'];
    court = json['court'] != null ? new Court.fromJson(json['court']) : null;
    jobTime = json['job_time'];
    jobDate = json['job_date'];
    briefType = json['brief_type'] != null
        ? new BriefType.fromJson(json['brief_type'])
        : null;
    description = json['description'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['title'] = this.title;
    if (this.court != null) {
      data['court'] = this.court.toJson();
    }
    data['job_time'] = this.jobTime;
    data['job_date'] = this.jobDate;
    if (this.briefType != null) {
      data['brief_type'] = this.briefType.toJson();
    }
    data['description'] = this.description;
    data['id'] = this.id;
    return data;
  }
}

class Court {
  String createdAt;
  bool isActive;
  String name;
  String town;
  String id;

  Court({this.createdAt, this.isActive, this.name, this.town, this.id});

  Court.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    town = json['town'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['town'] = this.town;
    data['id'] = this.id;
    return data;
  }
}

class BriefType {
  String createdAt;
  bool isActive;
  String caseType;
  String description;
  int amount;
  String id;

  BriefType(
      {this.createdAt,
      this.isActive,
      this.caseType,
      this.description,
      this.amount,
      this.id});

  BriefType.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    caseType = json['case_type'];
    description = json['description'];
    amount = json['amount'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['case_type'] = this.caseType;
    data['description'] = this.description;
    data['amount'] = this.amount;
    data['id'] = this.id;
    return data;
  }
}
