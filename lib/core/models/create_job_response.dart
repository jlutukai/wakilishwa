class CreateJobResponse {
  String status;
  Job job;
  String message;

  CreateJobResponse({this.status, this.job, this.message});

  CreateJobResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    job = json['job'] != null ? new Job.fromJson(json['job']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.job != null) {
      data['job'] = this.job.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Job {
  int numberOfBids;
  String creditAmount;
  String additionalFees;
  String status;
  String createdAt;
  bool isActive;
  String user;
  String title;
  String court;
  String jobTime;
  String town;
  String courtRoom;
  String magistrateOrJudge;
  String jobDate;
  String briefType;
  String description;
  String id;

  Job(
      {this.numberOfBids,
      this.creditAmount,
      this.additionalFees,
      this.status,
      this.createdAt,
      this.isActive,
      this.user,
      this.title,
      this.court,
      this.jobTime,
      this.town,
      this.courtRoom,
      this.magistrateOrJudge,
      this.jobDate,
      this.briefType,
      this.description,
      this.id});

  Job.fromJson(Map<String, dynamic> json) {
    numberOfBids = json['number_of_bids'];
    creditAmount = json['credit_amount']?.toString();
    additionalFees = json['additional_fees'].toString();
    status = json['status'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    user = json['user'];
    title = json['title'];
    court = json['court'];
    jobTime = json['job_time'];
    town = json['town'];
    courtRoom = json['court_room'];
    magistrateOrJudge = json['magistrate_or_judge'];
    jobDate = json['job_date'];
    briefType = json['brief_type'];
    description = json['description'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number_of_bids'] = this.numberOfBids;
    data['credit_amount'] = this.creditAmount;
    data['additional_fees'] = this.additionalFees;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['user'] = this.user;
    data['title'] = this.title;
    data['court'] = this.court;
    data['job_time'] = this.jobTime;
    data['town'] = this.town;
    data['court_room'] = this.courtRoom;
    data['magistrate_or_judge'] = this.magistrateOrJudge;
    data['job_date'] = this.jobDate;
    data['brief_type'] = this.briefType;
    data['description'] = this.description;
    data['id'] = this.id;
    return data;
  }
}
