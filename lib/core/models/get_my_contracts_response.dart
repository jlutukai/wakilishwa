import 'package:wakilishwa/core/models/get_jobs_response.dart';
import 'package:wakilishwa/core/models/local/local_models.dart';

class GetContractsResponse {
  String status;
  Page page;
  int count;
  List<Contracts> contracts;

  GetContractsResponse({this.status, this.page, this.count, this.contracts});

  GetContractsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    page = json['page'] != null ? new Page.fromJson(json['page']) : null;
    count = json['count'];
    if (json['contracts'] != null) {
      contracts = new List<Contracts>();
      json['contracts'].forEach((v) {
        contracts.add(new Contracts.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.page != null) {
      data['page'] = this.page.toJson();
    }
    data['count'] = this.count;
    if (this.contracts != null) {
      data['contracts'] = this.contracts.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Page {
  int prev;
  int next;
  int current;
  int limit;

  Page({this.prev, this.next, this.current, this.limit});

  Page.fromJson(Map<String, dynamic> json) {
    prev = json['prev'];
    next = json['next'];
    current = json['current'];
    limit = json['limit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['prev'] = this.prev;
    data['next'] = this.next;
    data['current'] = this.current;
    data['limit'] = this.limit;
    return data;
  }
}

class Contracts {
  String feedback;
  String returnFeedback;
  String status;
  String createdAt;
  bool isActive;
  JobsData job;
  UserData user;
  Contractor contractor;
  String bid;
  String id;

  Contracts(
      {this.feedback,
      this.returnFeedback,
      this.status,
      this.createdAt,
      this.isActive,
      this.job,
      this.user,
      this.contractor,
      this.bid,
      this.id});

  Contracts.fromJson(Map<String, dynamic> json) {
    feedback = json['feedback'];
    returnFeedback = json['return_feedback'];
    status = json['status'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    job = json['job'] != null ? new JobsData.fromJson(json['job']) : null;
    user = json['user'] != null ? new UserData.fromJson(json['user']) : null;
    contractor = json['contractor'] != null
        ? new Contractor.fromJson(json['contractor'])
        : null;
    bid = json['bid'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['feedback'] = this.feedback;
    data['return_feedback'] = this.returnFeedback;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    if (this.job != null) {
      data['job'] = this.job.toJson();
    }
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.contractor != null) {
      data['contractor'] = this.contractor.toJson();
    }
    data['bid'] = this.bid;
    data['id'] = this.id;
    return data;
  }
}

class Job {
  int numberOfBids;
  String creditAmount;
  String additionalFees;
  String status;
  String createdAt;
  bool isActive;
  UserData user;
  String title;
  Court court;
  String jobTime;
  Town town;
  String courtRoom;
  String magistrateOrJudge;
  String jobDate;
  BriefType briefType;
  String description;
  String id;

  Job(
      {this.numberOfBids,
      this.creditAmount,
      this.additionalFees,
      this.status,
      this.createdAt,
      this.isActive,
      this.user,
      this.title,
      this.court,
      this.jobTime,
      this.town,
      this.courtRoom,
      this.magistrateOrJudge,
      this.jobDate,
      this.briefType,
      this.description,
      this.id});

  Job.fromJson(Map<String, dynamic> json) {
    numberOfBids = json['number_of_bids'];
    creditAmount = json['credit_amount']?.toString();
    additionalFees = json['additional_fees']?.toString();
    status = json['status'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    user = json['user'] != null ? new UserData.fromJson(json['user']) : null;
    title = json['title'];
    court = json['court'] != null ? new Court.fromJson(json['court']) : null;
    jobTime = json['job_time'];
    town = json['town'] != null ? new Town.fromJson(json['town']) : null;
    courtRoom = json['court_room'];
    magistrateOrJudge = json['magistrate_or_judge'];
    jobDate = json['job_date'];
    briefType = json['brief_type'] != null
        ? new BriefType.fromJson(json['brief_type'])
        : null;
    description = json['description'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number_of_bids'] = this.numberOfBids;
    data['credit_amount'] = this.creditAmount;
    data['additional_fees'] = this.additionalFees;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['title'] = this.title;
    if (this.court != null) {
      data['court'] = this.court.toJson();
    }
    data['job_time'] = this.jobTime;
    if (this.town != null) {
      data['town'] = this.town.toJson();
    }
    data['court_room'] = this.courtRoom;
    data['magistrate_or_judge'] = this.magistrateOrJudge;
    data['job_date'] = this.jobDate;
    if (this.briefType != null) {
      data['brief_type'] = this.briefType.toJson();
    }
    data['description'] = this.description;
    data['id'] = this.id;
    return data;
  }
}

class Name {
  String first;
  String others;

  Name({this.first, this.others});

  Name.fromJson(Map<String, dynamic> json) {
    first = json['first'];
    others = json['others'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first'] = this.first;
    data['others'] = this.others;
    return data;
  }
}

class FreeCredits {
  int jobAssigned;
  int jobCount;
  int amount;

  FreeCredits({this.jobAssigned, this.jobCount, this.amount});

  FreeCredits.fromJson(Map<String, dynamic> json) {
    jobAssigned = json['job_assigned'];
    jobCount = json['job_count'];
    amount = json['amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['job_assigned'] = this.jobAssigned;
    data['job_count'] = this.jobCount;
    data['amount'] = this.amount;
    return data;
  }
}

class AccountType {
  String createdAt;
  bool isActive;
  String name;
  String typeName;
  String id;

  AccountType(
      {this.createdAt, this.isActive, this.name, this.typeName, this.id});

  AccountType.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    typeName = json['type_name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['type_name'] = this.typeName;
    data['id'] = this.id;
    return data;
  }
}

class Country {
  String createdAt;
  bool isActive;
  String name;
  String dialCode;
  String currencyName;
  String currencyCode;
  String code;
  String id;

  Country(
      {this.createdAt,
      this.isActive,
      this.name,
      this.dialCode,
      this.currencyName,
      this.currencyCode,
      this.code,
      this.id});

  Country.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    dialCode = json['dial_code'];
    currencyName = json['currency_name'];
    currencyCode = json['currency_code'];
    code = json['code'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['dial_code'] = this.dialCode;
    data['currency_name'] = this.currencyName;
    data['currency_code'] = this.currencyCode;
    data['code'] = this.code;
    data['id'] = this.id;
    return data;
  }
}

class Region {
  String createdAt;
  bool isActive;
  String country;
  String name;
  String id;

  Region({this.createdAt, this.isActive, this.country, this.name, this.id});

  Region.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    country = json['country'];
    name = json['name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['country'] = this.country;
    data['name'] = this.name;
    data['id'] = this.id;
    return data;
  }
}

class Town {
  String createdAt;
  bool isActive;
  String name;
  String region;
  String id;

  Town({this.createdAt, this.isActive, this.name, this.region, this.id});

  Town.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    region = json['region'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['region'] = this.region;
    data['id'] = this.id;
    return data;
  }
}

class Court {
  String createdAt;
  bool isActive;
  String name;
  String town;
  String id;

  Court({this.createdAt, this.isActive, this.name, this.town, this.id});

  Court.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    town = json['town'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['town'] = this.town;
    data['id'] = this.id;
    return data;
  }
}

class BriefType {
  String createdAt;
  bool isActive;
  String caseType;
  String description;
  int amount;
  String id;

  BriefType(
      {this.createdAt,
      this.isActive,
      this.caseType,
      this.description,
      this.amount,
      this.id});

  BriefType.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    caseType = json['case_type'];
    description = json['description'];
    amount = json['amount'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['case_type'] = this.caseType;
    data['description'] = this.description;
    data['amount'] = this.amount;
    data['id'] = this.id;
    return data;
  }
}

class User {
  Name name;
  FreeCredits freeCredits;
  String description;
  bool isSuperAdmin;
  String userType;
  String createdAt;
  bool isActive;
  bool isApproved;
  String email;
  String phone;
  AccountType accountType;
  String idNumber;
  String passportNumber;
  String admissionNumber;
  Country country;
  Region region;
  Town town;
  String photo;
  String id;

  User(
      {this.name,
      this.freeCredits,
      this.description,
      this.isSuperAdmin,
      this.userType,
      this.createdAt,
      this.isActive,
      this.isApproved,
      this.email,
      this.phone,
      this.accountType,
      this.idNumber,
      this.passportNumber,
      this.admissionNumber,
      this.country,
      this.region,
      this.town,
      this.photo,
      this.id});

  User.fromJson(Map<String, dynamic> json) {
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
    freeCredits = json['free_credits'] != null
        ? new FreeCredits.fromJson(json['free_credits'])
        : null;
    description = json['description'];
    isSuperAdmin = json['is_super_admin'];
    userType = json['user_type'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isApproved = json['is_approved'];
    email = json['email'];
    phone = json['phone'];
    accountType = json['account_type'] != null
        ? new AccountType.fromJson(json['account_type'])
        : null;
    idNumber = json['id_number'];
    passportNumber = json['passport_number'];
    admissionNumber = json['admission_number'];
    country =
        json['country'] != null ? new Country.fromJson(json['country']) : null;
    region =
        json['region'] != null ? new Region.fromJson(json['region']) : null;
    town = json['town'] != null ? new Town.fromJson(json['town']) : null;
    photo = json['photo'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.freeCredits != null) {
      data['free_credits'] = this.freeCredits.toJson();
    }
    data['description'] = this.description;
    data['is_super_admin'] = this.isSuperAdmin;
    data['user_type'] = this.userType;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_approved'] = this.isApproved;
    data['email'] = this.email;
    data['phone'] = this.phone;
    if (this.accountType != null) {
      data['account_type'] = this.accountType.toJson();
    }
    data['id_number'] = this.idNumber;
    data['passport_number'] = this.passportNumber;
    data['admission_number'] = this.admissionNumber;
    if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    if (this.region != null) {
      data['region'] = this.region.toJson();
    }
    if (this.town != null) {
      data['town'] = this.town.toJson();
    }
    data['photo'] = this.photo;
    data['id'] = this.id;
    return data;
  }
}

class Contractor {
  Name name;
  String email;
  String phone;
  AccountType accountType;
  Country country;
  Region region;
  Town town;
  String id;

  Contractor(
      {this.name,
      this.email,
      this.phone,
      this.accountType,
      this.country,
      this.region,
      this.town,
      this.id});

  Contractor.fromJson(Map<String, dynamic> json) {
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
    email = json['email'];
    phone = json['phone'];
    accountType = json['account_type'] != null
        ? new AccountType.fromJson(json['account_type'])
        : null;
    country =
        json['country'] != null ? new Country.fromJson(json['country']) : null;
    region =
        json['region'] != null ? new Region.fromJson(json['region']) : null;
    town = json['town'] != null ? new Town.fromJson(json['town']) : null;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    data['email'] = this.email;
    data['phone'] = this.phone;
    if (this.accountType != null) {
      data['account_type'] = this.accountType.toJson();
    }
    if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    if (this.region != null) {
      data['region'] = this.region.toJson();
    }
    if (this.town != null) {
      data['town'] = this.town.toJson();
    }
    data['id'] = this.id;
    return data;
  }
}
