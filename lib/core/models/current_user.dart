import 'local/local_models.dart';

class GetCurrentUserResponse {
  String status;
  UserData user;
  Reports reports;

  GetCurrentUserResponse({this.status, this.user, this.reports});

  GetCurrentUserResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    user = json['user'] != null ? new UserData.fromJson(json['user']) : null;
    reports =
        json['reports'] != null ? new Reports.fromJson(json['reports']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.reports != null) {
      data['reports'] = this.reports.toJson();
    }
    return data;
  }
}

class Reports {
  Jobs jobs;
  Ratings ratings;

  Reports({this.jobs, this.ratings});

  Reports.fromJson(Map<String, dynamic> json) {
    jobs = json['jobs'] != null ? new Jobs.fromJson(json['jobs']) : null;
    ratings =
        json['ratings'] != null ? new Ratings.fromJson(json['ratings']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.jobs != null) {
      data['jobs'] = this.jobs.toJson();
    }
    if (this.ratings != null) {
      data['ratings'] = this.ratings.toJson();
    }
    return data;
  }
}

class Jobs {
  int completed;
  int archived;
  int posted;
  int inprogress;

  Jobs({this.completed, this.archived, this.posted, this.inprogress});

  Jobs.fromJson(Map<String, dynamic> json) {
    completed = json['completed'];
    archived = json['archived'];
    posted = json['posted'];
    inprogress = json['inprogress'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['completed'] = this.completed;
    data['archived'] = this.archived;
    data['posted'] = this.posted;
    data['inprogress'] = this.inprogress;
    return data;
  }
}

class Ratings {
  double average;
  List<Rates> rates;

  Ratings({this.average, this.rates});

  Ratings.fromJson(Map<String, dynamic> json) {
    average = json['average']?.toDouble();
    if (json['rates'] != null) {
      rates = new List<Rates>();
      json['rates'].forEach((v) {
        rates.add(new Rates.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['average'] = this.average;
    if (this.rates != null) {
      data['rates'] = this.rates.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Rates {
  String createdAt;
  bool isActive;
  String user;
  String job;
  String id;
  String comment;
  double rating;

  Rates(
      {this.createdAt,
      this.isActive,
      this.user,
      this.job,
      this.id,
      this.comment,
      this.rating});

  Rates.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    user = json['user'];
    job = json['job'];
    id = json['id'];
    comment = json['comment'];
    rating = json['rating']?.toDouble();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['user'] = this.user;
    data['job'] = this.job;
    data['id'] = this.id;
    data['comment'] = this.comment;
    data['rating'] = this.rating;
    return data;
  }
}
