class GetAppCharges {
  String status;
  ServiceSetup serviceSetup;

  GetAppCharges({this.status, this.serviceSetup});

  GetAppCharges.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    serviceSetup = json['service_setup'] != null
        ? new ServiceSetup.fromJson(json['service_setup'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.serviceSetup != null) {
      data['service_setup'] = this.serviceSetup.toJson();
    }
    return data;
  }
}

class ServiceSetup {
  String setupType;
  int penalty;
  double transactionCost;
  double transactionRate;
  int maximumWithdrawAmountPerDay;
  int maximumWithdrawAmountPerTransaction;
  bool isActive;
  String createdAt;
  String id;

  ServiceSetup(
      {this.setupType,
      this.penalty,
      this.transactionCost,
      this.transactionRate,
      this.maximumWithdrawAmountPerDay,
      this.maximumWithdrawAmountPerTransaction,
      this.isActive,
      this.createdAt,
      this.id});

  ServiceSetup.fromJson(Map<String, dynamic> json) {
    setupType = json['setup_type'];
    penalty = json['penalty'];
    transactionCost = json['transaction_cost']?.toDouble();
    transactionRate = json['transaction_rate']?.toDouble();
    maximumWithdrawAmountPerDay = json['maximum_withdraw_amount_per_day'];
    maximumWithdrawAmountPerTransaction =
        json['maximum_withdraw_amount_per_transaction'];
    isActive = json['is_active'];
    createdAt = json['created_at'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['setup_type'] = this.setupType;
    data['penalty'] = this.penalty;
    data['transaction_cost'] = this.transactionCost;
    data['transaction_rate'] = this.transactionRate;
    data['maximum_withdraw_amount_per_day'] = this.maximumWithdrawAmountPerDay;
    data['maximum_withdraw_amount_per_transaction'] =
        this.maximumWithdrawAmountPerTransaction;
    data['is_active'] = this.isActive;
    data['created_at'] = this.createdAt;
    data['id'] = this.id;
    return data;
  }
}
