import 'brief_types_response.dart';

class GetCourtsResponse {
  String status;
  Page page;
  int count;
  List<CourtsData> courts;

  GetCourtsResponse({this.status, this.page, this.count, this.courts});

  GetCourtsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    page = json['page'] != null ? new Page.fromJson(json['page']) : null;
    count = json['count'];
    if (json['courts'] != null) {
      courts = new List<CourtsData>();
      json['courts'].forEach((v) {
        courts.add(new CourtsData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.page != null) {
      data['page'] = this.page.toJson();
    }
    data['count'] = this.count;
    if (this.courts != null) {
      data['courts'] = this.courts.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CourtsData {
  String createdAt;
  bool isActive;
  String name;
  Town town;
  String id;

  CourtsData({this.createdAt, this.isActive, this.name, this.town, this.id});

  CourtsData.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    town = json['town'] != null ? new Town.fromJson(json['town']) : null;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    if (this.town != null) {
      data['town'] = this.town.toJson();
    }
    data['id'] = this.id;
    return data;
  }
}

class Town {
  String createdAt;
  bool isActive;
  String name;
  String region;
  String id;

  Town({this.createdAt, this.isActive, this.name, this.region, this.id});

  Town.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    region = json['region'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['region'] = this.region;
    data['id'] = this.id;
    return data;
  }
}
