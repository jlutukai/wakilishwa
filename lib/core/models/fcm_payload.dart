class FCMPayLoad {
  String to;
  String collapseKey;
  Notification notification;
  Data data;

  FCMPayLoad({this.to, this.collapseKey, this.notification, this.data});

  FCMPayLoad.fromJson(Map<String, dynamic> json) {
    to = json['to'];
    collapseKey = json['collapse_key'];
    notification = json['notification'] != null
        ? new Notification.fromJson(json['notification'])
        : null;
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['to'] = this.to;
    data['collapse_key'] = this.collapseKey;
    if (this.notification != null) {
      data['notification'] = this.notification.toJson();
    }
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Notification {
  String body;
  String title;

  Notification({this.body, this.title});

  Notification.fromJson(Map<String, dynamic> json) {
    body = json['body'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['body'] = this.body;
    data['title'] = this.title;
    return data;
  }
}

class Data {
  String clickAction;
  String screen;
  String chatId;
  String type;

  Data({this.clickAction, this.screen, this.chatId, this.type});

  Data.fromJson(Map<String, dynamic> json) {
    clickAction = json['click_action'];
    screen = json['screen'];
    chatId = json['chatId'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['click_action'] = this.clickAction;
    data['screen'] = this.screen;
    data['chatId'] = this.chatId;
    data['type'] = this.type;
    return data;
  }
}
