class ChatsResponse {
  String status;
  int count;
  List<ChatsData> chats;

  ChatsResponse({this.status, this.count, this.chats});

  ChatsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    count = json['count'];
    if (json['chats'] != null) {
      chats = new List<ChatsData>();
      json['chats'].forEach((v) {
        chats.add(new ChatsData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['count'] = this.count;
    if (this.chats != null) {
      data['chats'] = this.chats.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ChatsData {
  String status;
  String chatType;
  String createdAt;
  bool isActive;
  Reciever reciever;
  String bid;
  Reciever user;
  String id;
  String message;

  ChatsData(
      {this.status,
      this.chatType,
      this.createdAt,
      this.isActive,
      this.reciever,
      this.bid,
      this.user,
      this.id,
      this.message});

  ChatsData.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    chatType = json['chat_type'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    reciever = json['reciever'] != null
        ? new Reciever.fromJson(json['reciever'])
        : null;
    bid = json['bid'];
    user = json['user'] != null ? new Reciever.fromJson(json['user']) : null;
    id = json['id'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['chat_type'] = this.chatType;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    if (this.reciever != null) {
      data['reciever'] = this.reciever.toJson();
    }
    data['bid'] = this.bid;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['id'] = this.id;
    data['message'] = this.message;
    return data;
  }
}

class Reciever {
  Name name;
  String email;
  String phone;
  AccountType accountType;
  Country country;
  Region region;
  Town town;
  String id;

  Reciever(
      {this.name,
      this.email,
      this.phone,
      this.accountType,
      this.country,
      this.region,
      this.town,
      this.id});

  Reciever.fromJson(Map<String, dynamic> json) {
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
    email = json['email'];
    phone = json['phone'];
    accountType = json['account_type'] != null
        ? new AccountType.fromJson(json['account_type'])
        : null;
    country =
        json['country'] != null ? new Country.fromJson(json['country']) : null;
    region =
        json['region'] != null ? new Region.fromJson(json['region']) : null;
    town = json['town'] != null ? new Town.fromJson(json['town']) : null;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    data['email'] = this.email;
    data['phone'] = this.phone;
    if (this.accountType != null) {
      data['account_type'] = this.accountType.toJson();
    }
    if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    if (this.region != null) {
      data['region'] = this.region.toJson();
    }
    if (this.town != null) {
      data['town'] = this.town.toJson();
    }
    data['id'] = this.id;
    return data;
  }
}

class Name {
  String first;
  String others;

  Name({this.first, this.others});

  Name.fromJson(Map<String, dynamic> json) {
    first = json['first'];
    others = json['others'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first'] = this.first;
    data['others'] = this.others;
    return data;
  }
}

class AccountType {
  String createdAt;
  bool isActive;
  String name;
  String typeName;
  String id;

  AccountType(
      {this.createdAt, this.isActive, this.name, this.typeName, this.id});

  AccountType.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    typeName = json['type_name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['type_name'] = this.typeName;
    data['id'] = this.id;
    return data;
  }
}

class Country {
  String createdAt;
  bool isActive;
  String name;
  String dialCode;
  String currencyName;
  String currencyCode;
  String code;
  String id;

  Country(
      {this.createdAt,
      this.isActive,
      this.name,
      this.dialCode,
      this.currencyName,
      this.currencyCode,
      this.code,
      this.id});

  Country.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    dialCode = json['dial_code'];
    currencyName = json['currency_name'];
    currencyCode = json['currency_code'];
    code = json['code'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['dial_code'] = this.dialCode;
    data['currency_name'] = this.currencyName;
    data['currency_code'] = this.currencyCode;
    data['code'] = this.code;
    data['id'] = this.id;
    return data;
  }
}

class Region {
  String createdAt;
  bool isActive;
  String country;
  String name;
  String id;

  Region({this.createdAt, this.isActive, this.country, this.name, this.id});

  Region.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    country = json['country'];
    name = json['name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['country'] = this.country;
    data['name'] = this.name;
    data['id'] = this.id;
    return data;
  }
}

class Town {
  String createdAt;
  bool isActive;
  String name;
  String region;
  String id;

  Town({this.createdAt, this.isActive, this.name, this.region, this.id});

  Town.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    region = json['region'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['region'] = this.region;
    data['id'] = this.id;
    return data;
  }
}
