import 'package:wakilishwa/core/models/brief_types_response.dart';

import 'local/local_models.dart';

class GetJobsResponse {
  String status;
  Page page;
  int count;
  List<JobsData> jobs;

  GetJobsResponse({this.status, this.page, this.count, this.jobs});

  GetJobsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    page = json['page'] != null ? new Page.fromJson(json['page']) : null;
    count = json['count'];
    if (json['jobs'] != null) {
      jobs = new List<JobsData>();
      json['jobs'].forEach((v) {
        jobs.add(new JobsData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.page != null) {
      data['page'] = this.page.toJson();
    }
    data['count'] = this.count;
    if (this.jobs != null) {
      data['jobs'] = this.jobs.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class JobsData {
  int numberOfBids;
  int creditAmount;
  double additionalFees;
  String status;
  String createdAt;
  bool isActive;
  UserData user;
  String title;
  Court court;
  String jobTime;
  TownData town;
  String courtRoom;
  String magistrateOrJudge;
  String jobDate;
  ChargesData briefType;
  String description;
  String id;

  JobsData(
      {this.numberOfBids,
      this.creditAmount,
      this.additionalFees,
      this.status,
      this.createdAt,
      this.isActive,
      this.user,
      this.title,
      this.court,
      this.jobTime,
      this.town,
      this.courtRoom,
      this.magistrateOrJudge,
      this.jobDate,
      this.briefType,
      this.description,
      this.id});

  JobsData.fromJson(Map<String, dynamic> json) {
    numberOfBids = json['number_of_bids'];
    creditAmount = json['credit_amount'];
    additionalFees = json['additional_fees']?.toDouble();
    status = json['status'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    user = json['user'] != null ? new UserData.fromJson(json['user']) : null;
    title = json['title'];
    court = json['court'] != null ? new Court.fromJson(json['court']) : null;
    jobTime = json['job_time'];
    town = json['town'] != null ? new TownData.fromJson(json['town']) : null;
    courtRoom = json['court_room'];
    magistrateOrJudge = json['magistrate_or_judge'];
    jobDate = json['job_date'];
    briefType = json['brief_type'] != null
        ? new ChargesData.fromJson(json['brief_type'])
        : null;
    description = json['description'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number_of_bids'] = this.numberOfBids;
    data['credit_amount'] = this.creditAmount;
    data['additional_fees'] = this.additionalFees;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['title'] = this.title;
    if (this.court != null) {
      data['court'] = this.court.toJson();
    }
    data['job_time'] = this.jobTime;
    if (this.town != null) {
      data['town'] = this.town.toJson();
    }
    data['court_room'] = this.courtRoom;
    data['magistrate_or_judge'] = this.magistrateOrJudge;
    data['job_date'] = this.jobDate;
    if (this.briefType != null) {
      data['brief_type'] = this.briefType.toJson();
    }
    data['description'] = this.description;
    data['id'] = this.id;
    return data;
  }
}

// class Name {
//   String first;
//   String others;
//
//   Name({this.first, this.others});
//
//   Name.fromJson(Map<String, dynamic> json) {
//     first = json['first'];
//     others = json['others'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['first'] = this.first;
//     data['others'] = this.others;
//     return data;
//   }
// }
//
// class FreeCredits {
//   int jobAssigned;
//   int jobCount;
//   int amount;
//
//   FreeCredits({this.jobAssigned, this.jobCount, this.amount});
//
//   FreeCredits.fromJson(Map<String, dynamic> json) {
//     jobAssigned = json['job_assigned'];
//     jobCount = json['job_count'];
//     amount = json['amount'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['job_assigned'] = this.jobAssigned;
//     data['job_count'] = this.jobCount;
//     data['amount'] = this.amount;
//     return data;
//   }
// }
//
// class AccountType {
//   String createdAt;
//   bool isActive;
//   String name;
//   String typeName;
//   String id;
//
//   AccountType(
//       {this.createdAt, this.isActive, this.name, this.typeName, this.id});
//
//   AccountType.fromJson(Map<String, dynamic> json) {
//     createdAt = json['created_at'];
//     isActive = json['is_active'];
//     name = json['name'];
//     typeName = json['type_name'];
//     id = json['id'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['created_at'] = this.createdAt;
//     data['is_active'] = this.isActive;
//     data['name'] = this.name;
//     data['type_name'] = this.typeName;
//     data['id'] = this.id;
//     return data;
//   }
// }
//
// class Country {
//   String createdAt;
//   bool isActive;
//   String name;
//   String dialCode;
//   String currencyName;
//   String currencyCode;
//   String code;
//   String id;
//
//   Country(
//       {this.createdAt,
//       this.isActive,
//       this.name,
//       this.dialCode,
//       this.currencyName,
//       this.currencyCode,
//       this.code,
//       this.id});
//
//   Country.fromJson(Map<String, dynamic> json) {
//     createdAt = json['created_at'];
//     isActive = json['is_active'];
//     name = json['name'];
//     dialCode = json['dial_code'];
//     currencyName = json['currency_name'];
//     currencyCode = json['currency_code'];
//     code = json['code'];
//     id = json['id'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['created_at'] = this.createdAt;
//     data['is_active'] = this.isActive;
//     data['name'] = this.name;
//     data['dial_code'] = this.dialCode;
//     data['currency_name'] = this.currencyName;
//     data['currency_code'] = this.currencyCode;
//     data['code'] = this.code;
//     data['id'] = this.id;
//     return data;
//   }
// }
//
// class Region {
//   String createdAt;
//   bool isActive;
//   String country;
//   String name;
//   String id;
//
//   Region({this.createdAt, this.isActive, this.country, this.name, this.id});
//
//   Region.fromJson(Map<String, dynamic> json) {
//     createdAt = json['created_at'];
//     isActive = json['is_active'];
//     country = json['country'];
//     name = json['name'];
//     id = json['id'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['created_at'] = this.createdAt;
//     data['is_active'] = this.isActive;
//     data['country'] = this.country;
//     data['name'] = this.name;
//     data['id'] = this.id;
//     return data;
//   }
// }
//
// class Town {
//   String createdAt;
//   bool isActive;
//   String name;
//   String region;
//   String id;
//
//   Town({this.createdAt, this.isActive, this.name, this.region, this.id});
//
//   Town.fromJson(Map<String, dynamic> json) {
//     createdAt = json['created_at'];
//     isActive = json['is_active'];
//     name = json['name'];
//     region = json['region'];
//     id = json['id'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['created_at'] = this.createdAt;
//     data['is_active'] = this.isActive;
//     data['name'] = this.name;
//     data['region'] = this.region;
//     data['id'] = this.id;
//     return data;
//   }
// }

class Court {
  String createdAt;
  bool isActive;
  String name;
  String town;
  String id;

  Court({this.createdAt, this.isActive, this.name, this.town, this.id});

  Court.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    town = json['town'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['town'] = this.town;
    data['id'] = this.id;
    return data;
  }
}
