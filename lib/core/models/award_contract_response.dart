class AwardContractResponse {
  String status;
  String message;
  Contract contract;

  AwardContractResponse({this.status, this.message, this.contract});

  AwardContractResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    contract = json['contract'] != null
        ? new Contract.fromJson(json['contract'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.contract != null) {
      data['contract'] = this.contract.toJson();
    }
    return data;
  }
}

class Contract {
  String status;
  String createdAt;
  bool isActive;
  String job;
  String user;
  String contractor;
  String bid;
  String id;

  Contract(
      {this.status,
      this.createdAt,
      this.isActive,
      this.job,
      this.user,
      this.contractor,
      this.bid,
      this.id});

  Contract.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    job = json['job'];
    user = json['user'];
    contractor = json['contractor'];
    bid = json['bid'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['job'] = this.job;
    data['user'] = this.user;
    data['contractor'] = this.contractor;
    data['bid'] = this.bid;
    data['id'] = this.id;
    return data;
  }
}
