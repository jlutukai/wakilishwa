class VerifyAdvocateResponse {
  String status;
  Advocate advocate;

  VerifyAdvocateResponse({this.status, this.advocate});

  VerifyAdvocateResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    advocate = json['advocate'] != null
        ? new Advocate.fromJson(json['advocate'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.advocate != null) {
      data['advocate'] = this.advocate.toJson();
    }
    return data;
  }
}

class Advocate {
  String firstName;
  String otherNames;
  String adovateNumber;
  bool isActive;
  String practisingYear;
  String recordsFound;

  Advocate(
      {this.firstName,
      this.otherNames,
      this.adovateNumber,
      this.isActive,
      this.practisingYear,
      this.recordsFound});

  Advocate.fromJson(Map<String, dynamic> json) {
    firstName = json['first_name'];
    otherNames = json['other_names'];
    adovateNumber = json['adovate_number'];
    isActive = json['is_active'];
    practisingYear = json['practising_year'];
    recordsFound = json['records_found'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first_name'] = this.firstName;
    data['other_names'] = this.otherNames;
    data['adovate_number'] = this.adovateNumber;
    data['is_active'] = this.isActive;
    data['practising_year'] = this.practisingYear;
    data['records_found'] = this.recordsFound;
    return data;
  }
}
