// import 'package:flutter_socket_io/flutter_socket_io.dart';
// import 'package:flutter_socket_io/socket_io_manager.dart';
// import 'package:wakilishwa/core/models/get_chats_response.dart';
// import 'package:wakilishwa/core/services/api.dart';
// import 'package:wakilishwa/core/viewmodels/base_model.dart';

// class ChatModel extends BaseModel {
//   List<ChatsData> messages;
//
//   void init(String id) {
//
//     var io = new Server();
//     var nsp = io.of('/some');
//     nsp.on('connection', (Socket client) {
//       print('connection /some');
//       client.on('msg', (data) {
//         print('data from /some => $data');
//         client.emit('fromServer', "ok 2");
//       });
//     });
//     io.on('connection', (Socket client) {
//       print('connection default namespace');
//       client.on('msg', (data) {
//         print('data from default => $data');
//         client.emit('fromServer', "ok");
//       });
//     });
//     io.listen(3000);
//
//     // Dart client
//     IO.Socket socket = IO.io('http://localhost:3000');
//     socket.onConnect((_) {
//       print('connect');
//       socket.emit('msg', 'test');
//     });
//     socket.on('event', (data) => print(data));
//     socket.onDisconnect((_) => print('disconnect'));
//     socket.on('fromServer', (_) => print(_));
//
//     socketIO = SocketIOManager()
//         .createSocketIO("${Api.baseUrl}/chats/by-bids/$id", '/').;
//     socketIO.init();
//
//     socketIO.subscribe('message_created', (jsonData) {
//       Map<String, dynamic> data = json.decode(jsonData);
//       messages.add(Message(
//           data['content'], data['senderChatID'], data['receiverChatID']));
//       notifyListeners();
//     });
//
//     socketIO.connect();
//   }
//
//   void sendMessage(String text, String receiverChatID) {
//     messages.add(Message(text, currentUser.chatID, receiverChatID));
//     socketIO.sendMessage(
//       'message_created',
//       json.encode({
//         'receiverChatID': receiverChatID,
//         'senderChatID': currentUser.chatID,
//         'content': text,
//       }),
//     );
//     notifyListeners();
//   }
//
//   List<Message> getMessagesForChatID(String chatID) {
//     return messages
//         .where((msg) => msg.senderID == chatID || msg.receiverID == chatID)
//         .toList();
//   }
// }
