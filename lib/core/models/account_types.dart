import 'package:wakilishwa/core/models/local/local_models.dart';

import 'brief_types_response.dart';

class GetAccountTypesResponse {
  String status;
  Page page;
  int count;
  List<AccountTypeData> accountTypes;

  GetAccountTypesResponse(
      {this.status, this.page, this.count, this.accountTypes});

  GetAccountTypesResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    page = json['page'] != null ? new Page.fromJson(json['page']) : null;
    count = json['count'];
    if (json['account_types'] != null) {
      accountTypes = new List<AccountTypeData>();
      json['account_types'].forEach((v) {
        accountTypes.add(new AccountTypeData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.page != null) {
      data['page'] = this.page.toJson();
    }
    data['count'] = this.count;
    if (this.accountTypes != null) {
      data['account_types'] = this.accountTypes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AccountTypes {
  String createdAt;
  bool isActive;
  String name;
  String typeName;
  String id;

  AccountTypes(
      {this.createdAt, this.isActive, this.name, this.typeName, this.id});

  AccountTypes.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    typeName = json['type_name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['type_name'] = this.typeName;
    data['id'] = this.id;
    return data;
  }
}
