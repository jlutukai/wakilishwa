class GetWalletResponse {
  String status;
  Wallet wallet;

  GetWalletResponse({this.status, this.wallet});

  GetWalletResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    wallet =
        json['wallet'] != null ? new Wallet.fromJson(json['wallet']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.wallet != null) {
      data['wallet'] = this.wallet.toJson();
    }
    return data;
  }
}

class Wallet {
  double amountIn;
  double amountOut;
  double amountBalance;
  bool isActive;
  String createdAt;
  String user;
  String walletType;
  String id;

  Wallet(
      {this.amountIn,
      this.amountOut,
      this.amountBalance,
      this.isActive,
      this.createdAt,
      this.user,
      this.walletType,
      this.id});

  Wallet.fromJson(Map<String, dynamic> json) {
    amountIn = json['amount_in']?.toDouble();
    amountOut = json['amount_out']?.toDouble();
    amountBalance = json['amount_balance']?.toDouble();
    isActive = json['is_active'];
    createdAt = json['created_at'];
    user = json['user'];
    walletType = json['wallet_type'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount_in'] = this.amountIn;
    data['amount_out'] = this.amountOut;
    data['amount_balance'] = this.amountBalance;
    data['is_active'] = this.isActive;
    data['created_at'] = this.createdAt;
    data['user'] = this.user;
    data['wallet_type'] = this.walletType;
    data['id'] = this.id;
    return data;
  }
}
