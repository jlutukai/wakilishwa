class GetTownsResponse {
  String status;
  Page page;
  int count;
  List<TownsData> towns;

  GetTownsResponse({this.status, this.page, this.count, this.towns});

  GetTownsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    page = json['page'] != null ? new Page.fromJson(json['page']) : null;
    count = json['count'];
    if (json['towns'] != null) {
      towns = new List<TownsData>();
      json['towns'].forEach((v) {
        towns.add(new TownsData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.page != null) {
      data['page'] = this.page.toJson();
    }
    data['count'] = this.count;
    if (this.towns != null) {
      data['towns'] = this.towns.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Page {
  int prev;
  int next;
  int current;
  int limit;

  Page({this.prev, this.next, this.current, this.limit});

  Page.fromJson(Map<String, dynamic> json) {
    prev = json['prev'];
    next = json['next'];
    current = json['current'];
    limit = json['limit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['prev'] = this.prev;
    data['next'] = this.next;
    data['current'] = this.current;
    data['limit'] = this.limit;
    return data;
  }
}

class TownsData {
  String createdAt;
  bool isActive;
  String name;
  Region region;
  String id;

  TownsData({this.createdAt, this.isActive, this.name, this.region, this.id});

  TownsData.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    region =
        json['region'] != null ? new Region.fromJson(json['region']) : null;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    if (this.region != null) {
      data['region'] = this.region.toJson();
    }
    data['id'] = this.id;
    return data;
  }
}

class Region {
  String createdAt;
  bool isActive;
  String name;
  String country;
  String id;

  Region({this.createdAt, this.isActive, this.name, this.country, this.id});

  Region.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    country = json['country'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['country'] = this.country;
    data['id'] = this.id;
    return data;
  }
}
