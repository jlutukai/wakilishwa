class IDVerificationResponse {
  String status;
  Results results;
  bool isCorrect;

  IDVerificationResponse({this.status, this.results, this.isCorrect});

  IDVerificationResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    results =
        json['results'] != null ? new Results.fromJson(json['results']) : null;
    isCorrect = json['is_correct'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.results != null) {
      data['results'] = this.results.toJson();
    }
    data['is_correct'] = this.isCorrect;
    return data;
  }
}

class Results {
  int crn;
  String dateOfBirth;
  String fullName;
  String gender;
  String nationalID;
  String otherNames;
  String surname;

  Results(
      {this.crn,
      this.dateOfBirth,
      this.fullName,
      this.gender,
      this.nationalID,
      this.otherNames,
      this.surname});

  Results.fromJson(Map<String, dynamic> json) {
    crn = json['crn'];
    dateOfBirth = json['dateOfBirth'];
    fullName = json['fullName'];
    gender = json['gender'];
    nationalID = json['nationalID'];
    otherNames = json['otherNames'];
    surname = json['surname'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['crn'] = this.crn;
    data['dateOfBirth'] = this.dateOfBirth;
    data['fullName'] = this.fullName;
    data['gender'] = this.gender;
    data['nationalID'] = this.nationalID;
    data['otherNames'] = this.otherNames;
    data['surname'] = this.surname;
    return data;
  }
}
