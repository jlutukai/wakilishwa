class UpdateUserResponse {
  String status;
  User user;
  String message;

  UpdateUserResponse({this.status, this.user, this.message});

  UpdateUserResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class User {
  Name name;
  bool isSuperAdmin;
  String userType;
  String createdAt;
  bool isActive;
  bool isApproved;
  String email;
  String phone;
  // String accountType;
  String idNumber;
  String passportNumber;
  String admissionNumber;
  // String country;
  // String region;
  // String town;
  String photo;
  String id;

  User(
      {this.name,
      this.isSuperAdmin,
      this.userType,
      this.createdAt,
      this.isActive,
      this.isApproved,
      this.email,
      this.phone,
      // this.accountType,
      this.idNumber,
      this.passportNumber,
      this.admissionNumber,
      // this.country,
      // this.region,
      // this.town,
      this.photo,
      this.id});

  User.fromJson(Map<String, dynamic> json) {
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
    isSuperAdmin = json['is_super_admin'];
    userType = json['user_type'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isApproved = json['is_approved'];
    email = json['email'];
    phone = json['phone'];
    // accountType = json['account_type'];
    idNumber = json['id_number'];
    passportNumber = json['passport_number'];
    admissionNumber = json['admission_number'];
    // country = json['country'];
    // region = json['region'];
    // town = json['town'];
    photo = json['photo'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    data['is_super_admin'] = this.isSuperAdmin;
    data['user_type'] = this.userType;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_approved'] = this.isApproved;
    data['email'] = this.email;
    data['phone'] = this.phone;
    // data['account_type'] = this.accountType;
    data['id_number'] = this.idNumber;
    data['passport_number'] = this.passportNumber;
    data['admission_number'] = this.admissionNumber;
    // data['country'] = this.country;
    // data['region'] = this.region;
    // data['town'] = this.town;
    data['photo'] = this.photo;
    data['id'] = this.id;
    return data;
  }
}

class Name {
  String first;
  String others;

  Name({this.first, this.others});

  Name.fromJson(Map<String, dynamic> json) {
    first = json['first'];
    others = json['others'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first'] = this.first;
    data['others'] = this.others;
    return data;
  }
}
