class GetUserDetailsResponse {
  String status;
  User user;
  Reports reports;

  GetUserDetailsResponse({this.status, this.user, this.reports});

  GetUserDetailsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    reports =
        json['reports'] != null ? new Reports.fromJson(json['reports']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.reports != null) {
      data['reports'] = this.reports.toJson();
    }
    return data;
  }
}

class User {
  Name name;
  FreeCredits freeCredits;
  bool isSuperAdmin;
  String userType;
  String createdAt;
  bool isActive;
  bool isApproved;
  String email;
  String phone;
  AccountType accountType;
  String idNumber;
  String passportNumber;
  String admissionNumber;
  Country country;
  Region region;
  Town town;
  String id;
  String description;

  User(
      {this.name,
      this.freeCredits,
      this.isSuperAdmin,
      this.userType,
      this.createdAt,
      this.isActive,
      this.isApproved,
      this.email,
      this.phone,
      this.accountType,
      this.idNumber,
      this.passportNumber,
      this.admissionNumber,
      this.country,
      this.region,
      this.town,
      this.id,
      this.description});

  User.fromJson(Map<String, dynamic> json) {
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
    freeCredits = json['free_credits'] != null
        ? new FreeCredits.fromJson(json['free_credits'])
        : null;
    isSuperAdmin = json['is_super_admin'];
    userType = json['user_type'];
    createdAt = json['created_at'];
    isActive = json['is_active'];
    isApproved = json['is_approved'];
    email = json['email'];
    phone = json['phone'];
    accountType = json['account_type'] != null
        ? new AccountType.fromJson(json['account_type'])
        : null;
    idNumber = json['id_number'];
    passportNumber = json['passport_number'];
    admissionNumber = json['admission_number'];
    country =
        json['country'] != null ? new Country.fromJson(json['country']) : null;
    region =
        json['region'] != null ? new Region.fromJson(json['region']) : null;
    town = json['town'] != null ? new Town.fromJson(json['town']) : null;
    id = json['id'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.freeCredits != null) {
      data['free_credits'] = this.freeCredits.toJson();
    }
    data['is_super_admin'] = this.isSuperAdmin;
    data['user_type'] = this.userType;
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['is_approved'] = this.isApproved;
    data['email'] = this.email;
    data['phone'] = this.phone;
    if (this.accountType != null) {
      data['account_type'] = this.accountType.toJson();
    }
    data['id_number'] = this.idNumber;
    data['passport_number'] = this.passportNumber;
    data['admission_number'] = this.admissionNumber;
    if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    if (this.region != null) {
      data['region'] = this.region.toJson();
    }
    if (this.town != null) {
      data['town'] = this.town.toJson();
    }
    data['id'] = this.id;
    data['description'] = this.description;
    return data;
  }
}

class Name {
  String first;
  String others;

  Name({this.first, this.others});

  Name.fromJson(Map<String, dynamic> json) {
    first = json['first'];
    others = json['others'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first'] = this.first;
    data['others'] = this.others;
    return data;
  }
}

class FreeCredits {
  int jobAssigned;
  int jobCount;
  int amount;

  FreeCredits({this.jobAssigned, this.jobCount, this.amount});

  FreeCredits.fromJson(Map<String, dynamic> json) {
    jobAssigned = json['job_assigned'];
    jobCount = json['job_count'];
    amount = json['amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['job_assigned'] = this.jobAssigned;
    data['job_count'] = this.jobCount;
    data['amount'] = this.amount;
    return data;
  }
}

class AccountType {
  String createdAt;
  bool isActive;
  String name;
  String typeName;
  String id;

  AccountType(
      {this.createdAt, this.isActive, this.name, this.typeName, this.id});

  AccountType.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    typeName = json['type_name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['type_name'] = this.typeName;
    data['id'] = this.id;
    return data;
  }
}

class Country {
  String createdAt;
  bool isActive;
  String name;
  String dialCode;
  String currencyName;
  String currencyCode;
  String code;
  String id;

  Country(
      {this.createdAt,
      this.isActive,
      this.name,
      this.dialCode,
      this.currencyName,
      this.currencyCode,
      this.code,
      this.id});

  Country.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    dialCode = json['dial_code'];
    currencyName = json['currency_name'];
    currencyCode = json['currency_code'];
    code = json['code'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['dial_code'] = this.dialCode;
    data['currency_name'] = this.currencyName;
    data['currency_code'] = this.currencyCode;
    data['code'] = this.code;
    data['id'] = this.id;
    return data;
  }
}

class Region {
  String createdAt;
  bool isActive;
  String country;
  String name;
  String id;

  Region({this.createdAt, this.isActive, this.country, this.name, this.id});

  Region.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    country = json['country'];
    name = json['name'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['country'] = this.country;
    data['name'] = this.name;
    data['id'] = this.id;
    return data;
  }
}

class Town {
  String createdAt;
  bool isActive;
  String name;
  String region;
  String id;

  Town({this.createdAt, this.isActive, this.name, this.region, this.id});

  Town.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    name = json['name'];
    region = json['region'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['name'] = this.name;
    data['region'] = this.region;
    data['id'] = this.id;
    return data;
  }
}

class Reports {
  Jobs jobs;
  Ratings ratings;

  Reports({this.jobs, this.ratings});

  Reports.fromJson(Map<String, dynamic> json) {
    jobs = json['jobs'] != null ? new Jobs.fromJson(json['jobs']) : null;
    ratings =
        json['ratings'] != null ? new Ratings.fromJson(json['ratings']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.jobs != null) {
      data['jobs'] = this.jobs.toJson();
    }
    if (this.ratings != null) {
      data['ratings'] = this.ratings.toJson();
    }
    return data;
  }
}

class Jobs {
  int completed;
  int posted;
  int inprogress;

  Jobs({this.completed, this.posted, this.inprogress});

  Jobs.fromJson(Map<String, dynamic> json) {
    completed = json['completed'];
    posted = json['posted'];
    inprogress = json['inprogress'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['completed'] = this.completed;
    data['posted'] = this.posted;
    data['inprogress'] = this.inprogress;
    return data;
  }
}

class Ratings {
  double average;
  List<Rates> rates;

  Ratings({this.average, this.rates});

  Ratings.fromJson(Map<String, dynamic> json) {
    average = json['average']?.toDouble();
    if (json['rates'] != null) {
      rates = new List<Rates>();
      json['rates'].forEach((v) {
        rates.add(new Rates.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['average'] = this.average;
    if (this.rates != null) {
      data['rates'] = this.rates.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Rates {
  String createdAt;
  bool isActive;
  String user;
  String job;
  String id;
  String comment;
  double rating;

  Rates(
      {this.createdAt,
      this.isActive,
      this.user,
      this.job,
      this.id,
      this.comment,
      this.rating});

  Rates.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    isActive = json['is_active'];
    user = json['user'];
    job = json['job'];
    id = json['id'];
    comment = json['comment'];
    rating = json['rating']?.toDouble();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['is_active'] = this.isActive;
    data['user'] = this.user;
    data['job'] = this.job;
    data['id'] = this.id;
    data['comment'] = this.comment;
    data['rating'] = this.rating;
    return data;
  }
}
