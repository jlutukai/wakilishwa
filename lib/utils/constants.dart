import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:wakilishwa/core/models/local/local_models.dart';

Color fromHex(String hexString) {
  final buffer = StringBuffer();
  if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
  buffer.write(hexString.replaceFirst('#', ''));
  return Color(int.parse(buffer.toString(), radix: 16));
}

var df2 = DateFormat("yyyy-MM-dd");
var tf = DateFormat("HH:mm");
var cf =
    NumberFormat.currency(locale: "en_KE", symbol: 'Ksh. ', decimalDigits: 2);

const String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

// const double appCharge = 0.05;

const String blue = '#042b5c';
const String gold_lite = '#C07D00';
const String grey = '#707070';
const String gold_dark = '#C89211';
const String serverKey =
    "AAAASevFUO8:APA91bGk6DEtB0uz0ndHBP4WrxvZ1zykdhXtDZaF9nEDmbnq6IvuAHJIbZ-Lz32QGzjtk79IDLJdZevmFXw5LW4-l3Edf2rz_vDxihXyJDd5dS1afqBhedtnvR8tJ-ViggWbx1X21ax1";
const String lorem =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem unknown printer took a galley of type and scrambled it to make a type specimen book.";

showToast(String msg) {
  Fluttertoast.showToast(msg: msg);
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}

Future<bool> checkConnection() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
    return false;
  } on SocketException catch (_) {
    return false;
  }
}

getBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(20.0),
      bottomRight: Radius.circular(20.0),
      bottomLeft: Radius.circular(20.0),
    ),
    borderSide: BorderSide(color: fromHex(gold_dark), width: 1.0),
  );
}

getSearchBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(20.0),
      bottomRight: Radius.circular(20.0),
      bottomLeft: Radius.circular(20.0),
    ),
    borderSide: BorderSide(color: Colors.transparent, width: 1.0),
  );
}

setToken(String s) {
  Box<String> accountInfo = Hive.box<String>("account_info");
  accountInfo.put("token", s);
}

String getToken() {
  Box<String> accountInfo = Hive.box<String>("account_info");
  return accountInfo.get("token");
}

deleteToken() {
  Box<String> accountInfo = Hive.box<String>("account_info");
  accountInfo.delete("token");
}

setCurrentUser(UserData s) {
  Box user = Hive.box("current_user");
  user.put("user", s);
}

UserData getCurrentUser() {
  Box user = Hive.box("current_user");
  return user.get("user");
}

deleteCurrentUser() {
  Box user = Hive.box("current_user");
  user.delete("user");
}

setAddUserData(CreateAccountBody s) {
  Box user = Hive.box("create_user");
  user.put("user", s);
}

CreateAccountBody getAddUserData() {
  Box user = Hive.box("create_user");
  return user.get("user");
}

deleteAddUserData() {
  Box user = Hive.box("create_user");
  return user.delete("user");
}

setActiveContracts(String s) {
  Box<String> accountInfo = Hive.box<String>("account_info");
  accountInfo.put("no_of_active_contracts", s);
}

String getActiveContracts() {
  Box<String> accountInfo = Hive.box<String>("account_info");
  return accountInfo.get("no_of_active_contracts");
}

setArchivedJobs(String s) {
  Box<String> accountInfo = Hive.box<String>("account_info");
  accountInfo.put("no_of_archived_jobs", s);
}

String getArchivedJobs() {
  Box<String> accountInfo = Hive.box<String>("account_info");
  return accountInfo.get("no_of_archived_jobs");
}

setNotificationCount(String s) {
  Box<String> accountInfo = Hive.box<String>("account_info");
  accountInfo.put("no_of_unread", s);
}

String getNotificationCount() {
  Box<String> accountInfo = Hive.box<String>("account_info");
  return accountInfo.get("no_of_unread");
}

deleteAccountInfo() {
  Box<String> accountInfo = Hive.box<String>("account_info");
  accountInfo.clear();
  // accountInfo.deleteFromDisk();
}

// DATA='{"notification": {"body": "this is a body","title": "this is a title"}, "priority": "high", "data": {"click_action": "FLUTTER_NOTIFICATION_CLICK", "id": "1", "status": "done"}, "to": "<FCM TOKEN>"}'
// curl https://fcm.googleapis.com/fcm/send -H "Content-Type:application/json" -X POST -d "$DATA" -H "Authorization: key=<FCM SERVER KEY>"
