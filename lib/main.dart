import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:wakilishwa/core/models/local/local_models.dart';
import 'package:wakilishwa/ui/pages/home.dart';
import 'package:wakilishwa/ui/pages/intro_page.dart';
import 'package:wakilishwa/ui/pages/login.dart';
import 'package:wakilishwa/ui/pages/profile.dart';
import 'package:wakilishwa/ui/pages/registration_page.dart';
import 'package:wakilishwa/ui/pages/reset_password.dart';
import 'package:wakilishwa/ui/pages/splash_page.dart';
import 'package:wakilishwa/ui/widgets/active_contracts_widget.dart';
import 'package:wakilishwa/ui/widgets/archived_jobs_widget.dart';
import 'package:wakilishwa/utils/constants.dart';

import 'core/services/NavigationService.dart';
import 'locator.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  Directory document = await getApplicationDocumentsDirectory();
  Hive
    ..init(document.path)
    ..registerAdapter(UserDataAdapter())
    ..registerAdapter(NameAdapter())
    ..registerAdapter(FreeCreditsAdapter())
    ..registerAdapter(AccountTypeDataAdapter())
    ..registerAdapter(CountryDataAdapter())
    ..registerAdapter(RegionDataAdapter())
    ..registerAdapter(CreateAccountBodyAdapter())
    ..registerAdapter(TownDataAdapter());
  await Hive.openBox<String>("account_info");
  await Hive.openBox("current_user");
  await Hive.openBox("create_user");
  setUpLocator();
  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('google_fonts/OFL.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      navigatorKey: locator<NavigationService>().navigationKey,
      debugShowCheckedModeBanner: false,
      title: 'Wakilishwa',
      theme: ThemeData(
          textTheme: GoogleFonts.montserratTextTheme(
            Theme.of(context).textTheme,
          ),
          brightness: Brightness.light,
          primaryColor: fromHex(blue)),
      darkTheme: ThemeData(
        textTheme: GoogleFonts.montserratTextTheme(
          Theme.of(context).textTheme,
        ),
        brightness: Brightness.dark,
      ),
      themeMode: ThemeMode.dark,
      routes: {
        '/': (context) => SplashPage(),
        IntoPage.tag: (context) => IntoPage(),
        LoginPage.tag: (context) => LoginPage(),
        ResetPassword.tag: (context) => ResetPassword(),
        HomePage.tag: (context) => HomePage(),
        Profile.tag: (context) => Profile(),
        RegistrationPage.tag: (context) => RegistrationPage(),
        ArchivedJobsWidget.tag: (context) => ArchivedJobsWidget(),
        ActiveContractsWidget.tag: (context) => ActiveContractsWidget(),
      },
    );
  }
}
